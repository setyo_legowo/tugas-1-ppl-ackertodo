<?php
/* vim: set ts=4 sw=4 si:
 * ackerTodo - a web-based todo list manager which supports multiple users
 * Copyright (C) 2004-2005 Rob Hensley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * $Id: config.inc.php,v 1.180 2006/09/24 23:02:57 zoidian Exp $
 */
?>
<?php
	$host = 'localhost'; /* Your mySQL server's hostname. */
	$user = 'user_ppl';      /* The user used to login to your mySQL server. */
	$pass = 'password_ppl';      /* The password for the user above. */
	$database = 'ppl001';  /* The database used to store all the information. */
	$table_prefix = '';  /* The prefix for all tables, must contain _ */

	$default_language = 'english'; /* Language before anybody logs in */

	$debug = false;		/* Debug master-switch */
	$db_debug = false;	/* Dump DB statements to debug */
	$db_debug_extended = false;	/* Dump DB results to debug, causes problems before PHP 4.3 */
	$session_debug = false;	/* Dump session info to debug */

	$reopen_daily_tasks = true; /* Set to true to automatically reopen daily tasks the next day */

	$version_number = '4.2';
	$version = "ackerTodo v$version_number";
?>
