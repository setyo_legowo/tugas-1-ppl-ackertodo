<?php
   $user_login = trim($_REQUEST['up_login']);
   $user_pass = trim($_REQUEST['up_pass']);
   $num_tasks = trim($_REQUEST['up_num_tasks']);
   $type = trim($_REQUEST['up_type']);
   $sort = trim($_REQUEST['up_sort']);

   if(isset($user_login) || isset($user_pass)) {
      include_once('../config/config.inc.php');
      include_once('../includes/db.inc');

      $result = db_query("SELECT * FROM ".$table_prefix."users "
                        ."WHERE login='$user_login' "
                        ."AND password=md5('$user_pass')");

      if(!$result || mysql_num_rows($result) != 1) {
         echo "Login Failure!";
         exit;
      }

      $sql = "SELECT * FROM ".$table_prefix."tasks "
            ."WHERE login='$user_login'";

      if(isset($type)) {
         $where = get_where($type);
      }

      if(isset($sort)) {
         $order = get_sort($sort);
      }

      if(trim($where) != '') {
         $sql = $sql . ' AND ' . $where;
      }

      if(trim($sort) != '') {
         $sql = $sql . ' ' . $order;
      }

      $sql = $sql . ' LIMIT ' . $num_tasks;

      $result = db_query($sql);
?>
   <table>
<?php
      $i = 1;
      while($myrow = mysql_fetch_array($result)) {
?>
         <tr>
            <td><?php echo $i ?>.)</td>
            <td><?php echo $myrow['title'] ?></td>
         </tr>
         <tr>
            <td></td>
            <td><?php echo $myrow['task'] ?></td>
         </tr>
<?php
         $i++;
      }
?>
   </table>
<?php
   }

   function get_where($type) {
      switch($type) {
         case 'All':
            $where_type = '';
            break;
         case 'Daily':
            $where_type = 'recurrence = 1';
            break;
         case 'Open-Ended':
            $where_type = 'recurrence = 4';
            break;
         case 'Weekly':
            $where_type = 'recurrence = 2';
            break;
         case 'Monthly':
            $where_type = 'recurrence = 3';
            break;
         case 'Yearly':
            $where_type = 'recurrence = 5';
            break;
         case 'Open':
            $where_type = 'completed = 0';
            break;
         case 'Completed':
            $where_type = 'completed = 1';
            break;
         case 'Overdue':
            $where_type = 'recurrence = 0 '
                    .'AND completed <> 1 '
                    ."AND ".$table_prefix."tasks.date < (CURDATE() + 0) ";
            break;
         case 'This Weeks':
            $where_type = 'recurrence = 0 '
                    ."AND ".$table_prefix."tasks.date >= (CURDATE() - 7) "
                    ."AND ".$table_prefix."tasks.date <= (CURDATE() + 0) ";
            break;
         case other:
            $where_type = '';
      }

      return $where_type;
   }

   function get_sort($sort) {
      switch($sort) {
         case 'Status':
            $where_sort = 'ORDER BY completed desc, '
                         .'compdate desc, recurrence desc, date desc';
            break;
         case 'Title':
            $where_sort = 'ORDER BY title asc';
            break;
         case 'Priority':
            $where_sort = 'ORDER BY priority desc';
            break;
         case 'Category':
            $where_sort = 'ORDER BY category desc';
            break;
         case 'Creation Date':
            $where_sort = 'ORDER BY compdate desc';
            break;
         case other:
            $where_sort = '';
      }

      return $where_sort;
   }
?>
