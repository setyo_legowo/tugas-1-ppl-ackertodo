<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: session.inc,v 1.20 2006/09/01 15:49:48 zoidian Exp $
*/
?>
<?php
ini_set('session.gc_maxlifetime', '7776000');

$session_path = ini_get('session.save_path');
if (file_exists($session_path) && is_dir($session_path)) {
	ini_set('session.save_path', $session_path);
} else {
	debug("Can't store our session files in $session_path. Our sessions may be closed prematurely", 'warning');
}

if(isset($_REQUEST['login_id']) || isset($_REQUEST['login_pass'])) {
	// we're on login page
	include_once('includes/db.inc');
	global $table_prefix;

	$enc_pass = md5($_REQUEST['login_pass']);
	$login = $_REQUEST['login_id'];
	$result = db_query("SELECT * FROM ".$table_prefix."users "
		."WHERE login='$login' AND password='$enc_pass'");

	if(!$result || mysql_num_rows($result) != 1) {
		// auth failed, display error and login window again
        load_language();
		$login_error = _WRONG_PASSWORD;
		include('html/login.html');
		exit;
	}

	if($_REQUEST['login_permanent'] == 1) {
		session_set_cookie_params(7776000);
	}

	session_start();

	$myrow = mysql_fetch_array($result);

	$_SESSION['login'] = $myrow['login'];
	$_SESSION['isadmin'] = $myrow['admin'];
	$_SESSION['theme'] = $myrow['theme'];
	$_SESSION['language'] = $myrow['language'];
	$_SESSION['public_list'] = $myrow['public_list'];
	$_SESSION['date_format'] = $myrow['date_format'];
	$_SESSION['first'] = $myrow['first'];
	$_SESSION['last'] = $myrow['last'];
	$_SESSION['email'] = $myrow['email'];
	$_SESSION['screenname'] = $myrow['screenname'];
	$_SESSION['allow_others'] = $myrow['allow_others'];
	$_SESSION['email_notification'] = $myrow['email_notification'];
	$_SESSION['im_notification'] = $myrow['im_notification'];
	$_SESSION['logged_in'] = 1;
    $_SESSION['default'] = 1;
	$_SESSION['version_check'] = $version_number;
	$_SESSION['login_permanent'] = $_REQUEST['login_permanent'];
	
	add_onlineuser($_SESSION['login']); // BY ANDREAS
    
    read_extra();

	load_language($myrow['language']);

	// refresh to remove URL
	$output['message'] = _LOGON_SUCCESSFUL;
	redirect_on_post();
} else {
	// normal page, do normal session handling

	session_start();

	if(!isset($_SESSION['logged_in']) || $_SESSION['version_check'] != $version_number) {
		session_destroy();
		// not logged in, show login page
        load_language();
		include('html/login.html');
		exit;
	} else {
		if ($_SESSION['login_permanent'] == 1) {
			session_write_close();
			
			session_set_cookie_params(7776000);

			session_start();
		}
	}
}
?>
