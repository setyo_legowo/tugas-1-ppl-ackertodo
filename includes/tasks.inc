<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: tasks.inc,v 1.62 2006/09/20 15:51:11 zoidian Exp $
*/
?>
<?php

/* This function is called from index.php when a task is assigned to a
 * user other then the one you're logged in as. It will notify that user
 * if they have Email notification enabled. */
function send_email($login, $task_array) {
    global $_PRIORITY, $version, $table_prefix;

    while($myrow = mysql_fetch_array($login)) {
        if($myrow['email_notification'] == 1) {
            $email = $myrow['email'];
            $date_format = $myrow['date_format'];

            $title = $task_array[0];
            $task = $task_array[1];
            $recurrence = $task_array[2];
            $priority = $task_array[3];
            $category = $task_array[4];
            $assignedby = $task_array[5];

            if($recurrence == 0) {
                $recurrence = _NO_RECURRENCE;
            } elseif($recurrence == 1) {
                $recurrence = _DAILY;
            } elseif($recurrence == 2) {
                $recurrence = _WEEKLY;
            } elseif($recurrence == 3) {
                $recurrence = _MONTHLY;
            } elseif($recurrence == 4) {
                $recurrence = _OPEN_ENDED;
            } elseif($recurrence == 5) {
                $recurrence = _YEARLY;
            }
            
            $priority = $_PRIORITY[$priority];

            if($category == 0) {
                $category = _NO_CATEGORY;
            } else {
                $result = db_query("SELECT * FROM ".$table_prefix."category "
                                  ."WHERE id='" . $category . "'");
                while($catrow = mysql_fetch_array($result)) {
                    $category = $catrow['name'];
                }
            }

            $body = _GREETING . "\n";
            $body .= "----------------------------\n\n";
            $body .= _TITLE . ": " . $title . "\n";
            $body .= _TASK . ": " . $task . "\n";
            $body .= _PRIORITY . ": " . $priority . "\n";
            $body .= _CATEGORY . ": " . $category . "\n";
            $body .= _RECURRENCE . ": " . $recurrence . "\n";
            $body .= _ASSIGNED_BY . ": " . $assignedby . "\n\n";
            $body .= "----------------------------\n";
            $body .= _CLOSING . " " . $version . "!";

            mail($email, _SUBJECT, $body,
                "From: ackerTodo\r\n" .
                "Reply-To: $replyto\r\n" .
                "X-Mailer: PHP/" . phpversion());
        }
    }
}

function get_task($id, &$output, $get_name = FALSE) {
	global $table_prefix;

	if (!isset($output)) {
		$output = array();
	}

	if ($get_name) {
		$result = db_query("SELECT ".$table_prefix."tasks.*, ".$table_prefix."tasks.login as task_owner, ".$table_prefix."category.name as category_name FROM ".$table_prefix."tasks, ".$table_prefix."category "
			. "WHERE ".$table_prefix."tasks.id='$id' AND ".$table_prefix."tasks.category = ".$table_prefix."category.id");
	} else {
		$result = db_query("SELECT *, ".$table_prefix."tasks.login as task_owner FROM ".$table_prefix."tasks WHERE id='$id'");
	}

	$myrow = mysql_fetch_assoc($result);

	$output = array_merge($myrow, $output);

	return $output;
}

function get_calendar_tasks($startdate, $enddate) {
    global $login, $table_prefix;

	$login_and_groups = "'$login'";
	$groups_in_array = get_groups_in_array($login);
	foreach ($groups_in_array as $group) {
		$login_and_groups .= ",'group_$group'";
	}

    $result = db_query("SELECT * FROM ".$table_prefix."tasks WHERE date >= '$startdate' "
                    ."AND date <= '$enddate' AND login IN ($login_and_groups)");
    
    $date_array = array();
    while($myrow = mysql_fetch_array($result)) {
        $temp_date = substr($myrow['date'], 0, 8);
        $date_array[] = $temp_date;
    }
    
    return $date_array;
}

function update_task($title, $task, $final_date, $recurrence, $users, $priority, $category, $id) {
	global $login, $table_prefix;

	$task = db_sanitize($task);
	$title = db_sanitize($title);

	if ($category == '') {
		$category = 0;
	}

	if (!$final_date) {
		$final_date = 'NOW()';
	} else {
		$final_date = "$final_date";
	}

	db_query("UPDATE ".$table_prefix."tasks "
    		."SET title='$title',task='$task',date='$final_date', "
	    	."recurrence='$recurrence',login='$users',priority='$priority', "
		    ."category='$category' "
    		."WHERE id='$id'");
}

function add_task($title, $task, $final_date, $recurrence, $users, $priority, $category) {
	global $login, $table_prefix;

	$task = db_sanitize($task);
	$title = db_sanitize($title);

	if ($category == '') {
		$category = 0;
	}

	if (!$final_date) {
		$final_date = 'NOW()';
	} else {
		$final_date = "'$final_date'";
	}

	db_query("INSERT INTO ".$table_prefix."tasks (login,creationdate,recurrence,title,task,date,assignedby,category,priority)"
		."VALUES ('$users',NOW(),'$recurrence','$title','$task',$final_date,'$login','$category','$priority')");
}

function add_comment($task_id, $comment) {
	global $login, $table_prefix;

	$comment = db_sanitize($comment);

	db_query("INSERT INTO ".$table_prefix."comments (task_id,login,date,comment) "
		."VALUES ('$task_id','$login',now(),'$comment')" );
}

function update_comment($comment_id, $comment) {
	global $login, $table_prefix;

	$comment = db_sanitize($comment);

	db_query("UPDATE ".$table_prefix."comments "
            ."SET login='$login',date=now(),comment='$comment'"
            ."WHERE id='$comment_id'");
}

function get_comments($task) {
    global $login, $cache_comments, $output, $table_prefix;
	
	if (empty($cache_comments)) {
		$result = db_query("SELECT task_id, COUNT(*) as num FROM ".$table_prefix."comments, ".$table_prefix."tasks, ".$table_prefix."category WHERE ${output['where']} AND task_id = tasks.id GROUP BY task_id");

		while($myrow = mysql_fetch_array($result)) {
			$cache_comments[$myrow['task_id']] = $myrow['num'];
		}
	}

    return empty($cache_comments[$task]) ? 0 : $cache_comments[$task];
}

function get_status($myrow, $date_format) {
    global $mysql_version;

	$recurrence = $myrow['recurrence'];
	$date = $myrow['date'];
    $curdate = date("Ymd");
    $cur_dow = "1" . date("w", strtotime(date("Ymd")));

    if($mysql_version != NULL) {
        if($mysql_version == 0) {
            $date = substr($date, 0, 8);
            $task_dow = substr($myrow['date'], 6, 2);
        } elseif($mysql_version == 1) {
            $date = substr($date, 0, 10);
            $date = explode("-", $date);
            $date = implode($date);
            $task_dow = substr($myrow['date'], 8, 2);
        }
    } else {
        $length = strlen($date);
        if($length == 19) {
            $date = substr($date, 0, 10);
            $date = explode("-", $date);
            $date = implode($date);
            $task_dow = substr($myrow['date'], 8, 2);
        } else {
            $date = substr($date, 0, 8);
            $task_dow = substr($myrow['date'], 6, 2);
        }
    }

	$difference = floor(abs(strtotime($curdate) - strtotime($date))/86400);

	if($myrow['completed'] == 1) {
		$status = _COMPLETE;
		$image = 'completed.gif';
	} else {
		if($recurrence != 1 && $recurrence != 4) {
			if($date < $curdate && $recurrence != 2) {
                if($recurrence == 0) {
    				if($difference > 1) {
	    				$status = $difference . ' ' . _DAYS . ' ' . _OVERDUE;
		    		} else {
			    		$status = $difference . ' ' . _DAY . ' ' . _OVERDUE;
				    }
                } elseif($recurrence == 3) {
                    if($difference == 0) {
                        $status = _MONTHLY . ': ' . _TODAY;
                    } elseif($difference > 1) {
	    				$status = _MONTHLY . ': ' . $difference . ' ' .  _DAYS . ' ' . _OVERDUE;
		    		} else {
			    		$status = _MONTHLY . ': ' . $difference . ' ' . _DAY . ' ' . _OVERDUE;
				    }
                } elseif($recurrence == 5) {
                    if($difference == 0) {
                        $status = _YEARLY . ': ' . _TODAY;
                    } elseif($difference > 1) {
	    				$status = _YEARLY . ': ' . $difference . ' ' . _DAYS . ' ' . _OVERDUE;
		    		} else {
			    		$status = _YEARLY . ': ' . $difference . ' ' . _DAY . ' ' . _OVERDUE;
				    }
                }
				$image = 'overdue.gif';
            } elseif($recurrence == 2 && $cur_dow > $task_dow) {
                if($task_dow == 10) {
                    $status = _WEEKLY . ': ' . _SUNDAY;
                } elseif($task_dow == 11) {
                    $status = _WEEKLY . ': ' . _MONDAY;
                } elseif($task_dow == 12) {
                    $status = _WEEKLY . ': ' . _TUESDAY;
                } elseif($task_dow == 13) {
                    $status = _WEEKLY . ': ' . _WEDNESDAY;
                } elseif($task_dow == 14) {
                    $status = _WEEKLY . ': ' . _THURSDAY;
                } elseif($task_dow == 15) {
                    $status = _WEEKLY . ': ' . _FRIDAY;
                } elseif($task_dow == 16) {
                    $status = _WEEKLY . ': ' . _SATURDAY;
                }
				$image = 'overdue.gif';
			} else {
                if($date == $curdate || $cur_dow == $task_dow) {
                    if($recurrence == 2) {
        				$status = _WEEKLY . ': ' . _TODAY;
                    } elseif($recurrence == 3) {
        				$status = _MONTHLY . ': ' . _TODAY;
                    } elseif($recurrence == 5) {
                        $status = _YEARLY;
                    } else {
        				$status = _DEADLINE . ': ' . _TODAY;
                    }
	    			$image = 'today.gif';
                } else {
                    if($recurrence == 2) {
                        if($task_dow == 10) {
                            $status = _WEEKLY . ': ' . _SUNDAY;
                        } elseif($task_dow == 11) {
                            $status = _WEEKLY . ': ' . _MONDAY;
                        } elseif($task_dow == 12) {
                            $status = _WEEKLY . ': ' . _TUESDAY;
                        } elseif($task_dow == 13) {
                            $status = _WEEKLY . ': ' . _WEDNESDAY;
                        } elseif($task_dow == 14) {
                            $status = _WEEKLY . ': ' . _THURSDAY;
                        } elseif($task_dow == 15) {
                            $status = _WEEKLY . ': ' . _FRIDAY;
                        } elseif($task_dow == 16) {
                            $status = _WEEKLY . ': ' . _SATURDAY;
                        }
                    } elseif($recurrence == 3) {
        				$status = _MONTHLY . ': ' . format_date_simple($myrow['date'], $date_format);
                    } elseif($recurrence == 5) {
        				$status = _YEARLY . ': ' . format_date_simple($myrow['date'], $date_format);
                    } else {
        				$status = _DEADLINE . ': ' . format_date_simple($myrow['date'], $date_format);
                    }
	    			$image = 'today.gif';
                }
			}
		} else {
            if($recurrence == 4) {
			    $status = _OPEN_ENDED;
            } else {
			    $status = _DAILY . ': ' . _OPEN;
            }
			$image = 'daily.gif';
		}
	}

    return array($status, $image);
}

/* Show list of tasks */
function get_task_list() {
	global $output, $login, $table_prefix;
    $where = NULL;

    if($_SESSION['default'] == 0) {
    	$task_what = get_value_session_request('task_what');
	    $task_who = get_value_session_request('task_who');
	    $filter_category = get_value_session_request('filter_category');
	    $filter_priority = get_value_session_request('filter_priority');
    } else {
        $task_what = isset($_SESSION['extra']['view'])?$_SESSION['extra']['view']['tasks']:null;
        $task_who = isset($_SESSION['extra']['view'])?$_SESSION['extra']['view']['users']:null;
        $filter_category = isset($_SESSION['extra']['view'])?$_SESSION['extra']['view']['category']:null;
        $filter_priority = isset($_SESSION['extra']['view'])?$_SESSION['extra']['view']['priority']:null;
        $_SESSION['task_what'] = $task_what;
        $_SESSION['task_who'] = $task_who;
        $_SESSION['filter_category'] = $filter_category;
        $_SESSION['filter_priority'] = $filter_priority;
    }

	if($task_what == 'open') {
		$where = 'completed = 0';
	} elseif($task_what == 'completed') {
		$where = 'completed = 1';
	} elseif($task_what == 'daily') {
		$where = 'recurrence = 1';
	} elseif($task_what == 'weekly') {
		$where = 'recurrence = 2';
	} elseif($task_what == 'monthly') {
		$where = 'recurrence = 3';
	} elseif($task_what == 'open-ended') {
        $where = 'recurrence = 4';
	} elseif($task_what == 'yearly') {
        $where = 'recurrence = 5';
	} elseif($task_what == 'overdue') {
        $where = 'recurrence = 0 '
            .'AND completed <> 1 '
            ."AND ".$table_prefix."tasks.date < (CURDATE() + 0) ";
	} elseif($task_what == 'this_week') {
		$where = 'recurrence = 0 '
			."AND ".$table_prefix."tasks.date >= (CURDATE() - 7) "
			."AND ".$table_prefix."tasks.date <= (CURDATE() + 0) ";
	} elseif($task_what == 'date') {
		$task_date = get_value_session_request('task_date');
		$where = 'recurrence = 0 '
			."AND ".$table_prefix."tasks.date = $task_date ";

		$output['task_date'] = $task_date;
	} elseif($task_what == 'all') {
		// no where clause for that...
	} else {
		// if none of the above, show only open
        $task_what = 'open';
		$where = 'completed = 0';
	}

    if ($task_who == 'mine') {
		// no where clause for that...
    } elseif($task_who == 'everyone') {
        verify_admin();
    } else {
        $whos_list = $task_who;
    }

	$filter_text = get_value_session_request('filter_text');

	if(isset($filter_text) && $filter_text) {
	    if (isset($where)) {
		    $where .= ' AND ';
		}

        if (isset($where)) {
    		$where .= "(title LIKE '%$filter_text%' OR task LIKE '%$filter_text%')";
        } else {
    		$where = "(title LIKE '%$filter_text%' OR task LIKE '%$filter_text%')";
        }
    }

	if (isset($filter_category) && $filter_category) {
		if (isset($where)) {
			$where .= ' AND ';
		}
		
		if (substr($filter_category, 0, 2) == 's_') {
			$supercat_id = substr($filter_category, 2);
			$categories = get_categories_in_supercat($supercat_id);
			
			$where .= "category in ('" . implode("','", $categories) . "')";
		} else {
			$where .= "category = '$filter_category'";
		}
	}

	if(isset($filter_priority) && $filter_priority > 0) {
		if(isset($where)) {
			$where .= ' AND ';
		}
		$where .= "priority >= '$filter_priority'";
	}

	if(isset($_SESSION['extra']['order']))
        $sort = $_SESSION['extra']['order'];
    else
        $sort = NULL;
    
    if(!isset($sort)) {
        $sort = array();
    }
    
	$order_by = '';
	
	foreach ($sort as $s) {
		$criteria = $s['criteria'];
		$dir = $s['dir'];
		
		switch ($criteria) {
			case 'status':
				$order_by .= "completed $dir, compdate $dir, recurrence $dir, date $dir";
				break;
			
			case 'category':
				$order_by .= "category_name $dir";
				break;
			
			default:
				$order_by .= "$criteria $dir";
				break;
		}
		
		$order_by .= ',';
	}
	
	if (!empty($order_by)) {
		$order_by = substr($order_by, 0, strlen($order_by) - 1);
	} else {
		unset($order_by);
	}

    if(isset($whos_list)) {
    	$login_and_groups = "'$task_who'";
    	$groups_in_array = get_groups_in_array($task_who);
    } else {
    	$login_and_groups = "'$login'";
	    $groups_in_array = get_groups_in_array($login);
    }

    foreach ($groups_in_array as $group) {
	    $login_and_groups .= ",'group_$group'";
    }
    
    if($task_who == 'everyone') {
    	$query = $table_prefix."tasks.category = ".$table_prefix."category.id";
    } else {
    	$query = $table_prefix."tasks.login IN ($login_and_groups) AND ".$table_prefix."tasks.category = ".$table_prefix."category.id";
    }

	if(isset($where)) {
		$query .= " AND $where";
	}

	$output['where'] = $query;
	
	if(isset($order_by)) {
		$query .= " ORDER BY $order_by";
	}
	
    // That'll stop some sql injections
    // This breaks ths task list for some reason...
    //    Warning: mysql_fetch_array(): supplied argument is not a valid
    //    MySQL result resource in /var/www/todo/html/list_tasks.html on
    //    line 257
    // $query with this line...
    //    tasks.login IN (\'rob\',\'group_43\') AND tasks.category =
    //    category.id AND completed = 0 ORDER BY category_name
    //    asc,completed asc, compdate asc, recurrence asc, date asc
    // $query without this line...
    //    tasks.login IN ('rob','group_43') AND tasks.category =
    //    category.id AND completed = 0 ORDER BY category_name
    //    asc,completed asc, compdate asc, recurrence asc, date asc
    // David -- Any suggestions?
//    $query = mysql_real_escape_string($query);
    
	$query = "SELECT ".$table_prefix."tasks.*, ".$table_prefix."category.name as category_name FROM ".$table_prefix."tasks, ".$table_prefix."category WHERE " . $query;

	if(isset($task_what))       $output['task_what'] = $task_what;
	if(isset($task_who))        $output['task_who'] = $task_who;
	if(isset($sort))            $output['sort'] = $sort;
	if(isset($dir))             $output['dir'] = $dir;
	if(isset($filter_text))     $output['filter_text'] = $filter_text;
	if(isset($filter_category)) $output['filter_category'] = $filter_category;
	if(isset($filter_priority)) $output['filter_priority'] = $filter_priority;
	$output['result'] = db_query($query);

	return $output['result'];
}
?>
