<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: db.inc,v 1.18 2005/11/28 15:12:03 zoidian Exp $
*/
?>
<?php
$db = mysql_connect($host, $user, $pass);
mysql_select_db($database, $db);

$mysql_version = mysql_get_server_info();
$mysql_version = explode(".", $mysql_version);
$mysql_version_major = $mysql_version[0];
$mysql_version_minor = $mysql_version[1];

if($mysql_version_major == 5) {
    $mysql_version = 1;
} elseif($mysql_version_major == 4 && $mysql_version_minor == 1) {
    $mysql_version = 1;
} elseif($mysql_version_major == 4 && $mysql_version_minor == 0) {
    $mysql_version = 0;
} elseif($mysql_version != 0 && $mysql_version != 1) {
    $mysql_version = 0;
}

function db_query($query) {
	global $db, $db_debug, $db_debug_extended;

	$result = mysql_query($query, $db);

	if ($db_debug) {
		$msg = "Query: $query<br />Output: " . mysql_error($db)
			 . '<br />Returned: ' . @mysql_affected_rows($db). "\n";

		if ($db_debug_extended && ! is_bool($result)) {
			$result1 = mysql_query($query, $db);
			$results = '';

			while ($myrow = mysql_fetch_array($result1, MYSQL_ASSOC)) {
    			$results .= '<br />' . print_r($myrow, true);
			}

			$msg .= "$results\n";
		}

		debug($msg);
	}

	return $result;
}

function db_sanitize($text) {
	if (get_magic_quotes_gpc()) {
		$text = stripslashes($text);
	}

	return mysql_real_escape_string(ereg_replace(13,'<br />', $text));
}
?>
