<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: categories.inc,v 1.11 2006/09/01 16:15:45 zoidian Exp $
*/
?>
<?php
/* This function gets all of the users categories and displays them in a
 * drop down box. */
function disp_category($category, $title = null, $name = _CATEGORY, $suffix = '', $all = _NO_CATEGORY, $include_supercats = false, $include_global = true) {

	if (!isset($title)) {
		$title = _CATEGORY.'&nbsp;';
	}

	$categories = get_categories_id_name_aarray($include_supercats, $include_global);

	echo "$title<select name=\"category\" size=\"1\" class=\"btn\" $suffix>\n";
	echo "<option value=\"\" selected=\"selected\">$all</option>\n";

	foreach ($categories as $id => $name) {
		if($id == $category) {
			echo '<option value="'.$id.'" selected="selected">'.$name."</option>\n";
		} else {
			echo '<option value="'.$id.'">'.$name."</option>\n";
		}
	}

	echo "</select>\n";
}

function get_categories_id_name_aarray($include_supercats = false, $include_global = false) {
	global $login, $table_prefix;

	$categories_id_name_aarray = array();

    if ($include_global) {
    	$result = db_query("SELECT * FROM ".$table_prefix."category "
                          ."WHERE login='$login' OR login='' AND name <> '' "
                          ."ORDER BY name");
    } else {
    	$result = db_query("SELECT * FROM ".$table_prefix."category "
                          ."WHERE login='$login' "
                          ."ORDER BY name");
    }

	while ($myrow = mysql_fetch_array($result)) {
		$categories_id_name_aarray[$myrow['id']] = $myrow['name'];
	}
	
	if ($include_supercats) {
		$result = db_query("SELECT * FROM ".$table_prefix."supercats WHERE login='$login' ORDER BY name");
	
		while ($myrow = mysql_fetch_array($result)) {
			$categories_id_name_aarray['s_' . $myrow['id']] = $myrow['name'] . ' (' . _SUPERCAT .')';
		}
	}
	
	return $categories_id_name_aarray;
}

function get_global_categories_array() {
	global $table_prefix;
    $result = db_query("SELECT * FROM ".$table_prefix."category WHERE login='' AND id!='0'");

    $global_categories = array();

    while ($myrow = mysql_fetch_array($result)) {
        $global_categories[$myrow['id']] = $myrow['name'];
    }

    return $global_categories;
}

/*function get_categories_id_array() {
	global $login;
	global $table_prefix;

	$result = db_query("SELECT id FROM ".$table_prefix."category WHERE login='$login' ORDER by id");
	$categories_id_array = array();

	while ($myrow = mysql_fetch_array($result)) {
		$categories_id_array[] = $myrow['id'];
	}

	return $categories_id_array;
}*/

function get_categories_in_supercat($supercat_id) {
	global $table_prefix;

	$result = db_query("SELECT category_id FROM ".$table_prefix."supercat_membership WHERE supercat_id = '$supercat_id'");
	$categories = array();
	
	while ($myrow = mysql_fetch_array($result)) {
		$categories[] = $myrow['category_id'];
	}
	
	return $categories;
}

function get_supercats_and_categories_array() {
	global $login, $table_prefix;

	$supercats_and_categories_array = array();

	$categories = get_categories_id_name_aarray();
	$categories1 = array();
	foreach ($categories as $id => $name) {
		$categories1[] = array('id' => $id, 'name' => $name);
	}

	// add to the list those supercats that have categories
	$result = db_query("SELECT ".$table_prefix."supercats.id as supercat_id, ".$table_prefix."supercats.name as name, ".$table_prefix."supercat_membership.category_id as category_id FROM ".$table_prefix."supercats, ".$table_prefix."supercat_membership"
		. " WHERE ".$table_prefix."supercats.id = ".$table_prefix."supercat_membership.supercat_id AND login='$login'"
		. " ORDER BY name");

	while ($myrow = mysql_fetch_array($result)) {
		$gid = $myrow['supercat_id'];
		$category_id = $myrow['category_id'];
		$category_name = $categories[$category_id];

		if (!isset($supercats_and_categories_array[$gid])) {
			$supercat = array();

			$supercat['id'] = $gid;
			$supercat['name'] = $myrow['name'];
			$supercat['categories_in'][] = array('id' => $category_id, 'name' => $category_name);
			$supercat['categories_in_hash'][$category_id] = $category_id;

			$supercats_and_categories_array[$gid] = $supercat;
		} else {
			$supercats_and_categories_array[$gid]['categories_in'][]
					= array('id' => $category_id, 'name' => $category_name);
			$supercats_and_categories_array[$gid]['categories_in_hash'][$category_id] = $category_id;
		}
	}

	// populate list of categories not in supercat
	foreach (array_keys($supercats_and_categories_array) as $gid) {
		foreach ($categories as $category_id => $category_name) {
			if (!isset($supercats_and_categories_array[$gid]['categories_in_hash'][$category_id])) {
				$supercats_and_categories_array[$gid]['categories_out'][]
						= array('id' => $category_id, 'name' => $category_name);
			}
		}
	}

	// add the supercats that have no categories
	$result = db_query("SELECT ".$table_prefix."supercats.id as supercat_id, ".$table_prefix."supercats.name as name FROM ".$table_prefix."supercats"
		. " WHERE login='$login' ORDER BY name");

	while ($myrow = mysql_fetch_array($result)) {
		$gid = $myrow['supercat_id'];

		if (!isset($supercats_and_categories_array[$gid])) {
			$supercat = array();

			$supercat['id'] = $gid;
			$supercat['name'] = $myrow['name'];
			$supercat['categories_out'] = $categories1;

			$supercats_and_categories_array[$gid] = $supercat;
		}
	}

	return $supercats_and_categories_array;
}

function add_category_to_supercat($category_id, $supercat_id) {
	global $table_prefix;

	db_query("INSERT INTO ".$table_prefix."supercat_membership (category_id, supercat_id) VALUES ('$category_id', '$supercat_id')");
}

function remove_category_from_supercat($category_id, $supercat_id) {
	global $table_prefix;

	db_query("DELETE FROM ".$table_prefix."supercat_membership WHERE category_id='$category_id' AND supercat_id='$supercat_id'");
}

function remove_supercat($supercat_id) {
	global $table_prefix;

	db_query("DELETE FROM ".$table_prefix."supercats WHERE id='$supercat_id'");
	db_query("DELETE FROM ".$table_prefix."supercat_membership WHERE supercat_id='$supercat_id'");
}
?>
