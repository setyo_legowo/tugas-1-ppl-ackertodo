<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: users.inc,v 1.28 2006/09/19 12:27:59 zoidian Exp $
*/
?>
<?php
/* This function lists all users and groups who you're allowed to
 * assign a task to in a dropdown box. */
function disp_users_and_groups($login, $select_user_or_group) {
	disp_users($login, 'allow_others', $select_user_or_group);
	disp_groups($login, $select_user_or_group, true, true);
}

/* This function lists all of the users who you're allowed to
 * assign a task to in a dropdown box. */
function disp_users($login, $how='all', $select_user_or_group=null) {
	$users = get_users_login_allow_array();

	if (empty($select_user_or_group)) {
		$select_user_or_group = $login;
	}

	foreach($users as $user) {
		$user_login = $user['login'];

		if($select_user_or_group == $user_login) {
			echo "<option value=\"$user_login\" selected=\"selected\">$user_login</option>\n";
		} else {
			if ($how != 'allow_others' || $user['allow_others']) {
				echo "<option value=\"$user_login\">$user_login</option>\n";
			}
		}
	}
}

/* This function will return a list of the users who have publically
 * viewable task lists. */
function disp_public_list() {
	global $table_prefix;

    $result = db_query("SELECT login,first,last FROM ".$table_prefix."users WHERE public_list='1'");
    
    return $result;
}

/* This function removes a user. */
function remove_user($login) {
	global $table_prefix;

	db_query("DELETE FROM ".$table_prefix."users WHERE login='$login'");
	db_query("DELETE FROM ".$table_prefix."group_membership WHERE login='$login'");
	// todo: also delete tasks and comments associated with user
}

/* This function is used to change a users password. */
function change_pw($users, $curpass, $newpass, $verpass) {
	global $output, $isadmin, $table_prefix;

	if($curpass == NULL && !$isadmin) {
		$output['error'] = _YOUR_CURRENT_PASS;
		return false;
	} elseif($newpass == NULL) {
		$output['error'] = _YOUR_NEW_PASS;
		return false;
	} elseif($verpass == NULL) {
		$output['error'] = _YOUR_VERIFY_PASS;
		return false;
	} elseif($verpass != $newpass) {
		$output['error'] = _NEW_PASS_ERROR;
		return false;
	} else {
		if (!$isadmin) {
			$result = db_query("SELECT password FROM ".$table_prefix."users "
				."WHERE login='$users' AND password=md5('$curpass')");

			if (mysql_num_rows($result) != 1) {
				// verification failed
				$output['error'] = _CURRENT_PASS_ERROR;
				return false;
			}
		}
		$result = db_query("UPDATE ".$table_prefix."users "
			."SET password=md5('$newpass') "
			."WHERE login='$users'");

		$output['message'] = _PASS_CHANGED;
	}
}

function change_user() {
	global $table_prefix,$isadmin, $users, $output, $login;

	if(isset($_REQUEST['allow_others'])) {
		$new_allow_others = 1;
	} else {
		$new_allow_others = 0;
	}

	if(isset($_REQUEST['public_list'])) {
		$new_public_list = 1;
	} else {
		$new_public_list = 0;
	}

	if(isset($_REQUEST['email_notification'])) {
		$new_email_notification = 1;
	} else {
		$new_email_notification = 0;
	}
    
	if(isset($_REQUEST['im_notification'])) {
		$new_im_notification = 1;
	} else {
		$new_im_notification = 0;
	}

	if($isadmin && isset($_REQUEST['allow_admin'])) {
		$new_allow_admin = 1;
	} else {
		$new_allow_admin = 0;
	}

	$firstname = $_REQUEST['firstname'];
	$lastname = $_REQUEST['lastname'];
	$email = $_REQUEST['email'];
	$screenname = $_REQUEST['screenname'];
	$language = $_REQUEST['newlanguage'];
	$date_format = $_REQUEST['newdate_format'];
	$theme = $_REQUEST['newtheme'];

	$result = db_query("UPDATE ".$table_prefix."users "
		."SET first='$firstname',last='$lastname',"
		."email='$email',screenname='$screenname',"
		."allow_others='$new_allow_others',"
        ."public_list='$new_public_list',"
		."email_notification='$new_email_notification',"
		."im_notification='$new_im_notification',"
		."language='$language',date_format='$date_format',"
        ."theme='$theme',admin='$new_allow_admin' "
		."WHERE login='$users'");

    if(!isset($login)) {
        $login = '';
    }
    
	if ($users == $login) {
		// update session
		$_SESSION['first'] = $firstname;
		$_SESSION['last'] = $lastname;
		$_SESSION['email'] = $email;
		$_SESSION['screenname'] = $screenname;
		$_SESSION['allow_others'] = $new_allow_others;
		$_SESSION['public_list'] = $new_public_list;
		$_SESSION['email_notification'] = $new_email_notification;
		$_SESSION['im_notification'] = $new_im_notification;
		$_SESSION['language'] = $language;
		$_SESSION['date_format'] = $date_format;
		$_SESSION['theme'] = $theme;
		$_SESSION['isadmin'] = $new_allow_admin;
		
		// save extra
		$_SESSION['extra']['auto_daily'] = $_REQUEST['auto_daily']?'1':'0';
        $_SESSION['extra']['ajax'] = $_REQUEST['ajax']?'1':'0';
		write_extra();
	}

	$output['message'] = _INFO_UPDATED;
}

/* This is the function that displays the form to change the users
 * theme. */
function disp_themes($login) {
	global $theme;

	echo '<select name="newtheme" size="1" class="btn">';
	if($handle = opendir('themes')) {
		while(false !== ($file = readdir($handle))) {
			if($file != '.' && $file != '..' && $file != 'CVS' && $file != 'index.php') {
				echo "$file\n";
				if ($file == $theme) {
					echo "<option value=\"$file\" selected=\"selected\">$file</option>\n";
				} else {
					echo "<option value=\"$file\">$file</option>\n";
				}
			}
		}
		closedir($handle);
	}
	echo "</select>\n";
}

/* This is the function that displays the form to change the users
 * language. */
function disp_languages($login) {
	global $table_prefix;

    $result = db_query("SELECT language FROM ".$table_prefix."users WHERE login='$login'");
    $myrow = mysql_fetch_array($result);
    $user_language = $myrow['language'];

	if($handle = opendir('includes/languages')) {
		while(false !== ($file = readdir($handle))) {
			if($file != '.' && $file != '..' && $file != 'CVS' && $file != 'index.php') {
				list($new_language, $ext) = explode('.', $file);
                $new_languages[] = $new_language;
				$disp_language = ucfirst($new_language);
                $disp_languages[] = $disp_language;
			}
		}
		closedir($handle);
	}

    sort($disp_languages);
    sort($new_languages);

	echo '<select name="newlanguage" size="1" class="btn">';
    $i = 0;
    foreach ($disp_languages as $language) {
        if ($new_languages[$i] == $user_language) {
		    echo "<option value=\"$new_languages[$i]\" selected=\"selected\">$language</option>\n";
        } else {
			echo "<option value=\"$new_languages[$i]\">$language</option>\n";
        }
        $i++;
    }
	echo "</select>\n";
}

/* This is the function that displays the form to change the users
 * date format. */
function disp_date_formats($date_format) {
	echo "<select name=\"newdate_format\" size=\"1\" class=\"btn\">\n";
    if($date_format == 0)
        echo "<option value=\"0\" selected>MM/DD/YYYY</option>\n";
    else
        echo "<option value=\"0\">MM/DD/YYYY</option>\n";
    if($date_format == 1)
        echo "<option value=\"1\" selected>MM/DD/YY</option>\n";
    else
        echo "<option value=\"1\">MM/DD/YY</option>\n";
    if($date_format == 2)
        echo "<option value=\"2\" selected>DD/MM/YYYY</option>\n";
    else
        echo "<option value=\"2\">DD/MM/YYYY</option>\n";
    if($date_format == 3)
        echo "<option value=\"3\" selected>DD/MM/YY</option>\n";
    else
        echo "<option value=\"3\">DD/MM/YY</option>\n";
    if($date_format == 4)
        echo "<option value=\"4\" selected>MM-DD-YYYY</option>\n";
    else
        echo "<option value=\"4\">MM-DD-YYYY</option>\n";
    if($date_format == 5)
        echo "<option value=\"5\" selected>MM-DD-YY</option>\n";
    else
        echo "<option value=\"5\">MM-DD-YY</option>\n";
    if($date_format == 6)
        echo "<option value=\"6\" selected>DD-MM-YYYY</option>\n";
    else
        echo "<option value=\"6\">DD-MM-YYYY</option>\n";
    if($date_format == 7)
        echo "<option value=\"7\" selected>DD-MM-YY</option>\n";
    else
        echo "<option value=\"7\">DD-MM-YY</option>\n";
    if($date_format == 8)
        echo "<option value=\"8\" selected>YYYY-MM-DD</option>\n";
    else
        echo "<option value=\"8\">YYYY-MM-DD</option>\n";
	echo "</select>\n";
}

function verify_admin($view = 'admin_view') {
	global $isadmin;

	if (!$isadmin) {
		$output['error'] = _ADMIN_ERROR;
		show_view($view);
		exit;
	}
}

function get_users_login_array() {
	global $table_prefix;

	$result = db_query("SELECT login FROM ".$table_prefix."users ORDER by login");
	$users_array = array();

	while ($myrow = mysql_fetch_array($result)) {
		$users_array[] = $myrow['login'];
	}

	return $users_array;
}

function get_users_login_allow_array() {
	global $table_prefix;

	$result = db_query("SELECT login, allow_others FROM ".$table_prefix."users ORDER BY login");
	$users_array = array();

	while ($myrow = mysql_fetch_array($result)) {
		$users_array[] = $myrow;
	}

	return $users_array;
}

function get_users() {
	global $table_prefix;

	return db_query("SELECT * FROM ".$table_prefix."users ORDER BY login");
}

function get_user_array($users) {
	global $table_prefix;

	$result = db_query("SELECT * FROM ".$table_prefix."users WHERE login='$users'");

	if (mysql_num_rows($result) == 1) {
		return mysql_fetch_array($result);
	} else {
		return;
	}
}

function get_groups_in_array($login) {
	global $table_prefix;

	$groups_in_array = array();

	$result = db_query("SELECT group_id FROM ".$table_prefix."group_membership WHERE login='$login'");

	while ($myrow = mysql_fetch_array($result)) {
		$groups_in_array[] = $myrow['group_id'];
	}

	return $groups_in_array;
}

function read_extra($login_override = null) {
	global $table_prefix;

	if (empty($login_override)) {
		$my_login = $_SESSION['login'];
	} else {
		$my_login = $login_override;
	}
	
	$result = db_query("SELECT extra FROM ".$table_prefix."users WHERE login='$my_login'");
	
	$myrow = mysql_fetch_array($result);
	
	$extra = $myrow['extra'];
	
	if (empty($extra)) {
		return null;
	} else {
		$extra = unserialize($extra);
		
		if (empty($login_override)) {
			$_SESSION['extra'] = $extra;
		}
		
		return unserialize($extra);
	}
}

function write_extra($extra = null, $login_override = null) {
	global $login, $table_prefix;
	
	if (empty($login_override)) {
		$my_login = $_SESSION['login'];
		
		if (empty($extra)) {
			$extra = $_SESSION['extra'];
		}
	} else {
		$my_login = $login_override;
	}
	
	$ser = serialize($extra);
	
	db_query("UPDATE ".$table_prefix."users SET extra = '$ser' WHERE login='$my_login'");
}
?>
