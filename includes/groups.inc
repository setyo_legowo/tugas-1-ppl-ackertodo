<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: groups.inc,v 1.16 2006/09/01 16:15:45 zoidian Exp $
*/
?>
<?php
/* This function lists all of the groups. */
function disp_groups($login, $select_group, $add_magic = true, $task = false) {
	global $isadmin, $table_prefix;

    $first = 1;

	if ($add_magic) {
		$magic = 'group_';
	} else {
		$magic = '';
	}

	if (isset($login) || !$isadmin) {
		$result = db_query("SELECT id, name FROM ".$table_prefix."groups, ".$table_prefix."group_membership"
			. " WHERE ".$table_prefix."groups.id = ".$table_prefix."group_membership.group_id AND ".$table_prefix."group_membership.login = '$login'"
			. " ORDER BY name");
	} else {
		$result = db_query("SELECT id, name FROM ".$table_prefix."groups ORDER BY name");
	}

	while($myrow = mysql_fetch_array($result)) {
        if($first == 1) {
            $first_id = $myrow['id'];
            $first = 0;
        }

        if($task) {
		    echo '<option value="' . $magic . $myrow['id'] . '"'. (isset($select_group) && $select_group == ($magic . $myrow['id'])?' selected="selected"':'') . '>' . $myrow['name']. " ("._GROUP.")</option>\n";
        } else {
		    echo '<option value="' . $magic . $myrow['id'] . '"'. (isset($select_group) && $select_group == ($magic . $myrow['id'])?' selected="selected"':'') . '>' . $myrow['name']. "</option>\n";
        }
	}

    return $first_id;
}

function disp_group_members($group) {
	$group_members_array = get_group_members_array($group);
	$users_array = get_users_login_array();

	foreach($users_array as $user) {
		echo '<option value="' . $user . '"' . (in_array($user, $group_members_array)?' selected="selected"':'') . '>' . $user . "</option>\n";
	}
}

/* This function removes a user from a group. */
function remove_user_from_group($login, $group) {
	global $table_prefix;

	$result = db_query("DELETE FROM ".$table_prefix."group_membership WHERE login='$login' AND group_id='$group'");
}

/* This function add a user to a group. */
function add_user_to_group($login, $group) {
	global $table_prefix;
	$result = db_query("INSERT ".$table_prefix."group_membership (login, group_id) VALUES ('$login', '$group')");
}

/* This function removes a group. */
function remove_group($group) {
	global $table_prefix;

	$result = db_query("DELETE FROM ".$table_prefix."group_membership WHERE group_id='$group'");
	$result = db_query("DELETE FROM ".$table_prefix."groups WHERE id='$group'");
}

function get_groups_and_users_array() {
	global $table_prefix;

	$groups_and_users_array = array();

	$users = get_users_login_array();

	// add to the list those groups that have users
	$result = db_query("SELECT ".$table_prefix."groups.id as group_id, ".$table_prefix."groups.name as name, ".$table_prefix."group_membership.login as login FROM ".$table_prefix."groups, ".$table_prefix."group_membership"
		. " WHERE ".$table_prefix."groups.id = ".$table_prefix."group_membership.group_id"
		. " ORDER BY name");

	while ($myrow = mysql_fetch_array($result)) {
		$gid = $myrow['group_id'];
		$login = $myrow['login'];

		if (!isset($groups_and_users_array[$gid])) {
			$group = array();

			$group['id'] = $gid;
			$group['name'] = $myrow['name'];
			$group['users_in'][] = $myrow['login'];
			$group['users_in_hash'][$login] = $login;

			$groups_and_users_array[$gid] = $group;
		} else {
			$groups_and_users_array[$gid]['users_in'][] = $login;
			$groups_and_users_array[$gid]['users_in_hash'][$login] = $login;
		}
	}

	// populate list of users not in group
	foreach (array_keys($groups_and_users_array) as $gid) {
		foreach ($users as $user) {
			if (!isset($groups_and_users_array[$gid]['users_in_hash'][$user])) {
				$groups_and_users_array[$gid]['users_out'][] = $user;
			}
		}
	}

	// add the groups that have no users
	$result = db_query("SELECT ".$table_prefix."groups.id as group_id, ".$table_prefix."groups.name as name FROM ".$table_prefix."groups"
		. ' ORDER BY name');

	while ($myrow = mysql_fetch_array($result)) {
		$gid = $myrow['group_id'];

		if (!isset($groups_and_users_array[$gid])) {
			$group = array();

			$group['id'] = $gid;
			$group['name'] = $myrow['name'];
			$group['users_out'] = $users;

			$groups_and_users_array[$gid] = $group;
		}
	}

	return $groups_and_users_array;
}

/* This function is used to list all of the groups, and the users in
 * them. It also adds a button for each user in the group to allow
 * them to be removed. */
function get_groups() {
	global $table_prefix;
	return db_query("SELECT * FROM ".$table_prefix."groups ORDER BY name");
}

function get_group_members_array($group_id) {
	global $table_prefix;

	$group_members_array = array();

	$result = db_query("SELECT login FROM ".$table_prefix."group_membership WHERE group_id='$group_id'");

	while ($myrow = mysql_fetch_array($result)) {
		$group_members_array[] = $myrow['login'];
	}

	return $group_members_array;
}

function get_group_name($group_id) {
	global $table_prefix;
	$group_array = array();

	$result = db_query("SELECT name FROM ".$table_prefix."groups WHERE id='$group_id'");

	if (mysql_num_rows($result) == 1) {
		$myrow = mysql_fetch_array($result);
		return $myrow['name'];
	} else {
		return;
	}
}

function set_group_name($group_id, $group_name) {
	global $table_prefix;
	db_query("UPDATE ".$table_prefix."groups SET name='$group_name' WHERE id='$group_id'");
}

function set_group_members($group_id, $members) {
	global $table_prefix;
	db_query("DELETE FROM ".$table_prefix."group_membership WHERE group_id='$group_id'");

	foreach ($members as $member) {
		add_user_to_group($member, $group_id);
	}
}
?>
