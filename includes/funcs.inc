<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: funcs.inc,v 1.68 2006/09/18 19:56:54 zoidian Exp $
*/
?>
<?php
global $host, $user, $pass, $database, $version;

/* This function is used to take in a date, and return the correct
 * month and day, both in dropdown boxes. */
function disp_date($date) {
    global $mysql_version;
    $cur_year = date("Y");
    
    $year = substr($date, 0, 4);
    $month = substr($date, 4, 2);
    $day = substr($date, 6, 2);

	echo "<select name=\"month\" size=\"1\" class=\"btn\">\n";
	if($month == "01")
    	echo "<option value=\"01\" selected>"._JANUARY."</option>\n";
	else
	    echo "<option value=\"01\">"._JANUARY."</option>\n";
	if($month == "02")
    	echo "<option value=\"02\" selected>"._FEBUARY."</option>\n";
	else
	    echo "<option value=\"02\">"._FEBUARY."</option>\n";
	if($month == "03")
    	echo "<option value=\"03\" selected>"._MARCH."</option>\n";
	else
	    echo "<option value=\"03\">"._MARCH."</option>\n";
	if($month == "04")
    	echo "<option value=\"04\" selected>"._APRIL."</option>\n";
	else
	    echo "<option value=\"04\">"._APRIL."</option>\n";
	if($month == "05")
    	echo "<option value=\"05\" selected>"._MAY."</option>\n";
	else
	    echo "<option value=\"05\">"._MAY."</option>\n";
	if($month == "06")
    	echo "<option value=\"06\" selected>"._JUNE."</option>\n";
	else
	    echo "<option value=\"06\">"._JUNE."</option>\n";
	if($month == "07")
    	echo "<option value=\"07\" selected>"._JULY."</option>\n";
	else
	    echo "<option value=\"07\">"._JULY."</option>\n";
	if($month == "08")
    	echo "<option value=\"08\" selected>"._AUGUST."</option>\n";
	else
	    echo "<option value=\"08\">"._AUGUST."</option>\n";
	if($month == "09")
    	echo "<option value=\"09\" selected>"._SEPTEMBER."</option>\n";
	else
	    echo "<option value=\"09\">"._SEPTEMBER."</option>\n";
	if($month == "10")
    	echo "<option value=\"10\" selected>"._OCTOBER."</option>\n";
	else
	    echo "<option value=\"10\">"._OCTOBER."</option>\n";
	if($month == "11")
    	echo "<option value=\"11\" selected>"._NOVEMBER."</option>\n";
	else
	    echo "<option value=\"11\">"._NOVEMBER."</option>\n";
	if($month == "12")
    	echo "<option value=\"12\" selected>"._DECEMBER."</option>\n";
	else
	    echo "<option value=\"12\">"._DECEMBER."</option>\n";

	echo "</select>\n";

    disp_dom($date, "date");

    echo "<select name=\"year\" size=\"1\" class=\"btn\">\n";

    for($i = 2004; $i <= $cur_year + 3; $i++) {
        if($year == $i)
            echo "<option value=\"" . $i . "\" selected>" . $i . "</option>\n";
        else
            echo "<option value=\"" . $i . "\">" . $i . "</option>\n";
    }

	echo "</select>\n";
}

function disp_date_year($date) {
    global $mysql_version;
    $cur_year = date("Y");
    
    $year = substr($date, 0, 4);
    $month = substr($date, 4, 2);
    $day = substr($date, 6, 2);
    
    echo "<select name=\"year\" size=\"1\" class=\"btn\">\n";

    for($i = 2004; $i <= $cur_year + 3; $i++) {
        if($year == $i)
            echo "<option value=\"" . $i . "\" selected>" . $i . "</option>\n";
        else
            echo "<option value=\"" . $i . "\">" . $i . "</option>\n";
    }

	echo "</select>\n";
}

function disp_date_month($date) {
    
    $year = substr($date, 0, 4);
    $month = substr($date, 4, 2);
    $day = substr($date, 6, 2);

	echo "<select name=\"month\" size=\"1\" class=\"btn\">\n";
	if($month == "01")
    	echo "<option value=\"01\" selected>"._JANUARY."</option>\n";
	else
	    echo "<option value=\"01\">"._JANUARY."</option>\n";
	if($month == "02")
    	echo "<option value=\"02\" selected>"._FEBUARY."</option>\n";
	else
	    echo "<option value=\"02\">"._FEBUARY."</option>\n";
	if($month == "03")
    	echo "<option value=\"03\" selected>"._MARCH."</option>\n";
	else
	    echo "<option value=\"03\">"._MARCH."</option>\n";
	if($month == "04")
    	echo "<option value=\"04\" selected>"._APRIL."</option>\n";
	else
	    echo "<option value=\"04\">"._APRIL."</option>\n";
	if($month == "05")
    	echo "<option value=\"05\" selected>"._MAY."</option>\n";
	else
	    echo "<option value=\"05\">"._MAY."</option>\n";
	if($month == "06")
    	echo "<option value=\"06\" selected>"._JUNE."</option>\n";
	else
	    echo "<option value=\"06\">"._JUNE."</option>\n";
	if($month == "07")
    	echo "<option value=\"07\" selected>"._JULY."</option>\n";
	else
	    echo "<option value=\"07\">"._JULY."</option>\n";
	if($month == "08")
    	echo "<option value=\"08\" selected>"._AUGUST."</option>\n";
	else
	    echo "<option value=\"08\">"._AUGUST."</option>\n";
	if($month == "09")
    	echo "<option value=\"09\" selected>"._SEPTEMBER."</option>\n";
	else
	    echo "<option value=\"09\">"._SEPTEMBER."</option>\n";
	if($month == "10")
    	echo "<option value=\"10\" selected>"._OCTOBER."</option>\n";
	else
	    echo "<option value=\"10\">"._OCTOBER."</option>\n";
	if($month == "11")
    	echo "<option value=\"11\" selected>"._NOVEMBER."</option>\n";
	else
	    echo "<option value=\"11\">"._NOVEMBER."</option>\n";
	if($month == "12")
    	echo "<option value=\"12\" selected>"._DECEMBER."</option>\n";
	else
	    echo "<option value=\"12\">"._DECEMBER."</option>\n";

	echo "</select>";
}

/* This function shows the days of the week S-S. */
function disp_dow($date) {
    global $mysql_version;

    if($mysql_version == 0) {
    	$day = substr($date, 6, 2);
    } elseif($mysql_version == 1) {
        $day = substr($date, 8, 2);
    }

	echo "<select name=\"dow\" size=\"1\" class=\"btn\">\n";
    if($day == 10)
    	echo "<option value=\"10\" selected>"._SUNDAY."</option>\n";
    else
    	echo "<option value=\"10\">"._SUNDAY."</option>\n";
    if($day == 11)
	    echo "<option value=\"11\" selected>"._MONDAY."</option>\n";
    else
	    echo "<option value=\"11\">"._MONDAY."</option>\n";
    if($day == 12)
    	echo "<option value=\"12\" selected>"._TUESDAY."</option>\n";
    else
    	echo "<option value=\"12\">"._TUESDAY."</option>\n";
    if($day == 13)
	    echo "<option value=\"13\" selected>"._WEDNESDAY."</option>\n";
    else
	    echo "<option value=\"13\">"._WEDNESDAY."</option>\n";
    if($day == 14)
    	echo "<option value=\"14\" selected>"._THURSDAY."</option>\n";
    else
    	echo "<option value=\"14\">"._THURSDAY."</option>\n";
    if($day == 15)
	    echo "<option value=\"15\" selected>"._FRIDAY."</option>\n";
    else
	    echo "<option value=\"15\">"._FRIDAY."</option>\n";
    if($day == 16)
    	echo "<option value=\"16\" selected>"._SATURDAY."</option>\n";
    else
    	echo "<option value=\"16\">"._SATURDAY."</option>\n";
    echo "</select>\n";
}

function disp_dom($date, $how) {
    global $mysql_version;
    
    $year = substr($date, 0, 4);
    $month = substr($date, 4, 2);
    $day = substr($date, 6, 2);

    if($how == "date") {
    	echo "<select name=\"day\" size=\"1\" class=\"btn\">\n";
    } elseif($how == "monthly") {
    	echo "<select name=\"mday\" size=\"1\" class=\"btn\">\n";
    }

	/*if($day == "01")
    	echo "<option value=\"01\" selected>1</option>\n";
	else
	    echo "<option value=\"01\">1</option>\n";
	if($day == "02")
    	echo "<option value=\"02\" selected>2</option>\n";
	else
	    echo "<option value=\"02\">2</option>\n";
	if($day == "03")
    	echo "<option value=\"03\" selected>3</option>\n";
	else
	    echo "<option value=\"03\">3</option>\n";
	if($day == "04")
    	echo "<option value=\"04\" selected>4</option>\n";
	else
	    echo "<option value=\"04\">4</option>\n";
	if($day == "05")
    	echo "<option value=\"05\" selected>5</option>\n";
	else
	    echo "<option value=\"05\">5</option>\n";
	if($day == "06")
    	echo "<option value=\"06\" selected>6</option>\n";
	else
	    echo "<option value=\"06\">6</option>\n";
	if($day == "07")
    	echo "<option value=\"07\" selected>7</option>\n";
	else
	    echo "<option value=\"07\">7</option>\n";
	if($day == "08")
    	echo "<option value=\"08\" selected>8</option>\n";
	else
	    echo "<option value=\"08\">8</option>\n";
	if($day == "09")
    	echo "<option value=\"09\" selected>9</option>\n";
	else
	    echo "<option value=\"09\">9</option>\n";*/

	for($i = 1; $i <= 31; $i++) {
		if($day == $i)
    		echo "<option value=\"".($i>9?$i:('0'.$i))."\" selected>$i</option>\n";
		else
	    	echo "<option value=\"".($i>9?$i:('0'.$i))."\">$i</option>\n";
	}

	echo "</select>\n";
}

/* This function formats a time in the form of HHMMSS to a much
 * easier form, along with an AM/PM. */
function format_time($time) {
	$hour = substr($time, 0, 2);
	$min = substr($time, 2, 2);
	$sec = substr($time, 4, 2);

	if($hour == "01" || $hour == "13")
    	$new_hour = 1;
	elseif($hour == "02" || $hour == "14")
	    $new_hour = 2;
	elseif($hour == "03" || $hour == "15")
    	$new_hour = 3;
	elseif($hour == "04" || $hour == "16")
	    $new_hour = 4;
	elseif($hour == "05" || $hour == "17")
    	$new_hour = 5;
	elseif($hour == "06" || $hour == "18")
	    $new_hour = 6;
	elseif($hour == "07" || $hour == "19")
    	$new_hour = 7;
	elseif($hour == "08" || $hour == "20")
	    $new_hour = 8;
	elseif($hour == "09" || $hour == "21")
    	$new_hour = 9;
	elseif($hour == "10" || $hour == "22")
	    $new_hour = 10;
	elseif($hour == "11" || $hour == "23")
    	$new_hour = 11;
	elseif($hour == "00" || $hour == "24")
	    $new_hour = 12;

	if($hour >= 12)
    	$a_or_p = "PM";
	else
	    $a_or_p = "AM";

	$final_date = $new_hour . ":" . $min . ":" . $sec . " " . $a_or_p;
	return $final_date;
}

/* This function takes a date and returns the month, day, and year. */
function format_date($date) {
    global $mysql_version;
    
    if($mysql_version == 0) {
    	$year = substr($date, 0, 4);
    	$month = substr($date, 4, 2);
    	$day = substr($date, 6, 2);
    } elseif($mysql_version == 1) {
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);
        $day = substr($date, 8, 2);
    }

	if($month == "01")
    	$month = _JANUARY;
	elseif($month == "02")
	    $month = _FEBUARY;
	elseif($month == "03")
    	$month = _MARCH;
	elseif($month == "04")
	    $month = _APRIL;
	elseif($month == "05")
    	$month = _MAY;
	elseif($month == "06")
	    $month = _JUNE;
	elseif($month == "07")
    	$month = _JULY;
	elseif($month == "08")
	    $month = _AUGUST;
	elseif($month == "09")
    	$month = _SEPTEMBER;
	elseif($month == "10")
	    $month = _OCTOBER;
	elseif($month == "11")
    	$month = _NOVEMBER;
	elseif($month == "12")
	    $month = _DECEMBER;

	if($day == "01")
    	$day = 1;
	elseif($day == "02")
	    $day = 2;
	elseif($day == "03")
    	$day = 3;
	elseif($day == "04")
	    $day = 4;
	elseif($day == "05")
    	$day = 5;
	elseif($day == "06")
	    $day = 6;
	elseif($day == "07")
    	$day = 7;
	elseif($day == "08")
	    $day = 8;
	elseif($day == "09")
    	$day = 9;

	return array($year, $day, $month);
}

function format_date_simple($date, $date_format = 0) {
    global $mysql_version;

    if($mysql_version == 0) {
    	$year = substr($date, 0, 4);
	    $month = substr($date, 4, 2);
    	$day = substr($date, 6, 2);
    } elseif($mysql_version == 1) {
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);
        $day = substr($date, 8, 2);
    }

    switch($date_format) {
        case '0': /* MM/DD/YYYY */
            $format = "$month/$day/$year";
            break;
        case '1': /* MM/DD/YY */
            $year = substr($year, 2, 2);
            $format = "$month/$day/$year";
            break;
        case '2': /* DD/MM/YYYY */
            $format = "$day/$month/$year";
            break;
        case '3': /* DD/MM/YY */
            $year = substr($year, 2, 2);
            $format = "$day/$month/$year";
            break;
        case '4': /* MM-DD-YYYY */
            $format = "$month-$day-$year";
            break;
        case '5': /* MM-DD-YY */
            $year = substr($year, 2, 2);
            $format = "$month-$day-$year";
            break;
        case '6': /* DD-MM-YYYY */
            $format = "$day-$month-$year";
            break;
        case '7': /* DD-MM-YY */
            $year = substr($year, 2, 2);
            $format = "$day-$month-$year";
            break;
        case '8': /* YYYY-MM-DD */
            $format = "$year-$month-$day";
            break;
        default:
            $format = "$month-$day-$year";
            break;
    }
    
    return $format;
}

/* This function displays a view */
function show_view($view) {
    if($view != "popcal") {
    	include('html/header.html');
    }

	if (is_array($view)) {
		foreach ($view as $v) {
			include_view($v);
		}
	} else {
		include_view($view);
	}

	debug('Session contents: ' . print_r($_SESSION, true));
    
    if($view != "popcal") {
    	include('html/footer.html');
    }
}

/* This function displays a view */
function include_view($view) {
	// todo: error checking
	debug("Displaying view: $view");
	include("html/$view.html");
}

function show_default_view() {
	show_view('list_tasks');
}

function disp_priority($priority, $title = null, $name = 'priority', $suffix = '') {
	global $_PRIORITY;

	if (!isset($title)) {
		$title = _PRIORITY.'&nbsp;';
	}

	echo "$title<select name=\"$name\" size=\"1\" class=\"btn\" $suffix>\n";

	foreach ($_PRIORITY as $k => $v) {
		$selected = ($priority == $k)?' selected="selected"':'';

		echo "<option value=\"$k\"$selected>$v</option>\n";
	}

	echo "</select>\n";
}

/* Use this method to get a value from the request or the session, to keep
 * some values in the session and not require them to be sent on the URL
 * every time (used for task list state, for example) */
function get_value_session_request($name) {
	if (isset($_REQUEST[$name])) {
		$_SESSION[$name] = $_REQUEST[$name];
		return $_REQUEST[$name];
	} elseif (isset($_SESSION[$name])) {
		return $_SESSION[$name];
	}
}

/* Use this method to get a value from the output, request or the session, to keep
 * some values in the session and not require them to be sent on the URL
 * every time (used for task list state, for example) */
function get_value_session_request_output($name) {
	global $output;

	if (isset($output[$name])) {
		$_SESSION[$name] = $output[$name];
		return $output[$name];
	} elseif (isset($_REQUEST[$name])) {
		$_SESSION[$name] = $_REQUEST[$name];
		return $_REQUEST[$name];
	} elseif (isset($_SESSION[$name])) {
		return $_SESSION[$name];
	}
}

/* We should use this function to redirect after a post, so the request is
 * not submitted again if the user clicks Reload. Any message for the user 
 * can be displayed by storing it in $output['error'] or $output['message'] */
function redirect_on_post($relative_url = 'index.php') {
	global $output, $debug_output;

	debug("Redirect on post to $relative_url");

	// preserve message and/or error in session: we won't get to show it this time around...
	if (isset($output['message'])) {
		$_SESSION['message'] = $output['message'];
	}
	if (isset($output['error'])) {
		$_SESSION['error'] = $output['error'];
	}
	if (isset($output['next_view'])) {
		$_SESSION['next_view'] = $output['next_view'];
	}

	debug('Session contents: ' . print_r($_SESSION, true));

	if (isset($debug_output) && $debug_output) {
		$_SESSION['debug_output'] = $debug_output;
	}

	session_write_close();

	// send redirect
    if ($_SERVER['HTTPS'] == 'on') {
        header("Location: https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/" . $relative_url);
    } else {
        header("Location: http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/" . $relative_url);
    }

	// what more could we do?
	exit;
}

function load_language() {
	global $language, $default_language;
	
	if(!file_exists("includes/languages/$language.inc")) {
		include_once("includes/languages/$default_language.inc");
	} else {
		include_once("includes/languages/$language.inc");
	}

    //include_once("includes/languages/english.inc");
}

function debug($msg, $level=null) {
	global $debug, $debug_output;

	if ($debug) {
		if ($level) {
			$debug_output .= "<p class=\"$level\">$msg</p>\n";
		} else {
			$debug_output .= "<p>$msg</p>\n";
		}
	}

	// include_once("includes/languages/english.inc");
}

function error_handler($errno, $errstr, $errfile, $errline)
{
	switch ($errno) {
		case E_USER_ERROR:
			echo "<b>FATAL</b> [$errno] $errstr<br />\n";
			echo "  Fatal error in line $errline of file $errfile";
			echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
			echo "Aborting...<br />\n";
			exit(1);
			break;
		case E_USER_WARNING:
			debug("<b>ERROR</b> [$errno] $errstr in $errfile, line $errline", 'error');
			break;
		case E_USER_NOTICE:
			debug("<b>WARNING</b> [$errno] $errstr in $errfile, line $errline", 'error');
			break;
		default:
			debug("Unkown error type: [$errno] $errstr in $errfile, line $errline", 'error');
			break;
	}
}

function list_dir_modules() {
    $modules = NULL;
    $i = 0;
    
    if($handle = opendir('modules')) {
        while(false !== ($file = readdir($handle))) {
            if($file != '.' && $file != '..' && $file != 'CVS' && $file != 'index.php') {
                if($file != '') {
                    $modules[$i] = $file;
                    $i++;
                }
            }
        }
    }

    return $modules;
}

function list_db_modules() {
	global $table_prefix;
    $modules = NULL;
    $i = 0;
    
    $result = db_query("SELECT * FROM ".$table_prefix."modules");

    while($myrow = mysql_fetch_array($result)) {
        $modules[$i] = $myrow['name'];
        $i++;
    }

    return $modules;
}

function list_enabled_modules() {
	global $table_prefix;
    $modules = NULL;
    $i = 0;

    $result = db_query("SELECT * FROM ".$table_prefix."modules "
                      ."WHERE enabled='1'");

    while($myrow = mysql_fetch_array($result)) {
        $modules[$i] = $myrow['name'];
        $i++;
    }

    return $modules;
}

function get_module_info($module) {
	global $table_prefix;

    $result = db_query("SELECT * FROM ".$table_prefix."modules "
                      ."WHERE name='$module'");

    $myrow = mysql_fetch_array($result);

    return $myrow;
}

function get_module_description($module) {
    $description = file_get_contents("modules/$module/description.txt");

    return $description;
}

function get_active_modules($page) {
    $modules = list_enabled_modules();
    $disp_mod = NULL;
    $i = 0;

    for($j = 0; $j < sizeof($modules); $j++) {
        $file = "modules/" . $modules[$j] . "/module.inc";
        $contents = file_get_contents($file);
        $needle = "function " . $modules[$j] . "_" . $page;
        $exists = substr_count($contents, $needle);

        if($exists == 1) {
            $disp_mod[$i] = $modules[$j];
            $i++;
        }
    }

    return $disp_mod;
}

function get_module_config($module) {
    $file = "modules/" . $module . "/module.inc";
    $contents = file_get_contents($file);
    $needle = "function " . $module . "_disp_module_config";
    $exists = substr_count($contents, $needle);

    return $exists;
}

function export_xml($query) {
    $result = db_query($query);

    if(!$result) {
        die ('<p>Error performing query: ' . mysql_error() . '</p>');
    }

    header("Content-type: application/xml;");
    echo '<?xml version="1.0" encoding="utf-8" ?>
         <todo>';

    while($myrow = mysql_fetch_array($result)) {
        $category = $myrow['category_name'];
        $title = $myrow['title'];
        $task = $myrow['task'];
        $priority = $_PRIORITY[$myrow['priority']];
        list($status, $image) = get_status($myrow, 0);
        $recurrence = $myrow['recurrence'];
        $date = $myrow['date'];

        if($category == '') {
            $category = 'Undefined';
        }

        if($recurrence == '1') {
            $recurrence = 'Daily';
        } elseif($recurrence == '2') {
            $recurrence = 'Weekly';
        } elseif($recurrence == '3') {
            $recurrence = 'Monthly';
        } elseif($recurrence == '4') {
            $recurrence = 'Open-Ended';
        } elseif($recurrence == '5') {
            $recurrence = 'Yearly';
        } elseif($recurrence == '6') {
            $recurrence = 'Once';
        } elseif($recurrence == '0') {
            $recurrence = 'Undefined';
        }

        echo '
            <task>
                <category>'.strip_tags($category).'</category>
                <title>'.strip_tags($title).'</title>
                <description>'.strip_tags($task).'</description>
                <priority>'.$priority.'</priority>
                <status>'.$status.'</status>
                <recurrence>'.$recurrence.'</recurrence>
                <duedate>'.$date.'</duedate>
            </task>';
    }
    echo '</todo>';
}
?>
