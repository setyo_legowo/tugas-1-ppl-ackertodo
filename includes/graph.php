<?php
/* vim: set ts=4 sw=4 si: */
?>
<?php
class phpGraph {
   var $_values;
   var $_ShowCounts;
   var $_BarWidth;
   var $_GraphWidth;
   var $_BarBorderWidth;
   var $_BarBorderColor;
   var $_ShowCountsMode;

   function phpGraph() {
      $this->_values = array();
      $this->_ShowLabels = false;
      $this->_BarWidth = 16;
      $this->_GraphWidth = 360;
      $this->_BarBorderWidth = 0;  
      $this->_BarBorderColor = "red";  
      $this->_ShowCountsMode = 2;
   }

   function SetBarBorderWidth($width) {
      $this->_BarBorderWidth = $width;
   }
   function SetBorderColor($color) {
      $this->_BarBorderColor = $color;
   } 
 
   function AddValue($theValue) {
      array_push($this->_values, array("value" => $theValue));
   }
   
   function SetBarWidth($width) {
      $this->_BarWidth = $width;
   }
   
   function SetShowLabels($lables) {
      $this->_ShowLabels = $labels;
   }
   
   function SetGraphWidth($width) {
      $this->_GraphWidth = $width;
   }
 
   //mode = percentage or counts
   function SetShowCountsMode($mode) {
      $this->_ShowCountsMode = $mode;
   }
   
   function GetMaxVal() {
      $maxval = 0;
      foreach($this->_values as $value) {
         if($maxval<$value["value"]) {
            $maxval = $value["value"];
         }
      }
      return $maxval;
   }
   
   function BarGraphVert() {
      $maxval = $this->GetMaxVal();
      
      foreach($this->_values as $value) {
         $sumval += $value["value"];
      }
         
      echo "<table>\n";
      echo "<tr>\n";
      foreach($this->_values as $value) {
         echo "<td valign=bottom align=center>\n";
         $height = $this->_BarWidth;
         $width = ceil($value["value"]*$this->_GraphWidth/$maxval);
         echo "<img SRC=\"images/bar.gif\" height=$width width=$height ";
         echo ">\n";
         echo "</TD>\n";
      }
      echo "</TR>\n";
      
      if($this->_ShowCountsMode>0) {
   	   echo "<tr>";
         foreach($this->_values as $value) {
            switch ($this->_ShowCountsMode) {
               case 1:
                  $count = round(100*$value["value"]/$sumval)."%";
                  break;  
               case 2:
                  $count = $value["value"];
                  break;  /* Exit the switch and the while. */
               default:
                  break;
            }
         echo "<td align=\"center\">$count</TD>\n";
   	   }
	   echo "</tr>\n";
      }   

      if($this->_ShowLabels) { 
         echo "<tr>\n"; 
         foreach($this->_values as $value) {
            echo "<td align=\"center\">".$value["label"]."</TD>\n";
	      }
	      echo "</tr>\n"; 
      }	 

      echo "</TABLE>\n";   
   }

   function BarGraphHoriz() {
      $maxval = $this->GetMaxVal();
      
      foreach($this->_values as $value) {
         $sumval += $value["value"];
      }
      
      echo "<table border=\"0\">\n";
      foreach($this->_values as $value) {
         if($this->_ShowLabels) { 
            echo "<tr border=\"0\">\n"; 
            echo "<td";
            if($this->_ShowCountsMode>0) {
               echo " colspan=\"2\" border=\"0\"";
            }
	         echo ">\n".$value["label"]."</TD></TR>\n";
         }	 
         echo "<tr>\n";
         if($this->_ShowCountsMode>0) {
            switch($this->_ShowCountsMode) {
               case 1:
                  $count = round(100*$value["value"]/$sumval)."%";
                  break;  
               case 2:
                  $count = $value["value"];
                  break;  /* Exit the switch and the while. */
               default:
                  break;
            }
            echo "<td border=\"0\">$count</TD>\n";
         }
         echo "<td border=\"0\">\n";
         $height = $this->_BarWidth;
         if($maxval > 0) {
             $width = ceil($value["value"]*$this->_GraphWidth/$maxval);
         } else {
             $width = 0;
         }
         echo "<img SRC=\"images/bar.gif\" height=$height width=$width ";
         echo " style=\"border: ".$this->_BarBorderWidth."px solid ".$this->_BarBorderColor."\"";
         echo ">\n";
         echo "</TD></TR>\n";
      }
      echo "</TABLE>\n";
   }
}
?>
