<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: cli.inc,v 1.9 2006/09/23 21:09:34 zoidian Exp $
*/
?>
<?php
function list_users($how = NULL) {
	global $table_prefix;
    $i = 1;

    $result = db_query("SELECT * FROM ".$table_prefix."users");

    echo "All Users\n";
    echo "=========\n\n";
    while($myrow = mysql_fetch_array($result)) {
        if($myrow['admin'] == 1) {
            $admin = "(Admin)";
        } else {
            $admin = "";
        }

        echo $i . ".) Login:  " . $myrow['login'] . " " . $admin . "\n";

        if($myrow['first'] || $myrow['last']) {
            echo "     Name:  " . $myrow['first'] . " " . $myrow['last'] . "\n";
        }
        
        if($myrow['email']) {
            echo "    Email:  " . $myrow['email'] . "\n";
        }

        echo "\n";
        $i++;
    }
}

function print_help() {
    echo "ackerTodo Rob Hensley <hensleyrob@nku.edu>\n";
    echo "Usage: php cmd.php <list_tasks|add_task|del_task\n";
    echo "                    list_users|add_user|del_user\n";
    echo "                    add_category|del_category>\n";
    echo "      list_tasks:  List your currently open tasks.\n";
    echo "        add_task:  Add a new task.\n";
    echo "        del_task:  Delete a task.\n";
    echo "      list_users:  List all of the users.\n";
    echo "        add_user:  Add a new user. (Admin Only)\n";
    echo "        del_user:  Delete a user. (Admin Only)\n";
    echo "    add_category:  Add a category.\n";
    echo "    del_category:  Delete a category.\n";
    echo "Example: php cmd.php list_tasks\n";
}

function list_tasks($login) {
	global $table_prefix;

    $i = 1;
    
    $result = db_query("SELECT * FROM ".$table_prefix."tasks "
                      ."WHERE login='$login' "
                      ."AND completed='0'");

    echo "Currently Open Tasks\n";
    echo "====================\n\n";
    while($myrow = mysql_fetch_array($result)) {
        $date = format_date_simple($myrow['date']);
        echo $i . ".) Title:  " . $myrow['title'] . "\n";
        echo "     Date:  " . $date . "\n";
        echo "\n";
        $i++;
    }
}

function add_task($login) {
	global $table_prefix;

    $date = date("Ymd");
    $today = date("YmdHms");

    while(!$title) {
        echo "Title: ";
        $title = trim(fgets(STDIN));
    }

    echo "Due Date (".$date."): ";
    $temp_date = trim(fgets(STDIN));
    $temp_date = trim($temp_date);
    
    if(is_numeric($temp_date) && strlen($temp_date) == "8") {
        $date = $temp_date;
    }

    while(!$task) {
        echo "Task: ";
        $task = trim(fgets(STDIN));
    }

    echo "Priority (0-3): ";
    $priority = trim(fgets(STDIN));
    if($priority != "0" || $priority != "1" ||
        $priority != "2" || $priority != "3") {
        $priority = "0";
    }   

    echo "No Recurrece/Daily/Weekly/Monthly/Yearly/Open-Ended (N/d/w/m/y/o): ";
    $recurrence = trim(fgets(STDIN));
    if($recurrence == "d" || $recurrence == "D") {
        $recurrence = 1;
    } elseif($recurrence == "w" || $recurrence == "W") {
        $recurrence = 2;
    } elseif($recurrence == "m" || $recurrence == "M") {
        $recurrence = 3;
    } elseif($recurrence == "y" || $recurrence == "Y") {
        $recurrence = 5;
    } elseif($recurrence == "o" || $recurrence == "O") {
        $recurrence = 4;
    } else {
        $recurrence = 0;
    }

    $result = db_query("INSERT INTO ".$table_prefix."tasks "
                      ."(login,creationdate,recurrence,title,task,"
                      ."date,assignedby,category,priority) "
                      ."VALUES ('$login','$today','$recurrence','$title',"
                      ."'$task','$date','$login','0','$priority')");

//    if(!$result || mysql_num_rows($result) != 1) {
//        echo "Failed To Add Task!\n";
//        exit;
//    }

    echo "Task Added Successfully!\n";
}

function del_task($login) {
	global $table_prefix;

    $result = db_query("SELECT * FROM ".$table_prefix."tasks "
                      ."WHERE login='$login' "
                      ."ORDER by id");
    
    echo "All Tasks\n";
    echo "=========\n\n";
    while($myrow = mysql_fetch_array($result)) {
        echo $myrow['id'] . ".) Title:  " . $myrow['title'] . "\n";
    }

    while(!$delete) {
        echo "\nEnter the number of the task to delete: ";
        $delete = trim(fgets(STDIN));
    }

    $result = db_query("DELETE FROM ".$table_prefix."tasks "
                      ."WHERE id='$delete'");

//    if(!$result || mysql_num_rows($result) != 1) {
//        echo "Failed To Delete Task!\n";
//        exit;
//    }

    echo "Task Deleted Successfully!\n";
}

function del_user($login) {
	global $table_prefix;

    while(!$delete) {
        echo "Enter the login name of the user to delete: ";
        $delete = trim(fgets(STDIN));
    }

    $result = db_query("DELETE FROM ".$table_prefix."users "
                      ."WHERE login='$delete'");

//    if(!$result || mysql_num_rows($result) != 1) {
//          echo "Failed To Delete User!\n";
//          exit;
//    }

    echo "User Deleted Successfully!\n";
}

function add_category($login) {
	global $table_prefix;

    while(!$new_cat || $exists == 1) {
        if($exists == 1) {
            echo "Category already exists!\n";
        }

        echo "Category Name: ";
        $new_cat = trim(fgets(STDIN));

        $result = db_query("SELECT * FROM ".$table_prefix."category "
                          ."WHERE login='$login' "
                          ."AND category='$new_cat'");

        if(mysql_num_rows($result) == 1) {
            $exists = 1;
        } else {
            $exists = 0;
        }
    }

    $result = db_query("INSERT INTO ".$table_prefix."category "
                      ."(login,name)"
                      ."VALUES ('$login','$new_cat')");

//    if(!$result || mysql_num_rows($result) != 1) {
//          echo "Failed To Add Category!\n";
//          exit;
//    }

    echo "Category Added Successfully!\n";
}

function del_category($login) {
	global $table_prefix;

    while(!$delete) {
        echo "Enter the category name to delete: ";
        $delete = trim(fgets(STDIN));
    }

    $result = db_query("DELETE FROM ".$table_prefix."category "
                      ."WHERE login='$login' "
                      ."AND name='$delete'");

//    if(!$result || mysql_num_rows($result) != 1) {
//          echo "Failed To Delete Category!\n";
//          exit;
//    }

    echo "Category Deleted Successfully!\n";
}

function list_categories($login) {
	global $table_prefix;

    $i = 1;
    
    $result = db_query("SELECT * FROM ".$table_prefix."category "
                      ."WHERE login='$login'");

    echo "All Your Categories\n";
    echo "===================\n\n";
    while($myrow = mysql_fetch_array($result)) {
        echo $i . ".) " . $myrow['name'] . "\n";
        $i++;
    }

    echo "\n";
}

function add_user() {
	global $table_prefix;

    echo "New User Information\n";
    echo "====================\n";

    while(!$new_login || $exists == 1) {
        if($exists == 1) {
            echo "User already exists!\n";
        }

        echo "Login: ";
        $new_login = trim(fgets(STDIN));

        $result = db_query("SELECT * FROM ".$table_prefix."users "
                          ."WHERE login='$new_login'");

        if(mysql_num_rows($result) == 1) {
            $exists = 1;
        } else {
            $exists = 0;
        }
    }
    
    echo "First Name: ";
    $new_first = trim(fgets(STDIN));

    echo "Last Name: ";
    $new_last = trim(fgets(STDIN));

    echo "E-Mail: ";
    $new_email = trim(fgets(STDIN));

    echo "AIM Screenname: ";
    $new_screenname = trim(fgets(STDIN));

    echo "Administrator (y/N): ";
    $new_admin = trim(fgets(STDIN));
    
    if($new_admin == "y" || $new_admin == "Y") {
        $new_admin = "1";
    } else {
        $new_admin = "0";
    }
    
    $i = 0;
    while(!$new_pass || !$new_pass_ver || $match == 0) {
        if($i > 0 && $match == 0) {
            echo "Passwords don't match!\n";
        }

        echo "Password: ";
        system("stty -echo");
        $new_pass = trim(fgets(STDIN));
        system("stty echo");
        echo "\n";

        echo "Verify Password: ";
        system("stty -echo");
        $new_pass_ver = trim(fgets(STDIN));
        system("stty echo");
        echo "\n";

        if($new_pass == $new_pass_ver) {
            $match = 1;
        } else {
            $match = 0;
        }
        
        $i++;
    }

    $result = db_query("INSERT INTO ".$table_prefix."users "
                      ."(login,password,first,last,email,"
                      ."screenname,admin) "
                      ."VALUES ('$new_login','$new_pass',"
                      ."'$new_first','$new_last','$new_email',"
                      ."'$new_screenname','$new_admin')");

//    if(!$result || mysql_num_rows($result) != 1) {
//        echo "Failed To Add User!\n";
//        exit;
//    }

      echo "User Added Successfully!\n";
}

function login() {
	global $table_prefix;

    while(!$login) {
        echo "Login: ";
        $login = trim(fgets(STDIN));
    }

    echo "Password: ";
    system("stty -echo");
    $pass = trim(fgets(STDIN));
    system("stty echo");
    $enc_pass = md5($pass);
    echo "\n";

    $result = db_query("SELECT * FROM ".$table_prefix."users "
                      ."WHERE login='$login' "
                      ."AND password='$enc_pass'");

    $myrow = mysql_fetch_array($result);
    $isadmin = $myrow['admin'];

    if(!$result || mysql_num_rows($result) != 1) {
        echo "Login Failed!\n";
        exit;
    }

    if($choice == "add_user" || $choice == "del_user") {
        if(!$isadmin == 1) {
            echo "You're Not An Admin!\n\n";
            exit;
        }
    }

    echo "Login Successful!\n\n";
    
    
    return $login;
}
?>
