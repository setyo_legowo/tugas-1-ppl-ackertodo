<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2006 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* Translated into Russian by Sheon (for ackerTodo v3.6)
*
* $Id: russian.inc,v 1.1 2006/04/21 02:52:56 zoidian Exp $
*/
?>
<?php
	/* This is the Russian language file where you define all the strings
	* used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "�������� �������");
	define("_THIS_WEEK", "������� ���� ������");

    /* These are for the Status drop down. */
	define("_TASKS_DAILY", "������� �� ����");
	define("_TASKS_OPEN_ENDED", "�������������� �������");
	define("_TASKS_WEEKLY", "������� �� ������");
	define("_TASKS_MONTHLY", "������� �� �����");
	define("_TASKS_YEARLY", "������� �� ���");
	define("_TASKS_OPEN", "�������� �������");
	define("_TASKS_COMPLETED", "����������� �������");
	define("_TASKS_OVERDUE", "������������ �������");
	define("_TASKS_ALL", "��� �������");
	define("_TASKS_THIS_WEEK", "������� ���� ������");
	define("_SEARCH_EVERYONES", "������� ���� ������ �������");
	define("_SEARCH_MINE", "��� ��� �������");
	define("_SEARCH_USERS", "'�� �������");
	define("_TASKS_DATE", "��� ");
    
    /* These are for the Main Menu. */
	define("_NEW_TASK", "����� �������");
	define("_CALENDAR", "���������");
	define("_ADMIN_MENU", "���� ������");
	define("_USER_MENU", "���� ������������");
	define("_LOG_OUT", "�����");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "������ ����� ����� ��� �������!");
	define("_EDIT_GROUPS", "������������� ������");
	define("_LIST_USERS", "������ �������������");
	define("_LIST_GROUPS", "������ �����");
	define("_CHANGE_PASS", "�������� ������");
	define("_EDIT_INFO", "������������� ������� ������������");
	define("_LIST_CATEGORIES", "������ ���������");
	define("_REMOVE_CATEGORY", "������� ���������");
	define("_RENAME_CATEGORY", "������������� ���������");
	define("_USER_CATEGORY_NAME", "���������������� ��� ���������");
	define("_GLOBAL_CATEGORY_NAME", "����� ��� ���������");
	define("_ADMIN_COMMAND", "������� ������");
    define("_COMMAND", "�������");
	define("_COMMAND_ERROR", "��� ������� ��� � ��� ��� ����!");
    define("_MODULES", "������");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "������������� �������");
	define("_DELETE_TASK", "������� �������");
	define("_VIEW_TASK", "�������� �������");
	define("_ACCESS_DENIED", "��� �������");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "������� ������� ���������!");
	define("_TASK_UPDATED", "������� ������� ����������!");
	define("_TASK_DELETED", "������� ������� �������!");
	define("_TASK_OPENED", "������� ������� �������!");
	define("_TASK_CLOSED", "������� ������� �������!");
	define("_USER_ADDED", "������������ ������� ��������!");
	define("_USER_REMOVED", "������������ ������� ������!");
	define("_ENTER_USER", "�� ������ ������ ��� ������������!");
	define("_ENTER_COMMENT", "�� ������ ������ �����������!");
	define("_ENTER_TITLE", "�� ������ ������ ��������!");
	define("_PASS_CHANGED", "������ ������� �������!");
	define("_ENTER_GROUP", "�� ������ ������ �� ������!");
	define("_GROUP_ADDED", "������ ������� ���������!");
	define("_GROUP_REMOVED", "������ ������� �������!");
	define("_GROUP_UPDATED", "������ ������� ���������!");
	define("_REMOVE_YOURSELF", "�� �� ������ ������� ����!");
	define("_REMOVED_FROM_GROUP", "������������ ������� ������ �� ������!");
	define("_ADDED_TO_GROUP", "������������ ������� �������� � ������!");
	define("_COMMENT_ADDED", "����������� ������� ��������!");
	define("_COMMENT_UPDATED", "����������� ������� ��������!");
	define("_VERIFY_PASS", "�� ������ ��������� ������ ������������!");
	define("_PASS_ERROR", "������ �� ��������!");
	define("_INFO_UPDATED", "���������� ������� ���������!");
	define("_QUERY_ERROR", "������ � �������!");
	define("_COMMENT_REMOVED", "����������� ������!");
	define("_CATEGORY_CREATED", "��������� ������� ���������!");
	define("_CATEGORY_UPDATED", "��������� ������� ���������!");
	define("_CATEGORY_DELETED", "��������� ������� �������!");
	define("_ADDED_TO_SUPERCAT", "��������� �������� � supercat �������!");
	define("_SUPERCAT_REMOVED", "Supercat ������ �������!");
	define("_ENTER_CATEGORY", "�� ������ ������ ��� ���������!");
	define("_NO_ID", "������ ID ������ ���!");
	define("_ALREADY_USER", "��� ���� ������������ � ����� ������!");
	define("_ALREADY_GROUP", "��� ���� ������ � ����� ������!");
    define("_MODULE_ENABLE", "������ ������� ���������!");
    define("_MODULE_DISABLE", "������ ������� ��������!");
    define("_MODULE_INSTALL", "������ ������� ����������!");
    define("_MODULE_UNINSTALL", "������ ������� ������!");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "������");
	define("_FEBUARY", "�������");
	define("_MARCH", "����");
	define("_APRIL", "������");
	define("_MAY", "���");
	define("_JUNE", "����");
	define("_JULY", "����");
	define("_AUGUST", "������");
	define("_SEPTEMBER", "��������");
	define("_OCTOBER", "�������");
	define("_NOVEMBER", "������");
	define("_DECEMBER", "�������");
	define("_SUNDAY", "�����������");
	define("_MONDAY", "�����������");
	define("_TUESDAY", "�������");
	define("_WEDNESDAY", "�����");
	define("_THURSDAY", "�������");
	define("_FRIDAY", "�������");
	define("_SATURDAY", "�������");
	define("_SUN", "�����");
	define("_MON", "�����");
	define("_TUE", "����");
	define("_WED", "����");
	define("_THU", "����");
	define("_FRI", "����");
	define("_SAT", "����");

	/* Other various strings. */
        define("_CHANGE_DUE_DATE", "�������� ���� ���������");
        define("_VIEW_UPDATED", "���� ��� ���������� �� ���������!");
	define("_CATEGORY", "���������");
	define("_NO_CATEGORY", "��� ���������");
        define("_ALL_CATEGORIES", "��� ���������");
	define("_PRIORITY", "���������");
	define("_CREATION", "��������");
	define("_GROUP", "������");
	define("_ACTIONS", "��������");
	define("_WHOS_TASK", "��� �������");
        define("_WHO", "���");
	define("_USERS_IN_GROUP", "������������ � ������ (������ ����� �������)");
	define("_USERS_NOT_IN_GROUP", "������������ �� � ������ (������ ����� ��������)");
	define("_USER", "������������");
	define("_EMPTY", "�����!");
	define("_REMOVE", "�������");
	define("_COMPLETE", "���������");
	define("_OPEN", "�������");
	define("_OVERDUE", "�����������");
	define("_TODAY", "�������");
	define("_DAILY", "���������");
	define("_OPEN_ENDED", "��������������");
	define("_WEEKLY", "�����������");
	define("_MONTHLY", "����������");
	define("_YEARLY", "��������");
        define("_DATE", "����");
        define("_DAY", "����");
        define("_DAYS", "���");
	define("_NO_RECURRENCE", "�� ���������");
        define("_RECURRENCE", "������");
	define("_RECURRE_EVERY", "��������� ��������� �");
	define("_RECURRE_ON", "���������");
	define("_EVERY_MONTH", "������ �����");
	define("_STATUS", "������");
	define("_TITLE", "��������");
	define("_DEADLINE", "���������� ����");
	define("_TASK", "�������");
	define("_ASSIGNED_BY", "��������");
	define("_ASSIGN_TO", "���������");
	define("_COMPLETED_BY", "��������");
	define("_COMPLETE_TASK_BY", "���������");
	define("_COMMENT", "�����������");
	define("_COMMENT_BY", "�����");
	define("_COMMENT_DATE", "���� ��������");
	define("_UPDATE_TASK", "�������� �������");
	define("_CURRENT_PASS", "������� ������");
	define("_ADD", "��������");
	define("_FILTER", "������");
	define("_VIEW_TASKS", "�������� �������");
	define("_EDIT", "�������������");
	define("_DELETE", "�������");
        define("_ENABLE_AJAX", "�������� Ajax?");
	define("_ALLOW_OTHERS", "��������� ��������� ��������� ��� �������?");
	define("_PUBLIC_LIST", "��������� ��������� ������������� ��� ���� �������?");
	define("_UPDATE_INFO", "�������� ����������");
	define("_PREV", "����");
	define("_NEXT", "����");
	define("_YOUR_CURRENT_PASS", "�� ������ ������ ��� ������� ������!");
	define("_YOUR_NEW_PASS", "�� ������ ������ ����� ������!");
	define("_YOUR_VERIFY_PASS", "�� ������ ���������� ��� ����� ������!");
	define("_NEW_PASS_ERROR", "��� ����� ������ �� ��������� � �����������!");
	define("_CURRENT_PASS_ERROR", "��� ������� ������ �� ��������!");
    define("_IS_UNDEFINED", "�������������!");
    define("_AS_OF", " ��� ��� ");
    define("_PRINT", "������");
    define("_EXPORT", "�������");
    define("_SET_VIEW", "��� �� ���������");
    define("_DESCRIPTION", "��������");
    define("_DISABLE", "���������");
    define("_ENABLE", "��������");
    define("_INSTALL", "����������");
    define("_UNINSTALL", "�������");
    define("_MODULE", "������");
    define("_CONFIGURE", "������������");
    define("_CANCEL", "��������");
    define("_WHAT_WOULD_YOU_LIKE", "��� �� ������ ������?");
    define("_RESET_ALL", "�������� ��� ������� � '��� ���������'?");
    define("_REMOVE_ALL", "������� ��� ������� � ���������?");

	/* These are used when adding a new user or group. */
	define("_USER_NAME", "���/�������");
	define("_NEW_PASS", "����� ������");
	define("_VERIFY", "������������");
	define("_FIRST_NAME", "���");
	define("_LAST_NAME", "�������");
	define("_THEME", "�����");
	define("_LANGUAGE", "����");
	define("_DATE_FORMAT", "������ ����");
	define("_EMAIL", "Email");
	define("_AIM", "��� � �����������");
	define("_IS_ADMIN", "�������������?");
	define("_GROUP_NAME", "��� ������");
	define("_GROUP_MEMBERS", "����� ������");
	define("_UPDATE", "��������");
	define("_SELECT_GROUP", "������� ������");
	define("_FINISH_CREATE_USER", "�����. ����/������");
	define("_AUTO_DAILY", "����� ������� ��������� �� ���������?");

	/* These are for login stuff. */
	define("_LOGIN", "�����");
	define("_LOGON_SUCCESSFUL", "����� ���������");
	define("_PASSWORD", "������");
	define("_LOGGED_OUT", "�� �����");
	define("_KEEP_LOGIN", "��������� ����");
	define("_WRONG_PASSWORD", "������������ ����� ��� ������");
	
	/* Supercats */
	define("_SUPERCAT", "��������");
	define("_LIST_SUPERCATS", "������ ��������");
	define("_CATS_IN_SUPERCAT", "��������� � ��������");
	define("_REMOVED_FROM_SUPERCAT", "��������� ������� ������� �� ��������!");
	define("_CATS_NOT_IN_SUPERCAT", "��������� �� � ��������");
	define("_GROUP_SUPERCAT", "�������� ������� �������!");
	define("_ALREADY_SUPERCAT", "� ��������� ��� ���� ��� ���!");

    /* Email Notifications */
    define("_SUBJECT", "��������� � �������!");
    define("_GREETING", "�� �������� �������!");
    define("_CLOSING", "��� ��������� ������� ���:");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => '����������', 1 => '������', 2 => '�������', 3 => '�������', 4 => '��������', 5 => '������');
?>
