<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: hungarian.inc,v 1.2 2005/07/19 18:30:45 zoidian Exp $
*/
?>
<?php
	/* This is the Hungarian language file where you define all the strings
 	 * used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Feladatok");
	define("_THIS_WEEK", "Heti feladatok");

    /* These are for the Status drop down. */
	define("_TASKS_DAILY", "Napi feladatok");
	define("_TASKS_OPEN_ENDED", "Nyitott-Befejezett feladatok");
	define("_TASKS_WEEKLY", "Heti feladatok");
	define("_TASKS_MONTHLY", "Havi feladatok");
	define("_TASKS_OPEN", "Nyitott feladatok");
	define("_TASKS_COMPLETED", "Befejezett feladatok");
	define("_TASKS_OVERDUE", "Lejárt feladatok");
	define("_TASKS_ALL", "Összes feladat");
	define("_TASKS_THIS_WEEK", "Eheti feladatok");
	define("_SEARCH_EVERYONES", "Mások feladatai");
	define("_SEARCH_MINE", "Saját feladataim");
	define("_SEARCH_USERS", " feladatai");
	define("_TASKS_DATE", "On ");

/* These are for the Main Menu. */
	define("_NEW_TASK", "Új feladat");
	define("_CALENDAR", "Naptár");
	define("_ADMIN_MENU", "Admin Menü");
	define("_USER_MENU", "Felhasználói Menü");
	define("_LOG_OUT", "Kijelentkezés");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "Adminisztrátor jogosultság szükséges!");
	define("_EDIT_GROUPS", "Csoportok szerkesztése");
	define("_LIST_USERS", "Felhasználók listázása");
	define("_LIST_GROUPS", "Csoportok listázása");
	define("_CHANGE_PASS", "Jelszó módosítás");
	define("_EDIT_INFO", "Felhasználó adatainak módosítása");
	define("_LIST_CATEGORIES", "Kategóriák listázása");
	define("_REMOVE_CATEGORY", "Kategória eltávolítása");
	define("_RENAME_CATEGORY", "Kategória átnevezése");
	define("_CATEGORY_NAME", "Kategória neve");
	define("_ADMIN_COMMAND", "Adminisztrátor parancs");
	define("_COMMAND_ERROR", "meghatározatlan vagy nincs elég jogosultság!");
   define("_MODULES", "Modulok");


/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "Feladat szerkesztése");
	define("_DELETE_TASK", "Feladat törlése");
	define("_VIEW_TASK", "Feladat megtekintése");
	define("_ACCESS_DENIED", "Hozzáférés megtagadva");

/* Success/Failure strings. */
	define("_TASK_ADDED", "A feladat hozzádása sikerült!");
	define("_TASK_UPDATED", "A feladat frissítése sikerült!");
	define("_TASK_DELETED", "A feladat törlése sikerült!");
	define("_TASK_OPENED", "A feladat megnyitása sikerült!");
	define("_TASK_CLOSED", "A feladat sikeresen lezárva!");
	define("_USER_ADDED", "A felhasználó hozzáadása sikerült!");
	define("_USER_REMOVED", "A felhasználó eltávolítása sikerült!");
	define("_ENTER_USER", "Add meg a felhasználóneved!");
	define("_ENTER_COMMENT", "Irj be megjegyzést!");
	define("_ENTER_TITLE", "Irj be címet!");
	define("_PASS_CHANGED", "A jelszóváltoztatás sikerült!");
	define("_ENTER_GROUP", "Add meg a csoport nevét!");
	define("_GROUP_ADDED", "A csoport hozzáadása sikerült!");
	define("_GROUP_REMOVED", "A csoport eltávolítása sikerült!");
	define("_GROUP_UPDATED", "A csoport frissítése sikerült!");
	define("_REMOVE_YOURSELF", "Nem távolíthatod el magad!");
	define("_REMOVED_FROM_GROUP", "A felhasználó eltávolítása a csoportból sikerült!");
	define("_ADDED_TO_GROUP", "A felhasználó hozzáadása a csoporthoz sikerült!");
	define("_COMMENT_ADDED", "A megjegyzés hozzáadása sikerült!");
	define("_COMMENT_UPDATED", "A megjegyzés hozzáadása sikerült!");
	define("_VERIFY_PASS", "Ellenőrizd a jelszót!");
	define("_PASS_ERROR", "A jelszavak nem egyeznek!");
	define("_INFO_UPDATED", "Az adatok frissítése sikerült!");
	define("_QUERY_ERROR", "A lekérdezés sikertelen volt!");
	define("_COMMENT_REMOVED", "A megjegyzés eltávolítása sikerült!");
	define("_CATEGORY_CREATED", "A kategória hozzáadása sikerült!");
	define("_CATEGORY_UPDATED", "A kategória frissítése sikerült!");
	define("_CATEGORY_DELETED", "A kategória törlése sikerült!");
	define("_ADDED_TO_SUPERCAT", "Superkategória hozzáadása sikerült!");
	define("_SUPERCAT_REMOVED", "Superkategória eltávolítása sikerült!");
	define("_ENTER_CATEGORY", "Adj meg egy kategória nevet!");
	define("_NO_ID", "Nem adtál meg azonosítót!");
	define("_ALREADY_USER", "Már van ilyen felhasználó!");
	define("_ALREADY_GROUP", "Már van ilyen csoport!");
   define("_MODULE_ENABLE", "A modul engedélyezve!");
   define("_MODULE_DISABLE", "A modul inaktíválva!");
   define("_MODULE_INSTALL", "A modul sikeresen telepítve!");
   define("_MODULE_UNINSTALL", "A modul sikeresen eltávolítva!");


/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "Január");
	define("_FEBUARY", "Február");
	define("_MARCH", "Március");
	define("_APRIL", "Április");
	define("_MAY", "Május");
	define("_JUNE", "Június");
	define("_JULY", "Július");
	define("_AUGUST", "Augusztus");
	define("_SEPTEMBER", "Szeptember");
	define("_OCTOBER", "Október");
	define("_NOVEMBER", "November");
	define("_DECEMBER", "December");
	define("_SUNDAY", "Vasárnap");
	define("_MONDAY", "Hétfő");
	define("_TUESDAY", "Kedd");
	define("_WEDNESDAY", "Szerda");
	define("_THURSDAY", "Csütörtök");
	define("_FRIDAY", "Péntek");
	define("_SATURDAY", "Szombat");
	define("_SUN", "V");
	define("_MON", "H");
	define("_TUE", "K");
	define("_WED", "Sz");
	define("_THU", "Cs");
	define("_FRI", "P");
	define("_SAT", "Szo");

	/* Other various strings. */
	define("_CATEGORY", "Kategória");
	define("_NO_CATEGORY", "Nincs kategória");
   define("_ALL_CATEGORIES", "Összes kategória");
	define("_PRIORITY", "Priorítás");
	define("_CREATION", "Létrehozás");
	define("_GROUP", "Csoport");
	define("_ACTIONS", "Művelet");
	define("_USERS_IN_GROUP", "A csoport tagjai (Kattints a névre az eltávolításhoz)");
	define("_USERS_NOT_IN_GROUP", "Nem csoport tagok (Kattints a névre a hozzáadáshoz)");
	define("_USER", "Felhasználó");
	define("_EMPTY", "Üres!");
	define("_REMOVE", "Eltávolítás");
	define("_COMPLETE", "Befejezve");
	define("_OPEN", "Nyitott");
	define("_OVERDUE", "Lejárt");
	define("_TODAY", "Mai");
	define("_DAILY", "Napi");
	define("_OPEN_ENDED", "Nyitott-Befejezett");
	define("_WEEKLY", "Heti");
	define("_MONTHLY", "Havi");
	define("_NO_RECURRENCE", "Nincs ismétlődés");
	define("_RECURRE_EVERY", "Ismétlődik minden");
	define("_RECURRE_ON", "Ismétlődjön");
	define("_EVERY_MONTH", "Minden hónap");
	define("_STATUS", "Státusz");
	define("_TITLE", "Megnevezés");
	define("_DEADLINE", "Határidő");
	define("_TASK", "Feladat");
	define("_ASSIGNED_BY", "Hozzárendelő");
	define("_ASSIGN_TO", "Hozzárendelve:");
	define("_COMPLETED_BY", "Befejezve");
	define("_COMPLETE_TASK_BY", "Pont ekkor");
	define("_COMMENT", "Megjegyzés");
	define("_COMMENT_BY", "Megjegyzés");
	define("_COMMENT_DATE", "Megjegyzés dátuma");
	define("_UPDATE_TASK", "Feladat frissítése");
	define("_CURRENT_PASS", "Aktuális jelszó");
	define("_ADD", "Hozzáadás");
	define("_VIEW_TASKS", "Feladatok megtekintése");
	define("_EDIT", "Szerkesztés");
	define("_DELETE", "Törlés");
	define("_ALLOW_OTHERS", "Mások is delegálhatnak nekem feladatokat?");
	define("_PUBLIC_LIST", "Mások is lássák a feladataimat?");
	define("_UPDATE_INFO", "Információk frissítése");
	define("_PREV", "Előző");
	define("_NEXT", "Következő");
	define("_YOUR_CURRENT_PASS", "Add meg a jelenlegi jelszavad!");
	define("_YOUR_NEW_PASS", "Adj meg egy új jelszót!");
	define("_YOUR_VERIFY_PASS", "Az ellenőrző jelszót is meg kell adnod!");
	define("_NEW_PASS_ERROR", "Az új jelszavak nem egyeznek, próbáld újra!");
	define("_CURRENT_PASS_ERROR", "A jelszavad nem egyezik!");
   define("_IS_UNDEFINED", "nem meghatározott!");
   define("_AS_OF", " : ");
   define("_PRINT", "Nyomtatás");
   define("_DESCRIPTION", "Leírás");
   define("_DISABLE", "Nem engedélyezett");
   define("_ENABLE", "Engedélyezett");
   define("_INSTALL", "Telepít");
   define("_UNINSTALL", "Eltávolít");
   define("_MODULE", "Modul");
   define("_CONFIGURE", "Konfigurálás");

	/* These are used when adding a new user or group. */
	define("_USER_NAME", "Felhasználónév");
	define("_NEW_PASS", "Új jelszó");
	define("_VERIFY", "Ellenőrzés");
	define("_FIRST_NAME", "Vezetéknév");
	define("_LAST_NAME", "Keresztnév");
	define("_THEME", "Kinézet");
	define("_LANGUAGE", "Nyelv");
	define("_DATE_FORMAT", "Dátum formátuma");
	define("_EMAIL", "Email");
	define("_AIM", "AIM név");
	define("_IS_ADMIN", "Adminisztrátor?");
	define("_GROUP_NAME", "Csoport név");
	define("_GROUP_MEMBERS", "Tagok");
	define("_UPDATE", "Frissítés");
	define("_SELECT_GROUP", "Válassz egy csoportot");
	define("_FINISH_CREATE_USER", "Felhasználó info/jelszó beállítása");
	define("_AUTO_DAILY", "Az új feladatok napiak alapból?");
	
	/* These are for login stuff. */
	define("_LOGIN", "Bejelentkezés");
	define("_LOGON_SUCCESSFUL", "Sikeres bejelentkezés");
	define("_PASSWORD", "Jelszó");
	define("_LOGGED_OUT", "Kijelentkeztél");
	define("_KEEP_LOGIN", "Maradjon bejelentkezve erről a gépről");
	define("_WRONG_PASSWORD", "Hibás felhasználónév vagy jelszó");

	/* Supercats */
	define("_SUPERCAT", "Superkategória");
	define("_LIST_SUPERCATS", "Superkategória lista");
	define("_CATS_IN_SUPERCAT", "Superkategóriák");
	define("_REMOVED_FROM_SUPERCAT", "Superkategória sikeresen eltávolítva!");
	define("_CATS_NOT_IN_SUPERCAT", "Nem Superkategóriák");
	define("_GROUP_SUPERCAT", "Superkategória sikeresen létrehozva!");
	define("_ALREADY_SUPERCAT", "Már van ilyen nevű Superkategória!");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Nem meghatározott', 1 => 'Alacsony', 2 => 'Közepes', 3 => 'Magas', 4 => 'Delayed', 5 => 'On Hold');
?>
