<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: dutch.inc,v 1.3 2005/07/19 18:30:45 zoidian Exp $
*/
?>
<?php
	/* This is the Dutch language file where you define all the strings
	 * used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Taken");
	define("_THIS_WEEK", "Deze week");

    /* These are for the Status drop down. */
	define("_TASKS_DAILY", "Dagelijks");
	define("_TASKS_OPEN_ENDED", "Ooit...");
	define("_TASKS_WEEKLY", "Elke week");
	define("_TASKS_MONTHLY", "Elke maand");
	define("_TASKS_YEARLY", "Jaarlijks");
	define("_TASKS_OPEN", "Nog te doen");
	define("_TASKS_COMPLETED", "Voltooid");
	define("_TASKS_OVERDUE", "Te laat");
	define("_TASKS_ALL", "Alle taken");
	define("_TASKS_THIS_WEEK", "Deze week");
	define("_SEARCH_EVERYONES", "Iedereen's taken");
	define("_SEARCH_MINE", "Mijn taken");
	define("_SEARCH_USERS", "'s taken");
	define("_TASKS_DATE", "Op ");
    
    /* These are for the Main Menu. */
	define("_NEW_TASK", "Nieuwe taak");
	define("_CALENDAR", "Kalender");
	define("_ADMIN_MENU", "Instellingen");
	define("_USER_MENU", "Instellingen");
	define("_LOG_OUT", "Uitloggen");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "Alleen de beheerder mag dat!");
	define("_EDIT_GROUPS", "Groepen aanpassen");
	define("_LIST_USERS", "Accounts");
	define("_LIST_GROUPS", "Groep toevoegen");
	define("_CHANGE_PASS", "Wachtwoord veranderen");
	define("_EDIT_INFO", "Account instellingen");
	define("_LIST_CATEGORIES", "Categori&euml;n");
	define("_REMOVE_CATEGORY", "Verwijderen");
	define("_RENAME_CATEGORY", "Hernoemen");
	define("_CATEGORY_NAME", "Categorienaam");
	define("_ADMIN_COMMAND", "Opdracht");
    define("_COMMAND", "Opdracht");
	define("_COMMAND_ERROR", "is onbekend of u bent geen beheerder!");
    define("_MODULES", "Modules");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "Aanpassen");
	define("_DELETE_TASK", "Verwijderen");
	define("_VIEW_TASK", "Bekijken");
	define("_ACCESS_DENIED", "Dat mag niet");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "Nieuwe taak toegevoegd");
	define("_TASK_UPDATED", "Taak gewijzigd");
	define("_TASK_DELETED", "Taak verwijderd");
	define("_TASK_OPENED", "Task geopened");
	define("_TASK_CLOSED", "Task voltooid");
	define("_USER_ADDED", "Account toegevoegd");
	define("_USER_REMOVED", "Account verwijderd");
	define("_ENTER_USER", "Geef een gebruikersnaam op");
	define("_ENTER_COMMENT", "Geef een omschrijving");
	define("_ENTER_TITLE", "Voer een titel in");
	define("_PASS_CHANGED", "Wachtwoord gewijzigd");
	define("_ENTER_GROUP", "Voer een groepsnaam in");
	define("_GROUP_ADDED", "Groep toegevoegd");
	define("_GROUP_REMOVED", "Groep verwijderd");
	define("_GROUP_UPDATED", "Groep gewijzigd");
	define("_REMOVE_YOURSELF", "U bent ingelogd en kunt u zelf niet verwijderen");
	define("_REMOVED_FROM_GROUP", "Account uit groep verwijderd");
	define("_ADDED_TO_GROUP", "Account toegevoegd aan groep");
	define("_COMMENT_ADDED", "Omschrijving toegevoegd");
	define("_COMMENT_UPDATED", "Omschrijving gewijzigd");
	define("_VERIFY_PASS", "Voer het wachtwoord nogmaals in");
	define("_PASS_ERROR", "De wachtwoorden komen niet overeen");
	define("_INFO_UPDATED", "Gegevens gewijzigd");
	define("_QUERY_ERROR", "De zoekopdracht leverde geen resultaten op");
	define("_COMMENT_REMOVED", "Omschrijving verwijderd");
	define("_CATEGORY_CREATED", "Categorie toegevoegd");
	define("_CATEGORY_UPDATED", "Categorie gewijzigd");
	define("_CATEGORY_DELETED", "Categorie verwijderd");
	define("_ADDED_TO_SUPERCAT", "Categorie toegevoegd aan hoofdcategorie");
	define("_SUPERCAT_REMOVED", "Hoofdcategorie verwijderd");
	define("_ENTER_CATEGORY", "Voer een categorienaam in");
	define("_NO_ID", "Geen ID ingevoerd");
	define("_ALREADY_USER", "Gebruikersnaam bestaat al");
	define("_ALREADY_GROUP", "Groepnaam bestaat al");
    define("_MODULE_ENABLE", "Module ingeschakeld");
    define("_MODULE_DISABLE", "Module uitgeschakeld");
    define("_MODULE_INSTALL", "Module geinstalleerd");
    define("_MODULE_UNINSTALL", "Module gedeinstalleerd");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "Januari");
	define("_FEBUARY", "Februari");
	define("_MARCH", "Maart");
	define("_APRIL", "April");
	define("_MAY", "Mei");
	define("_JUNE", "Juni");
	define("_JULY", "Juli");
	define("_AUGUST", "Augustus");
	define("_SEPTEMBER", "September");
	define("_OCTOBER", "Oktober");
	define("_NOVEMBER", "November");
	define("_DECEMBER", "December");
	define("_SUNDAY", "Zondag");
	define("_MONDAY", "Maandag");
	define("_TUESDAY", "Dinsdag");
	define("_WEDNESDAY", "Woensdag");
	define("_THURSDAY", "Donderdag");
	define("_FRIDAY", "Vrijdag");
	define("_SATURDAY", "Zaterdag");
	define("_SUN", "Zo");
	define("_MON", "Ma");
	define("_TUE", "Di");
	define("_WED", "Wo");
	define("_THU", "Do");
	define("_FRI", "Vr");
	define("_SAT", "Za");

	/* Other various strings. */
	define("_CATEGORY", "Categorie");
	define("_NO_CATEGORY", "Geen categorie");
    define("_ALL_CATEGORIES", "Alle categori&euml;n");
	define("_PRIORITY", "Prioriteit");
	define("_CREATION", "Gemaakt op");
	define("_GROUP", "Groep");
	define("_ACTIONS", "Opties");
	define("_WHOS_TASK", "Taak van");
	define("_USERS_IN_GROUP", "Account in groep (klik om te verwijderen)");
	define("_USERS_NOT_IN_GROUP", "Account niet in groep (klik om toe te voegen)");
	define("_USER", "Account");
	define("_EMPTY", "Leeg");
	define("_REMOVE", "Verwijderen");
	define("_COMPLETE", "Voltooid");
	define("_OPEN", "Nog te doen");
	define("_OVERDUE", "te laat");
	define("_TODAY", "Vandaag");
	define("_DAILY", "Dagelijks");
	define("_OPEN_ENDED", "Ooit...");
	define("_WEEKLY", "Elke week");
	define("_MONTHLY", "Elke maand");
	define("_YEARLY", "Jaarlijks");
	define("_NO_RECURRENCE", "Eenmalig");
	define("_RECURRE_EVERY", "Elke");
	define("_RECURRE_ON", "Iedere");
	define("_EVERY_MONTH", "v/d maand");
	define("_STATUS", "Status");
	define("_TITLE", "Titel");
	define("_DEADLINE", "Deadline");
	define("_TASK", "Taak");
	define("_ASSIGNED_BY", "Toegewezen door");
	define("_ASSIGN_TO", "Toegewezen aan");
	define("_COMPLETED_BY", "Voltooid door");
	define("_COMPLETE_TASK_BY", "klaar op");
	define("_COMMENT", "Omschrijving");
	define("_COMMENT_BY", "Omschrijving door");
	define("_COMMENT_DATE", "Datum Omschrijving");
    define("_UPDATE_TASK", "Wijzig taak");
	define("_CURRENT_PASS", "Huidig wachtwoord");
	define("_ADD", "Toevoegen");
	define("_FILTER", "Zoeken");
	define("_VIEW_TASKS", "Taken bekijken");
	define("_EDIT", "Aanpassen");
	define("_DELETE", "Verwijderen");
	define("_ALLOW_OTHERS", "Anderen mogen mij taken geven?");
	define("_PUBLIC_LIST", "Anderen mogen mijn taken bekijken?");
	define("_UPDATE_INFO", "Wijzigingen toepassen");
	define("_PREV", "Vorige");
	define("_NEXT", "Volgende");
	define("_YOUR_CURRENT_PASS", "Huidige wachtwoord moet ingevoerd worden!");
	define("_YOUR_NEW_PASS", "Nieuw wachtwoord moet ingevoerd worden!");
	define("_YOUR_VERIFY_PASS", "Wachtwoord moet bevestigd worden!");
	define("_NEW_PASS_ERROR", "Nieuw wachtwoord komt niet overeen met bevestiging!");
	define("_CURRENT_PASS_ERROR", "Huidige wachtwoord incorrect!");
    define("_IS_UNDEFINED", "is onbekend");
    define("_AS_OF", " op ");
    define("_PRINT", "Afdrukken");
    define("_SET_VIEW", "Maak dit de standaard weergave");
    define("_DESCRIPTION", "Omschrijving");
    define("_DISABLE", "Uitschakelen");
    define("_ENABLE", "Inschakelen");
    define("_INSTALL", "Installeren");
    define("_UNINSTALL", "Verwijderen");
    define("_MODULE", "Module");
    define("_CONFIGURE", "Configureren");

	/* These are used when adding a new user or group. */
	define("_USER_NAME", "Gebruikersnaam");
	define("_NEW_PASS", "Nieuw wachtwoord");
	define("_VERIFY", "Bevestigen");
	define("_FIRST_NAME", "Voornaam");
	define("_LAST_NAME", "Achternaam");
	define("_THEME", "Vormgeving");
	define("_LANGUAGE", "Taal");
	define("_DATE_FORMAT", "Datumweergave");
	define("_EMAIL", "E-mailadres");
	define("_AIM", "AIM gebruikersnaam");
	define("_IS_ADMIN", "Mag beheerder zijn?");
	define("_GROUP_NAME", "Groepnaam");
	define("_GROUP_MEMBERS", "Groepsleden");
	define("_UPDATE", "Toepassen");
	define("_SELECT_GROUP", "Kies een groepe");
	define("_FINISH_CREATE_USER", "Accountgegevens invoeren");
	define("_AUTO_DAILY", "Maak nieuwe taken automatisch dagelijkse taken?");

	/* These are for login stuff. */
	define("_LOGIN", "Login:");
	define("_LOGON_SUCCESSFUL", "Welkom");
	define("_PASSWORD", "Wachtwoord:");
	define("_LOGGED_OUT", "U bent uitgelogd");
	define("_KEEP_LOGIN", "Houd mij ingelogd op deze computer");
	define("_WRONG_PASSWORD", "Gebruikersnaam of wachtwoord onbekend");
	
	/* Supercats */
	define("_SUPERCAT", "Hoofdcategorie");
	define("_LIST_SUPERCATS", "Hoofdcategori&euml;n");
	define("_CATS_IN_SUPERCAT", "Categorie in hoofdcategorie (klik om te verwijderen");
	define("_REMOVED_FROM_SUPERCAT", "Categorie verwijderd uit hoofdcategorie!");
	define("_CATS_NOT_IN_SUPERCAT", "Categorie niet in hoofdcategorie (klik om toe te voegen)");
	define("_GROUP_SUPERCAT", "Hoofdcategorie toegevoegd");
	define("_ALREADY_SUPERCAT", "Hoofdcategorie bestaat al");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Geen', 1 => 'Laag', 2 => 'Normaal', 3 => 'Hoog', 4 => 'Delayed', 5 => 'On Hold');
?>
