<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: german.inc,v 1.11 2006/01/08 23:15:54 zoidian Exp $
*/
?>
<?php
	/* This is the German language file where you define all the strings
     * used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Aufgaben zeigen");
	define("_THIS_WEEK", "Aufgaben in dieser Woche");

    /* These are for the Status drop down. */
	define("_TASKS_DAILY", "T&auml;gliche Aufgaben");
	define("_TASKS_OPEN_ENDED", "Unbefristete Aufgaben");
	define("_TASKS_WEEKLY", "W�chentliche Aufgaben");
	define("_TASKS_MONTHLY", "Monatliche Aufgaben");
	define("_TASKS_YEARLY", "J�hrliche Aufgaben");
	define("_TASKS_OPEN", "Offene Aufgaben");
	define("_TASKS_COMPLETED", "Abgeschlossene Aufgaben");
	define("_TASKS_OVERDUE", "�berf�llige Aufgaben");
	define("_TASKS_ALL", "Alle Aufgaben");
	define("_TASKS_THIS_WEEK", "Aufgaben dieser Woche");
	define("_SEARCH_EVERYONES", "Alle Aufgaben");
	define("_SEARCH_MINE", "Meine Aufgaben");
	define("_SEARCH_USERS", "s Aufgaben");
	define("_TASKS_DATE", "Am ");

    /* These are for the Main Menu. */
	define("_NEW_TASK", "Neue Aufgabe");
	define("_CALENDAR", "Kalender");
	define("_ADMIN_MENU", "Adminmen�");
	define("_USER_MENU", "Benutzermen�");
	define("_LOG_OUT", "Abmelden");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "Nur der Administrator darf das!");
	define("_EDIT_GROUPS", "Gruppen bearbeiten");
	define("_LIST_USERS", "Benutzer auflisten");
	define("_LIST_GROUPS", "Gruppen auflisten");
	define("_CHANGE_PASS", "Passwort �ndern");
	define("_EDIT_INFO", "Benutzerinformation bearbeiten");
	define("_LIST_CATEGORIES", "Kategorien auflisten");
	define("_REMOVE_CATEGORY", "Kategorie entfernen");
	define("_RENAME_CATEGORY", "Kategorie umbenennen");
	define("_CATEGORY_NAME", "Kategorien-Name");
	define("_ADMIN_COMMAND", "Admin-Befehl");
    define("_COMMAND", "Befehl");
	define("_COMMAND_ERROR", "Nicht definiert oder unzureichende Berechtigung!");
    define("_MODULES", "Module");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "Aufgabe bearbeiten");
	define("_DELETE_TASK", "Aufgabe l�schen");
	define("_VIEW_TASK", "Aufgabe zeigen");
	define("_ACCESS_DENIED", "Zugriff verweigert");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "Aufgabe hinzugef�gt!");
	define("_TASK_UPDATED", "Aufgabe aktualisiert!");
	define("_TASK_DELETED", "Aufgabe gel�scht!");
	define("_TASK_OPENED", "Aufgabe ge�ffnet!");
	define("_TASK_CLOSED", "Aufgabe geschlossen!");
	define("_USER_ADDED", "Benutzer hinzugef�gt!");
	define("_USER_REMOVED", "Benutzer gel�scht!");
	define("_ENTER_USER", "Sie m�ssen einen Benutzernamen eingeben!");
	define("_ENTER_COMMENT", "Sie m�ssen einen Kommentar eingeben!");
	define("_ENTER_TITLE", "Sie m�ssen einen Titel eingeben!");
	define("_PASS_CHANGED", "Passwort ge�ndert!");
	define("_ENTER_GROUP", "Sie m�ssen einen Gruppen-Namen eingeben!");
	define("_GROUP_ADDED", "Gruppe hinzugef�gt!");
	define("_GROUP_REMOVED", "Gruppe gel�scht!");
	define("_GROUP_UPDATED", "Gruppe aktualisiert!");
	define("_REMOVE_YOURSELF", "Sie k�nnen sich nicht selbst entfernen!");
	define("_REMOVED_FROM_GROUP", "Benutzer aus Gruppe entfernt!");
	define("_ADDED_TO_GROUP", "Benutzer zur Gruppe hinzugef�gt!");
	define("_COMMENT_ADDED", "Kommentar erfolgreich hinzugef�gt!");
	define("_COMMENT_UPDATED", "Kommentar erfolgreich aktualisiert!");
	define("_VERIFY_PASS", "Sie m�ssen das Passwort best�tigen!");
	define("_PASS_ERROR", "Die Passw�rter sind nicht identisch!");
	define("_INFO_UPDATED", "Information aktualisiert!");
	define("_QUERY_ERROR", "Die Abfrage schlug fehl!");
	define("_COMMENT_REMOVED", "Kommentar wurde erfolgreich entfernt!");
	define("_CATEGORY_CREATED", "Kategorie hinzugef�gt!");
	define("_CATEGORY_UPDATED", "Kategorie aktualisiert!");
	define("_CATEGORY_DELETED", "Kategorie gel�scht!");
	define("_ADDED_TO_SUPERCAT", "Kategorie erfolgreich der �berkategorie zugewiesen!");
	define("_SUPERCAT_REMOVED", "�berkategorie gel�scht!");
	define("_ENTER_CATEGORY", "Sie m�ssen einen Kategorie-Namen eingeben!");
	define("_NO_ID", "Keine ID angegeben!");
	define("_ALREADY_USER", "Es existiert bereits ein Benutzer mit diesem Namen!");
	define("_ALREADY_GROUP", "Es existiert bereits eine Gruppe mit diesem Namen!");
    define("_MODULE_ENABLE", "Modul aktiviert!");
    define("_MODULE_DISABLE", "Modul deaktiviert!");
    define("_MODULE_INSTALL", "Modul installiert!");
    define("_MODULE_UNINSTALL", "Modul deinstalliert!");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "Januar");
	define("_FEBUARY", "Februar");
	define("_MARCH", "M&auml;rz");
	define("_APRIL", "April");
	define("_MAY", "Mai");
	define("_JUNE", "Juni");
	define("_JULY", "Juli");
	define("_AUGUST", "August");
	define("_SEPTEMBER", "September");
	define("_OCTOBER", "Oktober");
	define("_NOVEMBER", "November");
	define("_DECEMBER", "Dezember");
	define("_SUNDAY", "Sontag");
	define("_MONDAY", "Montag");
	define("_TUESDAY", "Dienstag");
	define("_WEDNESDAY", "Mittwoch");
	define("_THURSDAY", "Donnerstag");
	define("_FRIDAY", "Freitag");
	define("_SATURDAY", "Samstag");
	define("_SUN", "So");
	define("_MON", "Mo");
	define("_TUE", "Di");
	define("_WED", "Mi");
	define("_THU", "Do");
	define("_FRI", "Fr");
	define("_SAT", "Sa");

	/* Other various strings. */
	define("_CATEGORY", "Kategorie");
	define("_NO_CATEGORY", "Keine Kategorie");
    define("_ALL_CATEGORIES", "Alle Kategorien");
	define("_PRIORITY", "Priorit�t");
	define("_CREATION", "Erstellt am");
	define("_GROUP", "Gruppe");
	define("_ACTIONS", "Aktionen");
	define("_WHOS_TASK", "Wessen Aufgabe");
	define("_USERS_IN_GROUP", "Benutzer in Gruppe (Klick entfernt)");
	define("_USERS_NOT_IN_GROUP", "Benutzer nicht in Gruppe (Klick f&uuml;gt hinzu)");
	define("_USER", "Benutzer");
	define("_EMPTY", "Leer!");
	define("_REMOVE", "Entfernen");
	define("_COMPLETE", "Fertiggestellt");
	define("_OPEN", "�ffnen");
	define("_OVERDUE", "�berf�llig");
	define("_TODAY", "Heute");
	define("_DAILY", "T�glich");
	define("_OPEN_ENDED", "Unbefristete Aufgabe");
	define("_WEEKLY", "W�chentlich");
	define("_MONTHLY", "Monatlich");
	define("_YEARLY", "J�hrlich");
	define("_NO_RECURRENCE", "Keine Wiederholung");
	define("_RECURRE_EVERY", "Wiederholung jede");
	define("_RECURRE_ON", "Wiederholung an");
	define("_EVERY_MONTH", "Jeden Monat");
	define("_STATUS", "Status");
	define("_TITLE", "Titel");
	define("_DEADLINE", "Termin");
	define("_TASK", "Aufgabe");
	define("_ASSIGNED_BY", "Zugewiesen von");
	define("_ASSIGN_TO", "Zuweisen an");
	define("_COMPLETED_BY", "Fertig am");
	define("_COMPLETE_TASK_BY", "Fertig bis");
	define("_COMMENT", "Kommentar");
	define("_COMMENT_BY", "Kommentar von");
	define("_COMMENT_DATE", "Kommentar Datum");
	define("_UPDATE_TASK", "Aktualisiere Aufgabe");
	define("_CURRENT_PASS", "Derzeitiges Passwort");
	define("_ADD", "Hinzuf�gen");
	define("_FILTER", "Filter");
	define("_VIEW_TASKS", "Aufgaben zeigen");
	define("_EDIT", "Bearbeiten");
	define("_DELETE", "L�schen");
	define("_ALLOW_OTHERS", "Anderen erlauben mir Aufgaben zuzuweisen?");
	define("_PUBLIC_LIST", "Anderen erlauben meine Todo-Liste einzusehen?");
	define("_UPDATE_INFO", "Aktualisiere Informationen");
	define("_PREV", "davor");
	define("_NEXT", "danach");
	define("_YOUR_CURRENT_PASS", "Sie m&uuml;ssen ihr aktuelles Kennwort eingeben!");
	define("_YOUR_NEW_PASS", "Sie m&uuml;ssen ihr neues Kennwort eingeben!");
	define("_YOUR_VERIFY_PASS", "Zur &Uuml;berpr&uuml;fung nochmal das neue Kennwort eingeben!");
	define("_NEW_PASS_ERROR", "&Uuml;berpr&uuml;fung des neuen Kennworts ist fehlgeschlagen!");
	define("_CURRENT_PASS_ERROR", "Ihr aktuelles Kennwort ist falsch!");
    define("_IS_UNDEFINED", "nicht definiert!");
    define("_AS_OF", " am ");
    define("_PRINT", "Drucken");
    define("_SET_VIEW", "Als Standardansicht festlegen");
    define("_DESCRIPTION", "Beschreibung");
    define("_DISABLE", "Deaktivieren");
    define("_ENABLE", "Aktivieren");
    define("_INSTALL", "Installieren");
    define("_UNINSTALL", "Deinstallieren");
    define("_MODULE", "Modul");
    define("_CONFIGURE", "Konfiguration");

	/* These are used when adding a new user or group. */
	define("_USER_NAME", "Benutzername");
	define("_NEW_PASS", "Neues Passwort");
	define("_VERIFY", "�berpr�fen");
	define("_FIRST_NAME", "Vorname");
	define("_LAST_NAME", "Nachname");
	define("_THEME", "Oberfl�che");
	define("_LANGUAGE", "Sprache");
	define("_DATE_FORMAT", "Datumsformat");
	define("_ENABLE_AJAX", "AJAX einschalten?");
	define("_EMAIL", "E-mail");
	define("_AIM", "AIM-Name");
	define("_IS_ADMIN", "Administrator?");
	define("_GROUP_NAME", "Gruppen-Name");
	define("_GROUP_MEMBERS", "Mitglieder");
	define("_UPDATE", "Aktualisieren");
	define("_SELECT_GROUP", "W�hle eine Gruppe");
	define("_FINISH_CREATE_USER", "Setze Benutzer Info/Passwort");
	define("_AUTO_DAILY", "Sollen neue Aufgaben automatisch t&auml;glich sein?");

	/* These are for login stuff. */
	define("_LOGIN", "Anmeldung");
	define("_LOGON_SUCCESSFUL", "Anmeldung erfolgreich");
	define("_PASSWORD", "Kennwort");
	define("_LOGGED_OUT", "Sie wurden abgemeldet");
	define("_KEEP_LOGIN", "Anmeldung an dieser Machine aktiv halten");
	define("_WRONG_PASSWORD", "Falscher Benutzer oder falsches Kennwort");

	/* Category */
	define("_USER_CATEGORY_NAME", "Benutzerkategorien");
	define("_GLOBAL_CATEGORY_NAME", "Globale Kategorien");

	/* Supercats */
	define("_SUPERCAT", "&Uuml;berkategorie");
	define("_LIST_SUPERCATS", "Liste &Uuml;berkategorien");
	define("_CATS_IN_SUPERCAT", "Kategorien in &Uuml;berkategorie");
	define("_REMOVED_FROM_SUPERCAT", "Kategorie erfolgreioch von &Uuml;berkategorie entfernt!");
	define("_CATS_NOT_IN_SUPERCAT", "Kategorien nicht in &Uuml;berkategorie");
	define("_GROUP_SUPERCAT", "&Uuml;berkategorie erfolgreich erzeugt!");
	define("_ALREADY_SUPERCAT", "Es existiert bereits eine �berkategorie mit diesem Namen!");

	global $_PRIORITY;
	$_PRIORITY = array(0 => 'keine', 1 => 'niedrig', 2 => 'mittel', 3 => 'hoch', 4 => 'versp�tet', 5 => 'In der Warteschleife');
?>
