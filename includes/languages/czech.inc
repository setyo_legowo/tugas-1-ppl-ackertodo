<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: czech.inc,v 1.1 2005/09/18 15:16:18 zoidian Exp $
*/
?>
<?php
	/* This is the Czech language file where you define all the strings
     * used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Zabrazit úkoly");
	define("_THIS_WEEK", "Úkoly na tento týden");

    /* These are for the Status drop down. */
	define("_TASKS_DAILY", "Každodenní");
	define("_TASKS_OPEN_ENDED", "Bez termínu");
	define("_TASKS_WEEKLY", "Každotýdnní");
	define("_TASKS_MONTHLY", "Každoměsíční");
	define("_TASKS_YEARLY", "Každoroční");
	define("_TASKS_OPEN", "Otevřené");
	define("_TASKS_COMPLETED", "Dokončené");
	define("_TASKS_OVERDUE", "Zpožděné");
	define("_TASKS_ALL", "Všechny");
	define("_TASKS_THIS_WEEK", "Na tento týden");
	define("_SEARCH_EVERYONES", "Úkoly všech uživatelů");
	define("_SEARCH_MINE", "Moje úkoly");
	define("_SEARCH_USERS", " úkoly");
	define("_TASKS_DATE", " ");
    
    /* These are for the Main Menu. */
	define("_NEW_TASK", "Nový úkol");
	define("_CALENDAR", "Kalendář");
	define("_ADMIN_MENU", "Administrátorské menu");
	define("_USER_MENU", "Uživatelské menu");
	define("_LOG_OUT", "Odhlásit");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "Toto může provést pouze administrátor!");
	define("_EDIT_GROUPS", "Editovat skupiny");
	define("_LIST_USERS", "Seznam uživatelů");
	define("_LIST_GROUPS", "Seznam skupin");
	define("_CHANGE_PASS", "Změnit heslo");
	define("_EDIT_INFO", "Upravit info o uživateli");
	define("_LIST_CATEGORIES", "Seznam kategorií");
	define("_REMOVE_CATEGORY", "Odstranit kategorii");
	define("_RENAME_CATEGORY", "Přejmenovat kategorii");
	define("_USER_CATEGORY_NAME", "Název uživatelské kategorie");
	define("_GLOBAL_CATEGORY_NAME", "Název globální kategorie");
	define("_ADMIN_COMMAND", "Administrátorský příkaz");
    define("_COMMAND", "Příkaz");
	define("_COMMAND_ERROR", "není definován, nebo nemáte příslušné oprávnění!");
    define("_MODULES", "Moduly");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "Editovat úkol");
	define("_DELETE_TASK", "Odstranit úkol");
	define("_VIEW_TASK", "Zobrazit úkol");
	define("_ACCESS_DENIED", "Přístup odmítnut");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "Úkol byl úspěšně uložen!");
	define("_TASK_UPDATED", "Úkol byl úspěšně aktualizován!");
	define("_TASK_DELETED", "Úkol byl úspěšně odstraněn!");
	define("_TASK_OPENED", "Úkol byl uspěšně otevřen!");
	define("_TASK_CLOSED", "Úkol byl úspěšně dokončen!");
	define("_USER_ADDED", "Uživatel byl śpěšně přidán!");
	define("_USER_REMOVED", "Uživatel byl úspěšně odstraněn!");
	define("_ENTER_USER", "Musíte zadat uživatelské jméno!");
	define("_ENTER_COMMENT", "Musíte zadat komentář!");
	define("_ENTER_TITLE", "Musíte zadat předmět!");
	define("_PASS_CHANGED", "Heslo bylo úspěšně změněno!");
	define("_ENTER_GROUP", "Musíte zadat název skupiny!");
	define("_GROUP_ADDED", "Skupina byla úspěšně vytvořena!");
	define("_GROUP_REMOVED", "Skupina byla úspěšně odstraněna!");
	define("_GROUP_UPDATED", "Skupina byla úspěšně aktualizována!");
	define("_REMOVE_YOURSELF", "Nemůžete odstranit sám/sama sebe!");
	define("_REMOVED_FROM_GROUP", "Uživatel byl úspěšně odstraněn ze skupiny!");
	define("_ADDED_TO_GROUP", "Uživatel byl úspěšně přidán do skupiny!");
	define("_COMMENT_ADDED", "Komentář byl úspěšně uložen!");
	define("_COMMENT_UPDATED", "Komentář byl úspěšně aktualizován!");
	define("_VERIFY_PASS", "Musíte zadat heslo pro ověření!");
	define("_PASS_ERROR", "Hesla nejsou shodná!");
	define("_INFO_UPDATED", "Informace byly úspěšně aktualizovány!");
	define("_QUERY_ERROR", "Dotaz selhal!");
	define("_COMMENT_REMOVED", "Komentář byl úspěšně odstraněn!");
	define("_CATEGORY_CREATED", "Kategorie byla úspěšně přidána!");
	define("_CATEGORY_UPDATED", "Kategorie byla úspěšně aktualizována!");
	define("_CATEGORY_DELETED", "Kategorie byla úspěšně odstraněna!");
	define("_ADDED_TO_SUPERCAT", "Kategorie byla do super-kategorie úspěšně přidána!");
	define("_SUPERCAT_REMOVED", "Super-kategorie byla úspěšně odstraněna!");
	define("_ENTER_CATEGORY", "Musíte zadat název kategorie!");
	define("_NO_ID", "Nebylo zadáno ID kategorie!");
	define("_ALREADY_USER", "Uživatel s tímto jménem již existuje!");
	define("_ALREADY_GROUP", "Skupina s tímto názvem již existuje!");
    define("_MODULE_ENABLE", "Modul byl úspěšně aktivován!");
    define("_MODULE_DISABLE", "Modul byl úspěšně deaktivován!");
    define("_MODULE_INSTALL", "Modul byl úspěšně nainstalován!");
    define("_MODULE_UNINSTALL", "Modul byl úspěšně odinstalován!");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "Leden");
	define("_FEBUARY", "Únor");
	define("_MARCH", "Březen");
	define("_APRIL", "Duben");
	define("_MAY", "Květen");
	define("_JUNE", "Červen");
	define("_JULY", "Červenec");
	define("_AUGUST", "Srpen");
	define("_SEPTEMBER", "Září");
	define("_OCTOBER", "Říjen");
	define("_NOVEMBER", "Listopad");
	define("_DECEMBER", "Prosinec");
	define("_SUNDAY", "Neděle");
	define("_MONDAY", "Pondělí");
	define("_TUESDAY", "Úterý");
	define("_WEDNESDAY", "Středa");
	define("_THURSDAY", "Čtvrtek");
	define("_FRIDAY", "Pátek");
	define("_SATURDAY", "Sobota");
	define("_SUN", "Ne");
	define("_MON", "Po");
	define("_TUE", "Út");
	define("_WED", "St");
	define("_THU", "Čt");
	define("_FRI", "Pá");
	define("_SAT", "So");

	/* Other various strings. */
    define("_VIEW_UPDATED", "Výchozí náhled byl úspěšně aktualizován!");
	define("_CATEGORY", "Kategorie");
	define("_NO_CATEGORY", "Bez kategorie");
    define("_ALL_CATEGORIES", "Všechny kategorie");
	define("_PRIORITY", "Priorita");
	define("_CREATION", "Vytvořeno");
	define("_GROUP", "Skupina");
	define("_ACTIONS", "Akce");
	define("_WHOS_TASK", "Přiřazený uživatel");
	define("_USERS_IN_GROUP", "Členové skupiny (kliknutím odstranit)");
	define("_USERS_NOT_IN_GROUP", "Ostatní uživatelé (kliknutím přidat)");
	define("_USER", "Uživatel");
	define("_EMPTY", "-- prázdné --");
	define("_REMOVE", "Odstranit");
	define("_COMPLETE", "Dokončeno");
	define("_OPEN", "Otevřít");
	define("_OVERDUE", "zpoždění");
	define("_TODAY", "Dnes");
	define("_DAILY", "Denně");
	define("_OPEN_ENDED", "Bez termínu");
	define("_WEEKLY", "Týdně");
	define("_MONTHLY", "Měsíčně");
	define("_YEARLY", "Ročně");
	define("_NO_RECURRENCE", "Bez opakování");
    define("_RECURRENCE", "Opakování");
	define("_RECURRE_EVERY", "Opakovat v");
	define("_RECURRE_ON", "Opakovat dne");
	define("_EVERY_MONTH", "Každý měsíc");
	define("_STATUS", "Status");
	define("_TITLE", "Předmět");
	define("_DEADLINE", "Termín");
	define("_TASK", "Úkol");
	define("_ASSIGNED_BY", "Vytvořil");
	define("_ASSIGN_TO", "Přiřazeno");
	define("_COMPLETED_BY", "Dokončeno");
	define("_COMPLETE_TASK_BY", "Termín");
	define("_COMMENT", "Komentář");
	define("_COMMENT_BY", "Komentář od");
	define("_COMMENT_DATE", "Datum komentáře");
	define("_UPDATE_TASK", "Aktualizovat úkol");
	define("_CURRENT_PASS", "Aktuální heslo");
	define("_ADD", "Přidat");
	define("_FILTER", "Filtr");
	define("_VIEW_TASKS", "Zobrazit úkoly");
	define("_EDIT", "Upravit");
	define("_DELETE", "Odstranit");
	define("_ALLOW_OTHERS", "Povolit ostatním přidělovat mi úkoly?");
	define("_PUBLIC_LIST", "Povolit ostatním prohlížet moje úkoly?");
	define("_UPDATE_INFO", "Aktualizovat informace");
	define("_PREV", "Předchozí");
	define("_NEXT", "Další");
	define("_YOUR_CURRENT_PASS", "Musíte zadat vaše aktuální heslo!");
	define("_YOUR_NEW_PASS", "Musíte zadat nové heslo!");
	define("_YOUR_VERIFY_PASS", "Musíte zadat heslo pro ověření!");
	define("_NEW_PASS_ERROR", "Nové heslo se neshoduje s heslem pro ověření!");
	define("_CURRENT_PASS_ERROR", "Vaše aktuální heslo není správné!");
    define("_IS_UNDEFINED", "není definováno!");
    define("_AS_OF", " dne ");
    define("_PRINT", "Tisknout");
    define("_SET_VIEW", "Nastavit jako výchozí náhled");
    define("_DESCRIPTION", "Popis");
    define("_DISABLE", "Deaktivovat");
    define("_ENABLE", "Aktivovat");
    define("_INSTALL", "Instalovat");
    define("_UNINSTALL", "Odinistalovat");
    define("_MODULE", "Modul");
    define("_CONFIGURE", "Konfigurovat");
    define("_CANCEL", "Storno");
    define("_WHAT_WOULD_YOU_LIKE", "Co chcete provést?");
    define("_RESET_ALL", "Odstranit tuto kategorii ze všech úkolů?");
    define("_REMOVE_ALL", "Odstranit všechny úkoly v této kategorii?");

	/* These are used when adding a new user or group. */
	define("_USER_NAME", "Jméno uživatele");
	define("_NEW_PASS", "Nové heslo");
	define("_VERIFY", "Ověření hesla");
	define("_FIRST_NAME", "Jméno");
	define("_LAST_NAME", "Příjmení");
	define("_THEME", "Téma");
	define("_LANGUAGE", "Jazyk");
	define("_DATE_FORMAT", "Formát data");
	define("_EMAIL", "Email");
	define("_AIM", "Nick v AIMu");
	define("_IS_ADMIN", "Administrátor?");
	define("_GROUP_NAME", "Název skupiny");
	define("_GROUP_MEMBERS", "Členové");
	define("_UPDATE", "Aktualizovat");
	define("_SELECT_GROUP", "Vyberte skupinu");
	define("_FINISH_CREATE_USER", "Nastavit už. info/heslo");
	define("_AUTO_DAILY", "Nové úkoly jako každodenní?");

	/* These are for login stuff. */
	define("_LOGIN", "Přihlášení");
	define("_LOGON_SUCCESSFUL", "Přihlášení bylo úspěšné");
	define("_PASSWORD", "Heslo");
	define("_LOGGED_OUT", "Byl(a) jste odhlášen(a)");
	define("_KEEP_LOGIN", "Chci zůstat přihlášen(á) na tomto PC");
	define("_WRONG_PASSWORD", "Chybné uživatelské jméno nebo heslo");
	
	/* Supercats */
	define("_SUPERCAT", "Super-kategorie");
	define("_LIST_SUPERCATS", "Seznam superkategorií");
	define("_CATS_IN_SUPERCAT", "Kategorie v super-kategorii");
	define("_REMOVED_FROM_SUPERCAT", "Kategorie byla ze super-kategorie úspěšně odstraněna!");
	define("_CATS_NOT_IN_SUPERCAT", "Kategorie nepřiřazené super-kategorii");
	define("_GROUP_SUPERCAT", "Super-kategorie byla úspěšně vytvořena!");
	define("_ALREADY_SUPERCAT", "Super-kategorie s tímto názvem již existuje!");

    /* Email Notifications */
    define("_SUBJECT", "Oznámení o přidělení úkolu!");
    define("_GREETING", "Byl Vám přidělen úkol!");
    define("_CLOSING", "Toto oznámení Vám zaslal:");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Nedefinovaná', 1 => 'Nízká', 2 => 'Střední', 3 => 'Vysoká', 4 => 'Opožděno', 5 => 'Pozastaveno');
?>
