<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: bulgarian.inc,v 1.3 2005/07/19 18:30:29 zoidian Exp $
*/
?>
<?php
	/* This is the Bulgarian language file where you define all the strings
	* used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "������ ��������");
	define("_SHOW_DAILY", "������ ����������� ������");
	define("_SHOW_OPEN", "������ ������������� ������");
	define("_SHOW_COMPLETED", "������ ����������� ������");
	define("_SHOW_ALL", "������ ������ ������");
	define("_THIS_WEEK", "�������� �� ���� �������");
	define("_TASKS_DAILY", "��������� ������");
	define("_TASKS_OPEN", "����������� ������");
	define("_TASKS_COMPLETED", "��������� ������");
	define("_TASKS_OVERDUE", "���������� ������");
	define("_TASKS_ALL", "������ ������");
	define("_TASKS_THIS_WEEK", "�������� �� ���� �������(1)?");
	define("_TASKS_DATE", "�� ");
	define("_NEW_TASK", "���� ������");
	define("_CALENDAR", "��������");
	define("_ADMIN_MENU", "�������� ����");
	define("_USER_MENU", "������������� ����");
	define("_LOG_OUT", "�����");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "���� ������� ���� �� ����� ����!");
	define("_ADD_USER_GROUP", "������ ����������/�����");
	define("_REMOVE_USER_GROUP", "�������� ����������/�����");
	define("_EDIT_GROUPS", "���������� �����");
	define("_LIST_USERS", "������ � �����������");
	define("_LIST_GROUPS", "������ � �����");
	define("_CHANGE_PASS", "����� �� ��������");
	define("_EDIT_INFO", "���������� ��������������� ����������");
	define("_LIST_CATEGORIES", "������ � ���������");
	define("_REMOVE_CATEGORY", "�������� ���������");
	define("_RENAME_CATEGORY", "����������� ���������");
	define("_CATEGORY_NAME", "��� �� ���������");
	define("_VIEW_GROUPS", "��� �������");
	define("_FILTER", "������");
	define("_ADMIN_COMMAND", "���������������� �������");
	define("_COMMAND_ERROR", "�� � ���������� ��� ������ �����!");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "���������� ������");
	define("_DELETE_TASK", "������ ������");
	define("_VIEW_TASK", "��� ������");
	define("_ADD_COMMENT", "������ ��������");

	/* These are for the Search Menu. */
	define("_SEARCH_FOR", "����� �� ");
	define("_SEARCH_IN", " � ");
	define("_SEARCH_ALL", "������ ������");
	define("_SEARCH_OPEN", "����������� ������");
	define("_SEARCH_CLOSED", "���������� ������");
	define("_SEARCH_DAILY", "��������� ������");
	define("_SEARCH", "�������");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "�������� �������� �������!");
	define("_TASK_UPDATED", "�������� �������� �������!");
	define("_TASK_DELETED", "�������� ������� �������!");
	define("_TASK_OPENED", "�������� '�����������' �������!");
	define("_TASK_CLOSED", "�������� ���������� �������!");
	define("_USER_ADDED", "����������� ������� �������!");
	define("_USER_REMOVED", "����������� ������ �������!");
	define("_USER_EXISTS", "����������� ���� � � ���� �����!");
	define("_USER_TO_GROUP", "����������� ������� � �������!");
	define("_ENTER_USER", "������ �� �������� ������������� ���!");
	define("_ENTER_PASS", "������ �� �������� ������!");
	define("_PASS_CHANGED", "�������� ������� �������!");
	define("_ENTER_GROUP", "������ �� �������� ��� �� �����!");
	define("_GROUP_ADDED", "������� ��������!");
	define("_GROUP_REMOVED", "������� ����������!");
	define("_GROUP_UPDATED", "������� ��������!");
	define("_REMOVE_YOURSELF", "�� ������ �� �� ������������!");
	define("_REMOVED_FROM_GROUP", "����������� ��������� �� �������!");
	define("_ADDED_TO_GROUP", "����������� ������� � �������!");
	define("_COMMENT_ADDED", "��������� �������!");
	define("_VERIFY_PASS", "������ �� ���������� ��������������� ������!");
	define("_PASS_ERROR", "����� ������ �� ����������!");
	define("_SELECT_USER", "������ �� �������� ���������� �� �� �� �������� � �������!");
	define("_THEME_CHANGED", "������ ������� �������!");
	define("_LANGUAGE_CHANGED", "����� ������ �������!");
	define("_INFO_UPDATED", "������������ �������� �������!");
	define("_QUERY_ERROR", "�������� ���� ���������!");
	define("_COMMENT_REMOVED", "��������� ���������!");
	define("_CATEGORY_CREATED", "�������� ���������!");
	define("_CATEGORY_UPDATED", "��������� �������� �������!");
	define("_CATEGORY_DELETED", "������� ���������!");
	define("_ENTER_CATEGORY", "������ �� �������� ��� �� ���������!");
	define("_NO_ID", "���� ID!");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "������");
	define("_FEBUARY", "��������");
	define("_MARCH", "����");
	define("_APRIL", "�����");
	define("_MAY", "���");
	define("_JUNE", "���");
	define("_JULY", "���");
	define("_AUGUST", "������");
	define("_SEPTEMBER", "���������");
	define("_OCTOBER", "��������");
	define("_NOVEMBER", "�������");
	define("_DECEMBER", "��������");
	define("_SUNDAY", "������");
	define("_MONDAY", "����������");
	define("_TUESDAY", "�������");
	define("_WEDNESDAY", "�����");
	define("_THURSDAY", "���������");
	define("_FRIDAY", "�����");
	define("_SATURDAY", "������");

	/* Other various strings. */
   define("_DATE", "Date");
	define("_REDIRECT", "Redirect on post to ");
	define("_CATEGORY", "���������");
	define("_NO_CATEGORY", "��� ���������");
   define("_ALL_CATEGORIES", "������ ���������");
	define("_PRIORITY", "���������");
	define("_CREATION", "���������");
	define("_AUTHORIZATION", "������� �� �����������!");
	define("_LOGGING_OUT", "���� ��������...");
	define("_GROUP", "�����");
	define("_ACTIONS", "��������");
	define("_USERS_IN_GROUP", "���������� � ������� (�������� �� ����������)");
	define("_USERS_NOT_IN_GROUP", "����������� ����� ������� (�������� �� ��������)");
	define("_USERS", "�����������");
	define("_USER", "����������");
	define("_EMPTY", "������!");
	define("_REMOVE", "��������");
	define("_COMPLETED", "���������?");
	define("_COMPLETE", "�������");
	define("_OPEN", "�����������");
	define("_OVERDUE", "���������");
	define("_DAILY", "���������");
	define("_COMPLETED_FOR_DAY", "��������� �� ����");
	define("_REOPEN_TASK", "������ ��� ��������?");
	define("_STATUS", "������");
	define("_TITLE", "��������");
	define("_DEADLINE", "����� ����");
	define("_TASK", "������");
	define("_ASSIGNED_BY", "���������� ��");
	define("_ASSIGN_TO", "���������� ��");
	define("_COMPLETED_BY", "��������� ��");
	define("_COMPLETE_TASK_BY", "���������� �� ��������");
	define("_COMMENT", "��������");
	define("_COMMENT_BY", "�������� ��");
	define("_COMMENT_DATE", "���� �� ���������");
	define("_REMOVE_COMMENT", "������ ���������");
	define("_SUBMIT_TASK", "������� ������");
	define("_DAILY_TASK", "��������� ������");
	define("_UPDATE_TASK", "������ ������");
	define("_CURRENT_PASS", "������ ������");
	define("_ADD", "������");
	define("_VIEW_TASKS", "��� ��������");
	define("_EDIT", "����������");
	define("_ALLOW_OTHERS", "������� �� ������� �� �� ������� ������?");
	define("_ENABLE_EMAIL", "������� ����������� �� ��.����?");
	define("_UPDATE_INFO", "������ ������������");
	define("_PREV", "��������");
	define("_NEXT", "��������");
	define("_YOUR_CURRENT_PASS", "������ �� �������� �������� �� ������!");
	define("_YOUR_NEW_PASS", "������ �� ������� ���� ������!");
	define("_YOUR_VERIFY_PASS", "������ �� ���������� ������ �� ������!");
	define("_NEW_PASS_ERROR", "������ ������ �� ������� � ������� ���������!");
	define("_CURRENT_PASS_ERROR", "�������� �� ������ �� �������!");
	define("_CHANGE", "�������");
	define("_QUERY", "������");
	define("_OUTPUT", "�����");
	define("_RETURNED", "�������");

	/* These are used when adding a new user or group. */
	define("_ADD_USER", "������ ����");
	define("_ADD_GROUP", "������ �����");
	define("_USER_NAME", "������������� ���");
	define("_NEW_PASS", "���� ������");
	define("_VERIFY", "��������");
	define("_FIRST_NAME", "���");
	define("_LAST_NAME", "�������");
	define("_THEME", "����");
	define("_LANGUAGE", "Language");
	define("_EMAIL", "��.����");
	define("_AIM", "AIM");
	define("_IS_ADMIN", "�������������?");
	define("_GROUP_NAME", "��� �� �����");
	define("_GROUP_MEMBERS", "�������");
	define("_UPDATE", "������");
	define("_SELECT_GROUP", "������ �����");
	define("_FINISH_CREATE_USER", "������� ����/������ �� ����");

	/* These are for login stuff. */
	define("_LOGIN", "����");
	define("_LOGON_SUCCESSFUL", "������� �������");
	define("_PASSWORD", "������");
	define("_LOGGED_OUT", "����� ���������");
	define("_KEEP_LOGIN", "Keep me logged in on this machine");
	define("_WRONG_PASSWORD", "������ ����/������");

	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Undefined', 1 => '�����', 2 => '������', 3 => '�����', 4 => 'Delayed', 5 => 'On Hold');
?>
