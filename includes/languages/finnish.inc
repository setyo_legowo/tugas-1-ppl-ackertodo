<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: finnish.inc,v 1.4 2005/09/08 15:04:08 zoidian Exp $
*/
?>
<?php
    /* This is the Finnish language file where you define all the strings
     * used. Author: Matti J. K�rki <mjk@iki.fi> */

    /* These are for the Main Menu. */
    define("_SHOW_TASKS", "N�yt� teht�v�t");
    define("_THIS_WEEK", "T�m�n viikon teht�v�t");

    /* These are for the Status drop down. */
    define("_TASKS_DAILY", "P�ivitt�iset teht�v�t");
    define("_TASKS_OPEN_ENDED", "P��ttym�tt�m�t teht�v�t");
    define("_TASKS_WEEKLY", "Viikoittaiset teht�v�t");
    define("_TASKS_MONTHLY", "Kuukausittaiset teht�v�t");
    define("_TASKS_YEARLY", "Vuosittaiset teht�v�t");
    define("_TASKS_OPEN", "Avoimet teht�v�t");
    define("_TASKS_COMPLETED", "Suoritetut teht�v�t");
    define("_TASKS_OVERDUE", "My�h�styneet teht�v�t");
    define("_TASKS_ALL", "Kaikki teht�v�t");
    define("_TASKS_THIS_WEEK", "T�m�n viikon teht�v�t");
    define("_SEARCH_EVERYONES", "Kaikkien teht�v�t");
    define("_SEARCH_MINE", "Kaikki omat teht�v�ni");
    define("_SEARCH_USERS", ": Teht�v�t");
    define("_TASKS_DATE", " ");
    
    /* These are for the Main Menu. */
    define("_NEW_TASK", "Uusi teht�v�");
    define("_CALENDAR", "Kalenteri");
    define("_ADMIN_MENU", "Yll�pitovalikko");
    define("_USER_MENU", "K�ytt�j�valikko");
    define("_LOG_OUT", "Kirjaudu ulos");

    /* These are for the Admin Menu. */
    define("_ADMIN_ERROR", "Vain yll�pit�j� voi tehd� t�m�n!");
    define("_EDIT_GROUPS", "Muokkaa ryhmi�");
    define("_LIST_USERS", "Listaa k�ytt�j�t");
    define("_LIST_GROUPS", "Listaa ryhm�t");
    define("_CHANGE_PASS", "Muuta salasana");
    define("_EDIT_INFO", "Muokkaa k�ytt�j�tietoja");
    define("_LIST_CATEGORIES", "Listaa kategoriat");
    define("_REMOVE_CATEGORY", "Poista");
    define("_RENAME_CATEGORY", "Nime� uudelleen");
    define("_USER_CATEGORY_NAME", "K�ytt�j�n kategorian nimi");
    define("_GLOBAL_CATEGORY_NAME", "Globaalin kategorian nimi");
    define("_ADMIN_COMMAND", "Yll�pit�j�n komento");
    define("_COMMAND", "Komento");
    define("_COMMAND_ERROR", "on m��rittelem�t�n tai sinulla ei ole oikeuksia!");
    define("_MODULES", "Modulit");

    /* These are used in the buttons for each task. */
    define("_EDIT_TASK", "Muokkaa teht�v��");
    define("_DELETE_TASK", "Poista teht�v�");
    define("_VIEW_TASK", "N�yt� teht�v�");
    define("_ACCESS_DENIED", "P��sy ev�tty");

    /* Success/Failure strings. */
    define("_TASK_ADDED", "Teht�v� lis�tty onnistuneesti!");
    define("_TASK_UPDATED", "Teht�v� p�ivitetty onnistuneesti!");
    define("_TASK_DELETED", "Teht�v� poistettu onnistuneesti!");
    define("_TASK_OPENED", "Teht�v� avattu onnistuneesti!");
    define("_TASK_CLOSED", "Teht�v� suljettu onnistuneesti!");
    define("_USER_ADDED", "K�ytt�j� lis�tty onnistuneesti!");
    define("_USER_REMOVED", "K�ytt�j� poistettu onnistuneesti!");
    define("_ENTER_USER", "Sinun pit�� sy�tt�� k�ytt�j�nimi!");
    define("_ENTER_COMMENT", "Sinun pit�� sy�tt�� kommentti!");
    define("_ENTER_TITLE", "Sinun pit�� sy�tt�� otsikko!");
    define("_PASS_CHANGED", "Salasana vaihdettu onnistuneesti!");
    define("_ENTER_GROUP", "Sinun pit�� sy�tt�� ryhm�n nimi!");
    define("_GROUP_ADDED", "Ryhm� lis�tty onnistuneesti!");
    define("_GROUP_REMOVED", "Ryhm� poistettu onnistuneesti!");
    define("_GROUP_UPDATED", "Ryhm� p�ivitetty onnistuneesti!");
    define("_REMOVE_YOURSELF", "Et voi poistaa itse�si!");
    define("_REMOVED_FROM_GROUP", "K�ytt�j poistettu ryhm�st� onnistuneesti!");
    define("_ADDED_TO_GROUP", "K�ytt�j� lis�tty ryhm��n onnistuneesti!");
    define("_COMMENT_ADDED", "Kommentti lis�tty onnistuneesti!");
    define("_COMMENT_UPDATED", "Kommentti p�ivitetty onnistuneesti!");
    define("_VERIFY_PASS", "Sinun pit�� vahvistaa k�ytt�j�n salasana!");
    define("_PASS_ERROR", "Salasanat eiv�t vastaa toisiaan!");
    define("_INFO_UPDATED", "Tiedot p�ivitetty onnistuneesti!");
    define("_QUERY_ERROR", "Kysely ep�onnistui!");
    define("_COMMENT_REMOVED", "Kommentti poistettu onnistuneesti!");
    define("_CATEGORY_CREATED", "Kategoria lis�tty onnistuneesti!");
    define("_CATEGORY_UPDATED", "Kategoria p�ivitetty onnistuneesti!");
    define("_CATEGORY_DELETED", "Kategoria poistettu onnistuneesti!");
    define("_ADDED_TO_SUPERCAT", "Category added to supercat successfully!");
    define("_SUPERCAT_REMOVED", "Supercat removed successfully!");
    define("_ENTER_CATEGORY", "Sinun pit�� sy�tt�� kategorian nimi!");
    define("_NO_ID", "Ei ID:t� annettu!");
    define("_ALREADY_USER", "T�ll� nimell� on jo k�ytt�j�!");
    define("_ALREADY_GROUP", "T�ll� nimell� on jo ryhm�!");
    define("_MODULE_ENABLE", "Moduli otettu k�ytt��n onnistuneesti!");
    define("_MODULE_DISABLE", "Moduli poistettu k�yt�st� onnistuneesti!");
    define("_MODULE_INSTALL", "Moduli asennettu onnistuneesti!");
    define("_MODULE_UNINSTALL", "Moduli poistettu onnistuneesti!");

    /* These are used for the Calendar and formatting dates. */
    define("_JANUARY", "Tammikuu");
    define("_FEBUARY", "Helmikuu");
    define("_MARCH", "Maaliskuu");
    define("_APRIL", "Huhtikuu");
    define("_MAY", "Toukokuu");
    define("_JUNE", "Kes�kuu");
    define("_JULY", "Hein�kuu");
    define("_AUGUST", "Elokuu");
    define("_SEPTEMBER", "Syyskuu");
    define("_OCTOBER", "Lokakuu");
    define("_NOVEMBER", "Marraskuu");
    define("_DECEMBER", "Joulukuu");
    define("_SUNDAY", "Sunnuntai");
    define("_MONDAY", "Maanantai");
    define("_TUESDAY", "Tiistai");
    define("_WEDNESDAY", "Keskiviikko");
    define("_THURSDAY", "Torstai");
    define("_FRIDAY", "Perjantai");
    define("_SATURDAY", "Lauantai");
    define("_SUN", "Su");
    define("_MON", "Ma");
    define("_TUE", "Ti");
    define("_WED", "Ke");
    define("_THU", "To");
    define("_FRI", "Pe");
    define("_SAT", "La");

    /* Other various strings. */
    define("_VIEW_UPDATED", "Oletusn�kym� p�ivitetty onnistuneesti!");
    define("_CATEGORY", "Kategoria");
    define("_NO_CATEGORY", "Ei kategoriaa");
    define("_ALL_CATEGORIES", "Kaikki kategoriat");
    define("_PRIORITY", "Prioriteetti");
    define("_CREATION", "Luontiaika");
    define("_GROUP", "Ryhm�");
    define("_ACTIONS", "Toiminnot");
    define("_WHOS_TASK", "Kenen teht�v�");
    define("_USERS_IN_GROUP", "Ryhm�n k�ytt�j�t (klikkaa poistaaksesi)");
    define("_USERS_NOT_IN_GROUP", "Ryhm��n kuulumattomat k�ytt�j�t (klikkaa lis�t�ksesi)");
    define("_USER", "K�ytt�j�");
    define("_EMPTY", "Tyhj�!");
    define("_REMOVE", "Poista");
    define("_COMPLETE", "Tehty");
    define("_OPEN", "Avoin");
    define("_OVERDUE", "My�h�stynyt");
    define("_TODAY", "T�n��n");
    define("_DAILY", "P�ivitt�in");
    define("_OPEN_ENDED", "P��ttym�t�n");
    define("_WEEKLY", "Viikoittain");
    define("_MONTHLY", "Kuukausittain");
    define("_YEARLY", "Vuosittain");
    define("_NO_RECURRENCE", "Ei toistoa");
    define("_RECURRENCE", "Toisto");
    define("_RECURRE_EVERY", "Toisto joka");
    define("_RECURRE_ON", "Toista");
    define("_EVERY_MONTH", "Joka kuukausi");
    define("_STATUS", "Tila");
    define("_TITLE", "Otsikko");
    define("_DEADLINE", "Er�p�iv�");
    define("_TASK", "Teht�v�");
    define("_ASSIGNED_BY", "M��r�nnyt");
    define("_ASSIGN_TO", "M��r�tty");
    define("_COMPLETED_BY", "Suorittanut");
    define("_COMPLETE_TASK_BY", "Er�p�iv�");
    define("_COMMENT", "Kommentti");
    define("_COMMENT_BY", "Kommentoija");
    define("_COMMENT_DATE", "Kommentin p�iv�ys");
    define("_UPDATE_TASK", "P�ivit� teht�v�");
    define("_CURRENT_PASS", "Nykyinen salasana");
    define("_ADD", "Lis��");
    define("_FILTER", "Suodata");
    define("_VIEW_TASKS", "N�yt� teht�v�t");
    define("_EDIT", "Muokkaa");
    define("_DELETE", "Poista");
    define("_ALLOW_OTHERS", "Anna muiden m��r�t� teht�vi� minulle?");
    define("_PUBLIC_LIST", "Anna muiden n�hd� teht�v�lisani?");
    define("_UPDATE_INFO", "P�ivit� tiedot");
    define("_PREV", "Edellinen");
    define("_NEXT", "Seuraava");
    define("_YOUR_CURRENT_PASS", "Sinun pit�� sy�tt�� nykyinen salasanasi!");
    define("_YOUR_NEW_PASS", "Sinun pit�� sy�tt�� uusi salasana!");
    define("_YOUR_VERIFY_PASS", "Sinun pit�� vahvistaa uusi salasanasi!");
    define("_NEW_PASS_ERROR", "Uusi salasanasi ei vastaa vahvistustasi!");
    define("_CURRENT_PASS_ERROR", "Nykyinen salasanasi ei t�sm��!");
    define("_IS_UNDEFINED", "m��rittelem�t�n!");
    define("_AS_OF", " ");
    define("_PRINT", "Tulosta");
    define("_SET_VIEW", "Aseta oletusn�kym�ksi");
    define("_DESCRIPTION", "Kuvaus");
    define("_DISABLE", "Poista k�yt�st�");
    define("_ENABLE", "Ota k�ytt��n");
    define("_INSTALL", "Asenna");
    define("_UNINSTALL", "Poista");
    define("_MODULE", "Moduli");
    define("_CONFIGURE", "Konfiguroi");
    define("_CANCEL", "Peruuta");
    define("_WHAT_WOULD_YOU_LIKE", "Mit� haluaisit tehd�?");
    define("_RESET_ALL", "Palautetaanko kaikki teht�v�t tilaan 'Ei kategoriaa'?");
    define("_REMOVE_ALL", "Poistetaanko kaikki kategorian teht�v�t?");

    /* These are used when adding a new user or group. */
    define("_USER_NAME", "K�ytt�j�nimi");
    define("_NEW_PASS", "Uusi salasana");
    define("_VERIFY", "Vahvista");
    define("_FIRST_NAME", "Etunimi");
    define("_LAST_NAME", "Sukunimi");
    define("_THEME", "Teema");
    define("_LANGUAGE", "Kieli");
    define("_DATE_FORMAT", "P�iv�yksen muoto");
    define("_EMAIL", "S�hk�posti");
    define("_AIM", "AIM-k�ytt�j�nimi");
    define("_IS_ADMIN", "Yll�pit�j�?");
    define("_GROUP_NAME", "Ryhm�n nimi");
    define("_GROUP_MEMBERS", "J�senet");
    define("_UPDATE", "P�ivit�");
    define("_SELECT_GROUP", "Valitse ryhm�");
    define("_FINISH_CREATE_USER", "Aseta k�ytt�j�tiedot/salasana");
    define("_AUTO_DAILY", "Uudet teht�v�t p�ivitt�isi� oletuksena?");

    /* These are for login stuff. */
    define("_LOGIN", "Kirjautuminen");
    define("_LOGON_SUCCESSFUL", "Kirjautuminen onnistui");
    define("_PASSWORD", "Salasana");
    define("_LOGGED_OUT", "Olet kirjautunut ulos");
    define("_KEEP_LOGIN", "Pid� minut kirjautuneena t�ll� koneella");
    define("_WRONG_PASSWORD", "V��r� k�ytt�j�nimi tai salasana");
    
    /* Supercats */
    define("_SUPERCAT", "Supercat");
    define("_LIST_SUPERCATS", "Listaa Supercatit");
    define("_CATS_IN_SUPERCAT", "Supercatin kategoriat");
    define("_REMOVED_FROM_SUPERCAT", "Kategoria poistettu Supercatista onnistuneesti!");
    define("_CATS_NOT_IN_SUPERCAT", "Ei kategorioita Supercatissa");
    define("_GROUP_SUPERCAT", "Supercat luotu onnistuneesti!");
    define("_ALREADY_SUPERCAT", "T�ll� nimell� on jo Supercat olemassa!");
    
    /* Email Notifications */
    define("_SUBJECT", "Ilmoitus teht�v�nannosta!");
    define("_GREETING", "Sinulle on osoitettu teht�v�!");
    define("_CLOSING", "T�m�n ilmoituksen l�hetti:");
	
    global $_PRIORITY;
    $_PRIORITY = array(0 => 'M��rittelem�t�n', 1 => 'Matala', 2 => 'Normaali', 3 => 'Korkea', 4 => 'My�h�ss�', 5 => 'Odottaa');
?>
