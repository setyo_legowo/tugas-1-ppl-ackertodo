<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: italian.inc,v 1.4 2006/01/22 18:57:35 zoidian Exp $
*/
?>
<?php
	/* This is the Italian language file where you define all the strings 
    * used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Mostra Task");
	define("_THIS_WEEK", "Task di questa settimana");

    /* These are for the Status drop down. */
    define("_TASKS_DAILY", "Task giornalieri");
	define("_TASKS_OPEN_ENDED", "Open-Ended Tasks");
	define("_TASKS_WEEKLY", "Task settimanali");
	define("_TASKS_MONTHLY", "Task mensili");
	define("_TASKS_YEARLY", "Task annuali");
	define("_TASKS_OPEN", "Task aperti");
	define("_TASKS_COMPLETED", "Task completati");
	define("_TASKS_OVERDUE", "Task fuoritempo");
	define("_TASKS_ALL", "Tutti i Task");
	define("_TASKS_THIS_WEEK", "Task di questa settimana");
	define("_SEARCH_EVERYONES", "Task di TUTTI");
	define("_SEARCH_MINE", "Tutti i MIEI Task");
	define("_SEARCH_USERS", " Task");
	define("_TASKS_DATE", "Al ");

    /* These are for the Main Menu. */
	define("_NEW_TASK", "Nuovo Task");
	define("_CALENDAR", "Calendario");
	define("_ADMIN_MENU", "Menu Admin");
	define("_USER_MENU", "Menu Utente");
	define("_LOG_OUT", "Log Out");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "Solo admin può fare questo!");
	define("_EDIT_GROUPS", "Modifica Gruppi");
	define("_LIST_USERS", "Lista Utenti");
	define("_LIST_GROUPS", "Lista Gruppi");
	define("_CHANGE_PASS", "Cambia Password");
	define("_EDIT_INFO", "Modifica Informazioni Utente");
	define("_LIST_CATEGORIES", "Lista Categorie");
	define("_REMOVE_CATEGORY", "Rimuovi categorie");
	define("_RENAME_CATEGORY", "Rinomina categorie");
	define("_USER_CATEGORY_NAME", "Nome categoria Utente");
	define("_GLOBAL_CATEGORY_NAME", "Nome categoria globale");
	define("_ADMIN_COMMAND", "Comandi Admin");
    define("_COMMAND", "Comando");
	define("_COMMAND_ERROR", "è indefinito o non hai i permessi!");
    define("_MODULES", "Moduli");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "Modifica Task");
	define("_DELETE_TASK", "Cancella Task");
	define("_VIEW_TASK", "Vedi Task");
	define("_ACCESS_DENIED", "Accesso Negato");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "Task aggiunto con successo!");
	define("_TASK_UPDATED", "Task aggiornato con successo!");
	define("_TASK_DELETED", "Task cancellato con successo!");
	define("_TASK_OPENED", "Task aperto con successo!");
	define("_TASK_CLOSED", "Task chiuso con successo!");
	define("_USER_ADDED", "Utente aggiunto con successo!");
	define("_USER_REMOVED", "Utente rimosso con successo!");
	define("_ENTER_USER", "Devi inserire il nome di un Utente!");
	define("_ENTER_COMMENT", "Devi inserire un commento!");
	define("_ENTER_TITLE", "Devi inserire un titolo!");
	define("_PASS_CHANGED", "Password cambiata con successo!");
	define("_ENTER_GROUP", "Devi inserire il nome di un gruppo!");
	define("_GROUP_ADDED", "Groppo aggiunto con successo!");
	define("_GROUP_REMOVED", "Groppo rimosso con successo!");
	define("_GROUP_UPDATED", "Groppo aggiornato con successo!");
	define("_REMOVE_YOURSELF", "Non puoi rimuovere te stesso!");
	define("_REMOVED_FROM_GROUP", "Utente rimosso dal gruppo con successo!");
	define("_ADDED_TO_GROUP", "Utente aggiunto al gruppo con successo!");
	define("_COMMENT_ADDED", "Commento aggiunto con successo!");
	define("_COMMENT_UPDATED", "Commento aggiornato con successo!");
	define("_VERIFY_PASS", "Devi verificare la password dell'utente!");
	define("_PASS_ERROR", "La passwords non corrisponde!");
	define("_INFO_UPDATED", "Informazioni aggiornate con successo!");
	define("_QUERY_ERROR", "La richiesta è infruttuosa!");
	define("_COMMENT_REMOVED", "Commento rimosso con successo!");
	define("_CATEGORY_CREATED", "Categoria aggiunta con successo!");
	define("_CATEGORY_UPDATED", "Categoria aggiornata con successo!");
	define("_CATEGORY_DELETED", "Categoria cancellata con successo!");
	define("_ADDED_TO_SUPERCAT", "Categoria aggiunta a supercategoria con successo!");
	define("_SUPERCAT_REMOVED", "Supercategoria rimossa con successo!");
	define("_ENTER_CATEGORY", "Devi inserire il nome di una categoria!");
	define("_NO_ID", "Nessun id fornito!");
	define("_ALREADY_USER", "Esiste già un utente con quel nome!");
	define("_ALREADY_GROUP", "Esiste già un gruppo con quel nome!");
    define("_MODULE_ENABLE", "Modulo attivato con successo!");
    define("_MODULE_DISABLE", "Modulo disattivato con successo!");
    define("_MODULE_INSTALL", "Modulo installato con successo!");
    define("_MODULE_UNINSTALL", "Modulo disinstallato con successo!");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "Gennaio");
	define("_FEBUARY", "Febbraio");
	define("_MARCH", "Marzo");
	define("_APRIL", "Aprile");
	define("_MAY", "Maggio");
	define("_JUNE", "Giugno");
	define("_JULY", "Luglio");
	define("_AUGUST", "Agosto");
	define("_SEPTEMBER", "Settembre");
	define("_OCTOBER", "Ottobre");
	define("_NOVEMBER", "Novembre");
	define("_DECEMBER", "Dicembre");
	define("_SUNDAY", "Domenica");
	define("_MONDAY", "Lunedì");
	define("_TUESDAY", "Martedì");
	define("_WEDNESDAY", "Mercoledì");
	define("_THURSDAY", "Giovedì");
	define("_FRIDAY", "Venerdì");
	define("_SATURDAY", "Sabato");
	define("_SUN", "Dom");
	define("_MON", "Lun");
	define("_TUE", "Mar");
	define("_WED", "Mer");
	define("_THU", "Gio");
	define("_FRI", "Ven");
	define("_SAT", "Sab");

	/* Other various strings. */
    define("_CHANGE_DUE_DATE", "Cambia data di scadenza");
    define("_VIEW_UPDATED", "Vista di default aggiornata con successo!");
	define("_CATEGORY", "Categoria");
	define("_NO_CATEGORY", "Nessuna Categoria");
    define("_ALL_CATEGORIES", "Tutte le Categorie");
	define("_PRIORITY", "Priorità");
	define("_CREATION", "Creazione");
	define("_GROUP", "Gruppi");
	define("_ACTIONS", "Azioni");
	define("_WHOS_TASK", "Task assegnato a");
	define("_WHO", "Chi");
	define("_USERS_IN_GROUP", "Utenti nel gruppo (click per rimuovere)");
	define("_USERS_NOT_IN_GROUP", "Utenti non in gruppo (click per aggiungere)");
	define("_USER", "Utente");
	define("_EMPTY", "Vuoto!");
	define("_REMOVE", "Rimosso");
	define("_COMPLETE", "Completo");
	define("_OPEN", "Aperto");
	define("_OVERDUE", "Fuoridata");
	define("_TODAY", "Oggi");
	define("_DAILY", "Giornaliero");
    define("_OPEN_ENDED", "Open-Ended");
	define("_WEEKLY", "Settimanale");
	define("_MONTHLY", "Mensile");
	define("_YEARLY", "Annuale");
    define("_DATE", "Data");
    define("_DAY", "Giorno");
    define("_DAYS", "Giorni");
	define("_NO_RECURRENCE", "Nessuna Ricorrenza");
    define("_RECURRENCE", "Ricorrenza");
	define("_RECURRE_EVERY", "Ricorre ogni");
	define("_RECURRE_ON", "Ricorre");
	define("_EVERY_MONTH", "Ogni mese");
	define("_STATUS", "Stato");
	define("_TITLE", "Titolo");
	define("_DEADLINE", "Tempolimite");
	define("_TASK", "Task");
	define("_ASSIGNED_BY", "Assegnato Da");
	define("_ASSIGN_TO", "Assegnato A");
	define("_COMPLETED_BY", "Completato Da");
	define("_COMPLETE_TASK_BY", "Task assegnato");
	define("_COMMENT", "Commento");
	define("_COMMENT_BY", "Commento Da");
	define("_COMMENT_DATE", "Data Commento");
	define("_UPDATE_TASK", "Aggiorna Task");
	define("_CURRENT_PASS", "Password Corrente");
	define("_ADD", "Aggiungi");
	define("_FILTER", "Filtro");
	define("_VIEW_TASKS", "Vedi Task");
	define("_EDIT", "Modifica");
	define("_DELETE", "Cancella");
   define("_ENABLE_AJAX", "Attivare Ajax?");
	define("_ALLOW_OTHERS", "Permetti ad altri di assegnare task a me?");
	define("_PUBLIC_LIST", "Permetti ad altri di vedere la mia lista di TODO?");
	define("_UPDATE_INFO", "Aggiorna Informazioni");
	define("_PREV", "Prec");
	define("_NEXT", "Pros");
	define("_YOUR_CURRENT_PASS", "Devi inserire la tua password corrente!");
	define("_YOUR_NEW_PASS", "Devi inserire una nuova password!");
	define("_YOUR_VERIFY_PASS", "Devi confermare la tua nuova password!");
	define("_NEW_PASS_ERROR", "La tua nuova password non corrisponde alla verification!");
	define("_CURRENT_PASS_ERROR", "La tua password non corrisponde!");
    define("_IS_UNDEFINED", "è indefinito!");
    define("_AS_OF", " come di ");
    define("_PRINT", "Stampa");
    define("_SET_VIEW", "Imposta come vista di default");
    define("_DESCRIPTION", "Descrizione");
    define("_DISABLE", "Disattiva");
    define("_ENABLE", "Attiva");
    define("_INSTALL", "Installa");
    define("_UNINSTALL", "Disinstalla");
    define("_MODULE", "Modulo");
    define("_CONFIGURE", "Configura");
    define("_CANCEL", "Annulla");
    define("_WHAT_WOULD_YOU_LIKE", "Cosa preferisci fare?");
    define("_RESET_ALL", "Porta TUTTI i task a 'Nessuna Categoria'?");
    define("_REMOVE_ALL", "Cancella TUTTI i task nella categoria?");

	/* These are used when adding a new user or group. */
	define("_USER_NAME", "Name Utente");
	define("_NEW_PASS", "Nuova Password");
	define("_VERIFY", "Verifica");
	define("_FIRST_NAME", "Name");
	define("_LAST_NAME", "Cognome");
	define("_THEME", "Tema");
	define("_LANGUAGE", "Linguaggio");
	define("_EMAIL", "Email");
	define("_AIM", "AIM Screenname");
	define("_IS_ADMIN", "Amministratore?");
	define("_GROUP_NAME", "Name Gruppo");
	define("_GROUP_MEMBERS", "Membri");
	define("_UPDATE", "Aggiorna");
	define("_SELECT_GROUP", "Seleziona un gruppo");
	define("_FINISH_CREATE_USER", "Imposta utente info/pass");
	define("_AUTO_DAILY", "Nuovi tasks giornalieri per default?");

	/* These are for login stuff. */
	define("_LOGIN", "Login");
	define("_LOGON_SUCCESSFUL", "Login Corretto");
	define("_PASSWORD", "Password");
	define("_LOGGED_OUT", "Ti sei scollegato");
	define("_KEEP_LOGIN", "Tienimi loggato in questo computer");
	define("_WRONG_PASSWORD", "Username o password SBAGLIATA");
	
	/* Supercats */
	define("_SUPERCAT", "Supercategoria");
	define("_LIST_SUPERCATS", "Lista Supercategorie");
	define("_CATS_IN_SUPERCAT", "Categorie in Supercategoria");
	define("_REMOVED_FROM_SUPERCAT", "Categoria cancellata da Supercategoria con successo!");
	define("_CATS_NOT_IN_SUPERCAT", "Categorie non in Supercategoria");
	define("_GROUP_SUPERCAT", "Supercategoria creata con successo!");
	define("_ALREADY_SUPERCAT", "Esiste già una Supercategoria con questo nome!");
	
    /* Email Notifications */
    define("_SUBJECT", "Notifica dell'assegnazione del task");
    define("_GREETING", "ti è stato assegnato un task");
    define("_CLOSING", "Notifica realizzata da:");

	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Non Definita', 1 => 'Bassa', 2 => 'Media', 3 => 'Alta', 4 => 'Delayed', 5 => 'On Hold');
?>
