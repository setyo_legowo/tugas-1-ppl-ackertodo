<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: french.inc,v 1.11 2006/08/13 02:49:50 zoidian Exp $
*/
?>
<?php
   /* This is the French language file where you define all the strings
    * used. */
    
	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Afficher les t�ches");
	define("_THIS_WEEK", "Les t�ches de cette semaine");

    /* These are for the Status drop down. */
	define("_TASKS_DAILY", "T�ches quotidiennes");
	define("_TASKS_OPEN_ENDED", "T�ches ouvertes");
	define("_TASKS_WEEKLY", "T�ches de la semaine");
	define("_TASKS_MONTHLY", "T�ches mensuelles");
	define("_TASKS_YEARLY", "T�ches annuelles");
	define("_TASKS_OPEN", "T�ches ouvertes");
	define("_TASKS_COMPLETED", "T�ches r�alis�es");
	define("_TASKS_OVERDUE", "T�ches en retard");
	define("_TASKS_ALL", "Toutes les t�ches");
	define("_TASKS_THIS_WEEK", "T�ches de cette semaine");
	define("_SEARCH_EVERYONES", "Les t�ches de tous le monde");
	define("_SEARCH_MINE", "Toutes mes t�ches");
	define("_SEARCH_USERS", " (ses t�ches)");
	define("_TASKS_DATE", "Le");
	
	/* These are for the Main Menu. */
	define("_NEW_TASK", "Nouvelle t�che");
	define("_CALENDAR", "Calendrier");
	define("_ADMIN_MENU", "Menu d'administration");
	define("_USER_MENU", "Menu de l'utilisateur");
	define("_LOG_OUT", "D�connexion");
	
	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "R�serv� � l'administrateur !");
	define("_EDIT_GROUPS", "�diter les groupes");
	define("_LIST_USERS", "Afficher les utilisateurs");
	define("_LIST_GROUPS", "Afficher les groupes");
	define("_CHANGE_PASS", "Changer le mot de passe");
	define("_EDIT_INFO", "�diter les informations de l'utilisateur");
	define("_LIST_CATEGORIES", "Afficher les cat�gories");
	define("_REMOVE_CATEGORY", "Supprimer la cat�gorie");
	define("_RENAME_CATEGORY", "Renommer la cat�gorie");
	define("_USER_CATEGORY_NAME", "Nom de la cat�gorie utilisateur");
	define("_GLOBAL_CATEGORY_NAME", "Nom de la cat�gorie globale");
	define("_ADMIN_COMMAND", "Commandes de l'administrateur");
    define("_COMMAND", "Commande");
	define("_COMMAND_ERROR", "non d�fini ou vous n'�tes pas administrateur !");
    define("_MODULES", "Modules");
    
	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "�diter la t�che");
	define("_DELETE_TASK", "Supprimer la t�che");
	define("_VIEW_TASK", "Afficher La t�che");
	define("_ACCESS_DENIED", "Acc�s refus�");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "T�che ajout�e avec succ�s !");
	define("_TASK_UPDATED", "T�che mise � jour avec succ�s !");
	define("_TASK_DELETED", "T�che supprim�e avec succ�s !");
	define("_TASK_OPENED", "T�che ouverte avec succ�s !");
	define("_TASK_CLOSED", "T�che cl�tur�e avec succ�s !");
	define("_USER_ADDED", "Utilisateur ajout� avec succ�s !");
	define("_USER_REMOVED", "Utilisateur supprim� avec succ�s !");
	define("_ENTER_USER", "Vous devez saisir un nom d'utilisateur !");
	define("_ENTER_COMMENT", "Vous devez saisir un commentaire !");
	define("_ENTER_TITLE", "Vous devez saisir un titre !");
	define("_PASS_CHANGED", "Mot de passe chang� !");
	define("_ENTER_GROUP", "Vous devez saisir un nom de groupe !");
	define("_GROUP_ADDED", "Groupe ajout� !");
	define("_GROUP_REMOVED", "Groupe supprim�");
	define("_GROUP_UPDATED", "Groupe mis-�-jour");
	define("_REMOVE_YOURSELF", "Vous ne pouvez pas vous supprimer !");
	define("_REMOVED_FROM_GROUP", "Utilisateur retir� du groupe avec succ�s !");
	define("_ADDED_TO_GROUP", "Utilisateur ajout� au groupe !");
	define("_COMMENT_ADDED", "Commentaire ajout� avec succ�s !");
	define("_COMMENT_UPDATED", "Commentaire mis � jour avec succ�s !");
	define("_VERIFY_PASS", "V�rifier le mot de passe de l'utilisateur !");
	define("_PASS_ERROR", "Les mots de passe ne correspondent pas !");
	define("_INFO_UPDATED", "Information mise � jour avec succ�s !");
	define("_QUERY_ERROR", "La question �tait incorrecte !");
	define("_COMMENT_REMOVED", "Commentaire supprim� !");
	define("_CATEGORY_CREATED", "Cat�gorie ajout�e !");
	define("_CATEGORY_UPDATED", "Cat�gorie mise � jour !");
	define("_CATEGORY_DELETED", "Cat�gorie supprim�e !");
	define("_ADDED_TO_SUPERCAT", "Cat�gorie ajout�e aux super-cat�gories !");
	define("_SUPERCAT_REMOVED", "Super-cat�gorie supprim�e !");
	define("_ENTER_CATEGORY", "Vous devez saisir un nom de cat�gorie !");
	define("_NO_ID", "Aucun ID sp�cifi� !");
	define("_ALREADY_USER", "Cet utilisateur existe d�j� !");
	define("_ALREADY_GROUP", "Ce groupe existe d�j� !");
    define("_MODULE_ENABLE", "Module activ� avec succ�s !");
    define("_MODULE_DISABLE", "Module d�sactiv� avec succ�s !");
    define("_MODULE_INSTALL", "Module install� avec succ�s !");
    define("_MODULE_UNINSTALL", "Module d�sinstall� avec succ�s !");
    
    
    /* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "Janvier");
	define("_FEBUARY", "F�vrier");
	define("_MARCH", "Mars");
	define("_APRIL", "Avril");
	define("_MAY", "Mai");
	define("_JUNE", "Juin");
	define("_JULY", "Juillet");
	define("_AUGUST", "Ao�t");
	define("_SEPTEMBER", "Septembre");
	define("_OCTOBER", "Octobre");
	define("_NOVEMBER", "Novembre");
	define("_DECEMBER", "D�cembre");
	define("_SUNDAY", "Dimanche");
	define("_MONDAY", "Lundi");
	define("_TUESDAY", "Mardi");
	define("_WEDNESDAY", "Mercredi");
	define("_THURSDAY", "Jeudi");
	define("_FRIDAY", "Vendredi");
	define("_SATURDAY", "Samedi");
	define("_SUN", "Dim");
	define("_MON", "Lun");
	define("_TUE", "Mar");
	define("_WED", "Mer");
	define("_THU", "Jeu");
	define("_FRI", "Ven");
	define("_SAT", "Sam");

	/* Other various strings. */
    define("_CHANGE_DUE_DATE", "Changer la date d'�ch�ance");
    define("_VIEW_UPDATED", "Vue par d�faut mise � jour !");
    define("_CATEGORY", "Cat�gorie");
	define("_NO_CATEGORY", "Aucune cat�gorie");
    define("_ALL_CATEGORIES", "Toutes les cat�gories");
	define("_PRIORITY", "Priorit�");
	define("_CREATION", "Cr�ation");
	define("_GROUP", "Groupe");
	define("_ACTIONS", "Actions");
	define("_WHOS_TASK", "T�ches de");
	define("_WHO", "Qui");
	define("_USERS_IN_GROUP", "Utilisateurs du groupe (cliquer pour supprimer)");
	define("_USERS_NOT_IN_GROUP", "Utilisateurs n'�tant pas dans le groupe (cliquer pour ajouter)");
	define("_USER", "Utilisateur"); 
	define("_EMPTY", "Vide !");
	define("_REMOVE", "Supprimer ?");
	define("_COMPLETE", "Terminer");
	define("_OPEN", "Ouvert");
	define("_OVERDUE", "En retard");
	define("_TODAY", "Aujourd'hui");
	define("_DAILY", "Quotidien");
	define("_OPEN_ENDED", "Sans-limite");
	define("_WEEKLY", "Toutes les semaines");
	define("_MONTHLY", "Mensuellement");
	define("_YEARLY", "Annuellement");
    define("_DATE", "Date");
    define("_DAY", "Jour");
    define("_DAYS", "Jours");
	define("_NO_RECURRENCE", "Pas de r�currence");
    define("_RECURRENCE", "R�currence");
	define("_RECURRE_EVERY", "R�currence chaque");
	define("_RECURRE_ON", "R�currence pour");
	define("_EVERY_MONTH", "Chaque mois");
	define("_STATUS", "Statut");
	define("_TITLE", "Titre");
	define("_DEADLINE", "Date limite");
	define("_TASK", "T�che");
	define("_ASSIGNED_BY", "Affect� par");
	define("_ASSIGN_TO", "Affect� �");
	define("_COMPLETED_BY", "Termin� par");
	define("_COMPLETE_TASK_BY", "Accomplir la t�che avant");
	define("_COMMENT", "Commentaire");
	define("_COMMENT_BY", "Commentaire de");
	define("_COMMENT_DATE", "Date du commentaire");
	define("_UPDATE_TASK", "Mettre � jour la t�che");
	define("_CURRENT_PASS", "Mot de passe actuel");
	define("_ADD", "Ajouter");
	define("_FILTER", "Filtre");
	define("_VIEW_TASKS", "Voir les t�ches");
	define("_EDIT", "Modifier");
	define("_DELETE", "Supprimer");
    define("_ENABLE_AJAX", "Activer l'AJAX ?");
	define("_ALLOW_OTHERS", "Permettre � d'autres de m'affecter des t�ches ?");
	define("_PUBLIC_LIST", "Permettre aux autres de voir mes t�ches ?");
	define("_UPDATE_INFO", "Mettre � jour l'information");
	define("_PREV", "Pr�c�dent");
	define("_NEXT", "Suivant");
	define("_YOUR_CURRENT_PASS", "Saisissez votre mot de passe actuel !");
	define("_YOUR_NEW_PASS", "Saisissez un nouveau mot de passe !");
	define("_YOUR_VERIFY_PASS", "V�rifiez votre nouveau mot de passe !");
	define("_NEW_PASS_ERROR", "Nouveau mot de passe incorrect !");
	define("_CURRENT_PASS_ERROR", "Mot de passe actuel incorrect !");
    define("_IS_UNDEFINED", "est ind�finis !");
    define("_AS_OF", "tel quel");
	define("_PRINT", "Imprimer");
    define("_EXPORT", "Exporter");
    define("_SET_VIEW", "Configurer comme vue par d�faut");
    define("_DESCRIPTION", "Description");
    define("_INSTALL", "Installer");
    define("_ENABLE", "Activer");
    define("_DISABLE", "D�sactiver");
    define("_UNINSTALL", "D�sinstaller");
    define("_MODULE", "Module");
    define("_CONFIGURE", "Configurer");
    define("_CANCEL", "Annuler");
    define("_WHAT_WOULD_YOU_LIKE", "Que voulez vous faire ?");
    define("_RESET_ALL", "R�initialiser toutes les t�ches � 'Pas de cat�gorie' ?");
    define("_REMOVE_ALL", "Retirer toutes les t�ches de cette cat�gorie ?");

	/* These are used when adding a new user or group. */
	define("_USER_NAME", "Nom d'utilisateur");
	define("_NEW_PASS", "Nouveau mot de passe");
	define("_VERIFY", "V�rifier");
	define("_FIRST_NAME", "Pr�nom");
	define("_LAST_NAME", "Nom");
	define("_THEME", "Th�me");
	define("_LANGUAGE", "Langue");
	define("_DATE_FORMAT", "Format de date");
	define("_EMAIL", "Courriel");
	define("_AIM", "Nom AIM (screenname)");
	define("_IS_ADMIN", "Administrateur ?");
	define("_GROUP_NAME", "Nom du groupe");
	define("_GROUP_MEMBERS", "Membres");
	define("_UPDATE", "Mettre � jour");
	define("_SELECT_GROUP", "S�lectionner un groupe");
	define("_FINISH_CREATE_USER", "Terminer l'ajout de l'usager");
	define("_AUTO_DAILY", "Nouvelles t�ches quotidiennes par d�faut ?");

	/* These are for login stuff. */
	define("_LOGIN", "Login");
	define("_LOGON_SUCCESSFUL", "Connexion r�ussie");
	define("_PASSWORD", "Mot de passe");
    define("_LOGGED_OUT", "Vous �tes maintenant d�connect�");
	define("_KEEP_LOGIN", "Me laisser connect�");
	define("_WRONG_PASSWORD", "Mauvais login ou mot de passe");

	/* Supercats */
	define("_SUPERCAT", "Supercat�gorie");
	define("_LIST_SUPERCATS", "Lister les super-cat�gories");
	define("_CATS_IN_SUPERCAT", "Cat�gories de la super-cat�gorie");
	define("_REMOVED_FROM_SUPERCAT", "Cat�gorie supprim�e de la super-cat�gorie avec succ�s !");
	define("_CATS_NOT_IN_SUPERCAT", "Cat�gories n'appartenant pas � la super-cat�gorie");
	define("_GROUP_SUPERCAT", "Super-cat�gorie cr��e avec succ�s !");
	define("_ALREADY_SUPERCAT", "Il existe d�j� une super-cat�gorie avec ce nom !");

    /* Email Notifications */
    define("_SUBJECT", "Notification d'une nouvelle t�che");
    define("_GREETING", "On vous a assign� une t�che !");
    define("_CLOSING", "Cette notification vous � �t� d�livr�e par:");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Undefined', 1 => 'Low', 2 => 'Medium', 3 => 'High', 4 => 'Delayed', 5 => 'On Hold');
?>
