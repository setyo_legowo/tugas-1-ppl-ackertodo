<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: japanese.inc,v 1.1 2006/09/25 15:04:43 zoidian Exp $
*/
?>
<?php
	/* This is the Japanese language file where you define all the strings
	* used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "タスクを表示");
	define("_THIS_WEEK", "今週のタスク");

    /* These are for the Status drop down. */
	define("_TASKS_DAILY", "毎日のタスク");
	define("_TASKS_OPEN_ENDED", "期限のないタスク");
	define("_TASKS_WEEKLY", "毎週のタスク");
	define("_TASKS_MONTHLY", "毎月のタスク");
	define("_TASKS_YEARLY", "毎年のタスク");
	define("_TASKS_OPEN", "実行中のタスク");
	define("_TASKS_COMPLETED", "完了したタスク");
	define("_TASKS_OVERDUE", "遅れているタスク");
	define("_TASKS_ALL", "すべてのタスク");
	define("_TASKS_THIS_WEEK", "今週のタスク");
	define("_SEARCH_EVERYONES", "全員のタスク");
	define("_SEARCH_MINE", "すべての自分のタスク");
	define("_SEARCH_USERS", "のタスク");
	define("_TASKS_DATE", "On ");
    
    /* These are for the Main Menu. */
	define("_NEW_TASK", "新しいタスクを作成");
	define("_CALENDAR", "カレンダー");
	define("_ADMIN_MENU", "管理者メニュー");
	define("_USER_MENU", "ユーザーメニュー");
	define("_LOG_OUT", "ログアウト");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "管理者のみが実行できます！");
	define("_EDIT_GROUPS", "グループを編集");
	define("_LIST_USERS", "ユーザー一覧");
	define("_LIST_GROUPS", "グループ一覧");
	define("_CHANGE_PASS", "パスワードを変更");
	define("_EDIT_INFO", "ユーザー情報を編集");
	define("_LIST_CATEGORIES", "カテゴリー一覧");
	define("_REMOVE_CATEGORY", "カテゴリーを削除");
	define("_RENAME_CATEGORY", "カテゴリー名を変更");
	define("_USER_CATEGORY_NAME", "ユーザーのカテゴリー名");
	define("_GLOBAL_CATEGORY_NAME", "共通のカテゴリー名");
	define("_ADMIN_COMMAND", "管理者コマンド");
    define("_COMMAND", "コマンド");
	define("_COMMAND_ERROR", "は定義されていないか権限がありません");
    define("_MODULES", "モジュール");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "タスクを編集");
	define("_DELETE_TASK", "タスクを削除");
	define("_VIEW_TASK", "タスクを閲覧");
	define("_ACCESS_DENIED", "アクセスが拒否されました");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "タスクを追加しました");
	define("_TASK_UPDATED", "タスクを更新しました");
	define("_TASK_DELETED", "タスクを削除しました");
	define("_TASK_OPENED", "タスクを開始しました");
	define("_TASK_CLOSED", "タスクを閉じました");
	define("_USER_ADDED", "ユーザーを追加しました");
	define("_USER_REMOVED", "ユーザーを削除しました");
	define("_ENTER_USER", "ユーザー名を入力してください");
	define("_ENTER_COMMENT", "コメントを入力してください");
	define("_ENTER_TITLE", "タイトルを入力してください");
	define("_PASS_CHANGED", "パスワードを変更しました");
	define("_ENTER_GROUP", "グループ名を入力してください");
	define("_GROUP_ADDED", "新しいグループを追加しました");
	define("_GROUP_REMOVED", "グループを削除しました");
	define("_GROUP_UPDATED", "グループを更新しました");
	define("_REMOVE_YOURSELF", "自分自身を削除できません");
	define("_REMOVED_FROM_GROUP", "ユーザーをグループから削除しました");
	define("_ADDED_TO_GROUP", "ユーザーをグループに追加しました");
	define("_COMMENT_ADDED", "コメントを追加しました");
	define("_COMMENT_UPDATED", "コメントを更新しました");
	define("_VERIFY_PASS", "ユーザーのパスワードを確認してください");
	define("_PASS_ERROR", "パスワードが一致しません");
	define("_INFO_UPDATED", "情報を更新しました");
	define("_QUERY_ERROR", "クエリーに失敗しました");
	define("_COMMENT_REMOVED", "コメントを削除しました");
	define("_CATEGORY_CREATED", "カテゴリーを追加しました");
	define("_CATEGORY_UPDATED", "カテゴリーを更新しました");
	define("_CATEGORY_DELETED", "カテゴリーを削除しました");
	define("_ADDED_TO_SUPERCAT", "カテゴリーを supercat に追加しました");
	define("_SUPERCAT_REMOVED", "Supercat を削除しました");
	define("_ENTER_CATEGORY", "カテゴリー名を入力してください");
	define("_NO_ID", "ＩＤが入力されてません");
	define("_ALREADY_USER", "すでに同じ名前のユーザーが存在しています");
	define("_ALREADY_GROUP", "すでに同じ名前のグループが存在しています");
    define("_MODULE_ENABLE", "モジュールを有効にしました");
    define("_MODULE_DISABLE", "モジュールを無効にしました");
    define("_MODULE_INSTALL", "モジュールをインストールしました");
    define("_MODULE_UNINSTALL", "モジュールをアンインストールしました");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "１月");
	define("_FEBUARY", "２月");
	define("_MARCH", "３月");
	define("_APRIL", "４月");
	define("_MAY", "５月");
	define("_JUNE", "６月");
	define("_JULY", "７月");
	define("_AUGUST", "８月");
	define("_SEPTEMBER", "９月");
	define("_OCTOBER", "10月");
	define("_NOVEMBER", "11月");
	define("_DECEMBER", "12月");
	define("_SUNDAY", "日");
	define("_MONDAY", "月");
	define("_TUESDAY", "火");
	define("_WEDNESDAY", "水");
	define("_THURSDAY", "木");
	define("_FRIDAY", "金");
	define("_SATURDAY", "土");
	define("_SUN", "日");
	define("_MON", "月");
	define("_TUE", "火");
	define("_WED", "水");
	define("_THU", "木");
	define("_FRI", "金");
	define("_SAT", "土");

	/* Other various strings. */
    define("_CHANGE_DUE_DATE", "期日を変更する");
    define("_VIEW_UPDATED", "デフォルトの表示を更新しました");
	define("_CATEGORY", "カテゴリー");
	define("_NO_CATEGORY", "カテゴリーなし");
    define("_ALL_CATEGORIES", "すべてのカテゴリー");
	define("_PRIORITY", "優先順位");
	define("_CREATION", "作成");
	define("_GROUP", "グループ");
	define("_ACTIONS", "アクション");
	define("_WHOS_TASK", "担当者");
	define("_WHO", "Who");
	define("_USERS_IN_GROUP", "グループのユーザー（クリックで削除）");
	define("_USERS_NOT_IN_GROUP", "グループにいないユーザー（クリックで追加）");
	define("_USER", "ユーザー");
	define("_EMPTY", "空です");
	define("_REMOVE", "削除");
	define("_COMPLETE", "完了");
	define("_OPEN", "実行中");
	define("_OVERDUE", "遅延");
	define("_TODAY", "今日");
	define("_DAILY", "毎日");
	define("_OPEN_ENDED", "期限のない");
	define("_WEEKLY", "毎週");
	define("_MONTHLY", "毎月");
	define("_YEARLY", "毎月");
    define("_DATE", "日付");
    define("_DAY", "日");
    define("_DAYS", "日");
	define("_NO_RECURRENCE", "一度きり");
    define("_RECURRENCE", "繰り返し");
	define("_RECURRE_EVERY", "しべて繰り返し");
	define("_RECURRE_ON", "繰り返し On");
	define("_EVERY_MONTH", "すべての月");
	define("_STATUS", "ステータス");
	define("_TITLE", "タイトル");
	define("_DEADLINE", "期限");
	define("_TASK", "タスク");
	define("_ASSIGNED_BY", "割り当てた人：");
	define("_ASSIGN_TO", "割り当てられた人：");
	define("_COMPLETED_BY", "完了した人：");
	define("_COMPLETE_TASK_BY", "タスクの期限");
	define("_COMMENT", "コメント");
	define("_COMMENT_BY", "コメント者：");
	define("_COMMENT_DATE", "コメントした日付");
	define("_UPDATE_TASK", "タスクを更新");
	define("_CURRENT_PASS", "現在のパスワード");
	define("_ADD", "追加");
	define("_FILTER", "フィルター");
	define("_VIEW_TASKS", "タスクを閲覧");
	define("_EDIT", "編集");
	define("_DELETE", "削除");
    define("_ENABLE_AJAX", "Ａｊａｘを有効にしますか？");
	define("_ALLOW_OTHERS", "他人がタスクを割り当てるのを許可しますか？");
	define("_PUBLIC_LIST", "他人がＴｏｄｏ一覧を見るのを許可しますか？");
	define("_UPDATE_INFO", "情報を更新");
	define("_PREV", "前");
	define("_NEXT", "次");
	define("_YOUR_CURRENT_PASS", "現在のパスワードを入力してください");
	define("_YOUR_NEW_PASS", "新しいパスワードを入力してください");
	define("_YOUR_VERIFY_PASS", "新しいパスワードを確認してください");
	define("_NEW_PASS_ERROR", "新しいパスワードが確認用のものと一致しません");
	define("_CURRENT_PASS_ERROR", "現在のパスワードが一致しません");
    define("_IS_UNDEFINED", "は定義されていません");
    define("_AS_OF", "期日");
    define("_PRINT", "印刷");
    define("_EXPORT", "エクスポート");
    define("_SET_VIEW", "デフォルトの表示に設定");
    define("_DESCRIPTION", "説明");
    define("_DISABLE", "無効");
    define("_ENABLE", "有効");
    define("_INSTALL", "インストール");
    define("_UNINSTALL", "アンインストール");
    define("_MODULE", "モジュール");
    define("_CONFIGURE", "設定");
    define("_CANCEL", "キャンセル");
    define("_WHAT_WOULD_YOU_LIKE", "What would you like to do?");
    define("_RESET_ALL", "カテゴリーなしのタスクをすべてリセットしますか？");
    define("_REMOVE_ALL", "カテゴリーのすべてのタスクを削除しますか？");

	/* These are used when adding a new user or group. */
	define("_USER_NAME", "ユーザー名");
	define("_NEW_PASS", "新しいパスワード");
	define("_VERIFY", "確認");
	define("_FIRST_NAME", "名");
	define("_LAST_NAME", "姓");
	define("_THEME", "テーマ");
	define("_LANGUAGE", "言語");
	define("_DATE_FORMAT", "日付の形式");
	define("_EMAIL", "Email");
	define("_AIM", "AIM のスクリーン名");
	define("_IS_ADMIN", "管理者ですか？");
	define("_GROUP_NAME", "グループ名");
	define("_GROUP_MEMBERS", "メンバー");
	define("_UPDATE", "更新");
	define("_SELECT_GROUP", "グループを選択");
	define("_FINISH_CREATE_USER", "ユーザーの情報とパスワードを設定");
	define("_AUTO_DAILY", "新しいタスクをデフォルトで毎日に設定しますか？");

	/* These are for login stuff. */
	define("_LOGIN", "ログイン");
	define("_LOGON_SUCCESSFUL", "ログインしました");
	define("_PASSWORD", "パスワード");
	define("_LOGGED_OUT", "ログアウトしました");
	define("_KEEP_LOGIN", "このコンピューターでログインするときに、状態を保持する");
	define("_WRONG_PASSWORD", "ユーザー名もしくはパスワードが間違っています");
	
	/* Supercats */
	define("_SUPERCAT", "Supercat");
	define("_LIST_SUPERCATS", "Supercatの一覧");
	define("_CATS_IN_SUPERCAT", "Categories in Supercat");
	define("_REMOVED_FROM_SUPERCAT", "Category removed from Supercat successfully!");
	define("_CATS_NOT_IN_SUPERCAT", "Categories not in Supercat");
	define("_GROUP_SUPERCAT", "Supercat created successfully!");
	define("_ALREADY_SUPERCAT", "There is already a Supercat with that name!");

    /* Email Notifications */
    define("_SUBJECT", "タスクの連絡を問い合わせる");
    define("_GREETING", "タスクを割り当てられました");
    define("_CLOSING", "この連絡を割り当てた人:");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => '設定なし', 1 => '低', 2 => '中', 3 => '高', 4 => '遅延', 5 => 'ホールド');
?>
