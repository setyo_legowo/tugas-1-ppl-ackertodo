<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: catala.inc,v 1.2 2005/07/19 18:30:45 zoidian Exp $
*
***************************************
* TRANSLATED TO CATAL� BY eb_scrooge ( eb.scrooge@gmail.com )
*	catala.inc
***************************************
*/
?>
<?php
	/* This is the Catala language file where you define all the strings
	* used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Mostra les tasques");
	define("_SHOW_DAILY", "Mostra les tasques del dia");
	define("_SHOW_OPEN", "Mostra les tasques incompletes");
	define("_SHOW_COMPLETED", "Mostra les tasques completades");
	define("_SHOW_ALL", "Mostra totes les tasques");
	define("_THIS_WEEK", "Tasques per aquesta setmana");
	define("_TASKS_DAILY", "Tasques diaries");
	define("_TASKS_OPEN", "Tasques incompletes");
	define("_TASKS_COMPLETED", "Tasques completades");
	define("_TASKS_OVERDUE", "Tasques excedides");
	define("_TASKS_ALL", "Totes les Tasques");
	define("_TASKS_THIS_WEEK", "Tasques per aquesta setmana");
	define("_TASKS_DATE", "el ");
	define("_NEW_TASK", "Nova tasca");
	define("_CALENDAR", "Calendari");
	define("_ADMIN_MENU", "Men� Mantenidor");
	define("_USER_MENU", "Men� Usuari");
	define("_LOG_OUT", "Desconnectar");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "Sols el mantenidor pot realitzar aquesta acci�!");
	define("_ADD_USER_GROUP", "Afegir Usuari/Grup");
	define("_REMOVE_USER_GROUP", "Eliminar Usuari/Grup");
	define("_EDIT_GROUPS", "Editar grups");
	define("_LIST_USERS", "Llistar usuaris");
	define("_LIST_GROUPS", "Llistar grups");
	define("_CHANGE_PASS", "Canviar contrasenya");
	define("_EDIT_INFO", "Modificar informaci� de l' usuari");
	define("_LIST_CATEGORIES", "Llistar categories");
	define("_REMOVE_CATEGORY", "Eliminar categoria");
	define("_RENAME_CATEGORY", "Renombrar categoria");
	define("_CATEGORY_NAME", "Nom de la categoria");
	define("_VIEW_GROUPS", "Veure grups");
	define("_FILTER", "Filtre");
	define("_ADMIN_COMMAND", "Comandament de Mantenidor");
	define("_COMMAND_ERROR", "no est� definit o no tens perm�s!");
    define("_MODULES", "M�duls");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "Editar tasca");
	define("_DELETE_TASK", "Eliminar tasca");
	define("_VIEW_TASK", "Veure tasca");
	define("_ADD_COMMENT", "Afegir comentari");
	define("_ACCESS_DENIED", "Acc�s denegat");

	/* These are for the Search Menu. */
	define("_SEARCH_FOR", "Buscar per ");
	define("_SEARCH_IN", " a ");
	define("_SEARCH_ALL", "Totes les tasques");
	define("_SEARCH_OPEN", "Tasques incompletes");
	define("_SEARCH_CLOSED", "Tasques completades");
	define("_SEARCH_DAILY", "Tasques diaries");
	define("_SEARCH_EVERYONES", "Tasques de tots");
	define("_SEARCH_MINE", "Totes les meues tasques");
	define("_SEARCH_USERS", "'s tasques");
	define("_SEARCH", "Buscar");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "Tasca afegida satisfactoriament!");
	define("_TASK_UPDATED", "Tasca actualitzada satisfactoriament!");
	define("_TASK_DELETED", "Tasca eliminada satisfactoriament!");
	define("_TASK_OPENED", "Tasca oberta satisfactoriament!");
	define("_TASK_CLOSED", "Tasca tancada satisfactoriament!");
	define("_USER_ADDED", "Usuari afegit satisfactoriament!");
	define("_USER_REMOVED", "Usuari eliminat satisfactoriament!");
	define("_USER_EXISTS", "L' usuari ja existeix a aquest grup!");
	define("_USER_TO_GROUP", "Usuari afegit a aquest grup satisfactoriament!");
	define("_ENTER_USER", "Cal ingressar un nom d' usuari!");
	define("_ENTER_PASS", "Cal ingressar una contrasenya!");
	define("_ENTER_COMMENT", "Cal afegir un comentari!");
	define("_ENTER_TITLE", "Cal afegir un t�tol!");
	define("_PASS_CHANGED", "Contrasenya canviada satisfactoriament!");
	define("_ENTER_GROUP", "Cal ingressar un nom de grup!");
	define("_GROUP_ADDED", "Grup afegit satisfactoriament!");
	define("_GROUP_REMOVED", "Grup eliminat satisfactoriament!");
	define("_GROUP_UPDATED", "Grup actualitzat satisfactoriament!");
	define("_REMOVE_YOURSELF", "No pots eliminar-te a tu mateix!");
	define("_REMOVED_FROM_GROUP", "Usuari eliminat del grup satisfactoriament!");
	define("_ADDED_TO_GROUP", "Usuari afegit al grup satisfactoriament!");
	define("_COMMENT_ADDED", "Comentari afegit satisfactoriament!");
	define("_VERIFY_PASS", "Cal verificar les contrasenyes dels usuaris!");
	define("_PASS_ERROR", "Les contrasenyes no coincideixen!");
	define("_SELECT_USER", "Cal seleccionar un usuari per afegir al grup!");
	define("_THEME_CHANGED", "Tema canviat satisfactoriament!");
	define("_LANGUAGE_CHANGED", "Idioma canviat satisfactoriament!");
	define("_INFO_UPDATED", "Informaci� actualitzada satisfactoriament!");
	define("_QUERY_ERROR", "La petici� ha fallat!");
	define("_COMMENT_REMOVED", "Comentari eliminat satisfactoriament!");
	define("_CATEGORY_CREATED", "Categoria afegida satisfactoriament!");
	define("_CATEGORY_UPDATED", "Categoria actualitzada satisfactoriament!");
	define("_CATEGORY_DELETED", "Categoria eliminada satisfactoriament!");
	define("_ADDED_TO_SUPERCAT", "Categoria afegida a Supercat satisfactoriament!");
	define("_SUPERCAT_REMOVED", "Supercat eliminada satisfactoriament!");
	define("_ENTER_CATEGORY", "Cal ingressar un nom per la categoria!");
	define("_NO_ID", "No s' ha proporcionat Id!");
	define("_ALREADY_USER", "Ja existeix un usuari amb eixe nom!");
	define("_ALREADY_GROUP", "Ja existeix un grup amb eixe nom!");
    define("_MODULE_ENABLE", "M�dul activat satisfactoriament!");
    define("_MODULE_DISABLE", "M�dul desactivat satisfactoriament!");
    define("_MODULE_INSTALL", "M�dul instalat satisfactoriament!");
    define("_MODULE_UNINSTALL", "M�dul desinstalat satisfactoriament!");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "Gener");
	define("_FEBUARY", "Febrer");
	define("_MARCH", "Mar�");
	define("_APRIL", "Abril");
	define("_MAY", "Maig");
	define("_JUNE", "Juny");
	define("_JULY", "Juliol");
	define("_AUGUST", "Agost");
	define("_SEPTEMBER", "Setembre");
	define("_OCTOBER", "Octubre");
	define("_NOVEMBER", "Novembre");
	define("_DECEMBER", "Desembre");
	define("_SUNDAY", "Diumenge");
	define("_MONDAY", "Dilluns");
	define("_TUESDAY", "Dimarts");
	define("_WEDNESDAY", "Dimecres");
	define("_THURSDAY", "Dijous");
	define("_FRIDAY", "Divendres");
	define("_SATURDAY", "Dissabte");
	define("_SUN", "Dg");
	define("_MON", "Dl");
	define("_TUE", "Dm");
	define("_WED", "Dc");
	define("_THU", "Dj");
	define("_FRI", "Dv");
	define("_SAT", "Ds");

	/* Other various strings. */
    define("_DATE", "Data");
	define("_REDIRECT", "Redre�ar cap a ");
	define("_CATEGORY", "Categoria");
	define("_NO_CATEGORY", "Sense categoria");
    define("_ALL_CATEGORIES", "Totes les categories");
	define("_PRIORITY", "Prioritat");
	define("_CREATION", "Creaci�");
	define("_AUTHORIZATION", "Requereix autoritzaci�!");
	define("_LOGGING_OUT", "Desconectant...");
	define("_GROUP", "Grup");
	define("_ACTIONS", "Accions");
	define("_USERS_IN_GROUP", "Usuaris al grup (click per eliminar)");
	define("_USERS_NOT_IN_GROUP", "Usuaris f�ra del grup (click per afegir)");
	define("_USERS", "Usuaris");
	define("_USER", "Usuari");
	define("_EMPTY", "Buit!");
	define("_REMOVE", "Eliminar");
	define("_COMPLETED", "Completada?");
	define("_COMPLETE", "Completada");
	define("_OPEN", "Oberta");
	define("_OVERDUE", "Excedida");
	define("_TODAY", "Hui");
	define("_DAILY", "Diaria");
	define("_COMPLETED_FOR_DAY", "Completada pel dia");
	define("_REOPEN_TASK", "Reobrir tasca?");
	define("_STATUS", "Estat");
	define("_TITLE", "T�tol");
	define("_DEADLINE", "Termini");
	define("_TASK", "Tasca");
	define("_ASSIGNED_BY", "Assignada per");
	define("_ASSIGN_TO", "Assignar a");
	define("_COMPLETED_BY", "Completada per");
	define("_COMPLETE_TASK_BY", "Tasca pel");
	define("_COMMENT", "Comentari");
	define("_COMMENT_BY", "Comentari de");
	define("_COMMENT_DATE", "Data");
	define("_REMOVE_COMMENT", "Eliminar comentari");
	define("_SUBMIT_TASK", "Afegir tasca");
	define("_DAILY_TASK", "Tasca diaria");
	define("_UPDATE_TASK", "Actualitzar tasca");
	define("_CURRENT_PASS", "Contrasenya actual");
	define("_ADD", "Afegir");
	define("_VIEW_TASKS", "Veure tasques");
	define("_EDIT", "Editar");
	define("_DELETE", "Eliminar");
	define("_ALLOW_OTHERS", "Permetre a altres assignar-me tasques?");
	define("_PUBLIC_LIST", "Permetre a altres veure la meua llista ToDo?");
	define("_ENABLE_EMAIL", "Activar notificaci� per email?");
	define("_ENABLE_AIM", "Activar notificaci� AIM?");
	define("_UPDATE_INFO", "Actualitzar informaci�");
	define("_PREV", "Ant");
	define("_NEXT", "Seg");
	define("_YOUR_CURRENT_PASS", "Cal que ingresses la teua contrasenya actual!");
	define("_YOUR_NEW_PASS", "Cal ingressar una nova contrasenya!");
	define("_YOUR_VERIFY_PASS", "Cal verificar la teua nova contrasenya!");
	define("_NEW_PASS_ERROR", "La nova contrasenya no coincideix amb la verificaci�!");
	define("_CURRENT_PASS_ERROR", "La teua contrasenya actual no coincideix!");
	define("_CHANGE", "Canvia");
	define("_QUERY", "Petici�");
	define("_OUTPUT", "Eixida");
	define("_RETURNED", "Retornat");
    define("_COMMAND", "Comandament");
    define("_IS_UNDEFINED", "no est� definit!");
    define("_PRINT", "Imprimir");
    define("_DESCRIPTION", "Descripci�");
    define("_DISABLE", "Desactivar");
    define("_ENABLE", "Activar");
    define("_INSTALL", "Instalar");
    define("_UNINSTALL", "Desinstalar");
    define("_MODULE", "M�dul");
    define("_CONFIGURE", "Configurar");

	/* These are used when adding a new user or group. */
	define("_ADD_USER", "Afegir usuari");
	define("_ADD_GROUP", "Afegir grup");
	define("_USER_NAME", "Nom d' usuari");
	define("_NEW_PASS", "Nova contrasenya");
	define("_VERIFY", "Verifica");
	define("_FIRST_NAME", "Nom");
	define("_LAST_NAME", "Cognom");
	define("_THEME", "Tema");
	define("_LANGUAGE", "Idioma");
	define("_DATE_FORMAT", "Format de data");
	define("_EMAIL", "Email");
	define("_AIM", "AIM Nick");
	define("_IS_ADMIN", "Mantenidor?");
	define("_GROUP_NAME", "Nom del grup");
	define("_GROUP_MEMBERS", "Membres");
	define("_UPDATE", "Actualitzar");
	define("_SELECT_GROUP", "Selecciona un grup");
	define("_FINISH_CREATE_USER", "Afegida informaci�/contrasenya de l' usuari");
	define("_AUTO_DAILY", "Noves tasques diaries per defecte?");

	/* These are for login stuff. */
	define("_LOGIN", "Login");
	define("_LOGON_SUCCESSFUL", "Login correcte");
	define("_PASSWORD", "Contrasenya");
	define("_LOGGED_OUT", "Has estat desconectat");
	define("_KEEP_LOGIN", "Recorda'm a aquest ordinador");
	define("_WRONG_PASSWORD", "Nom d' usuari o contrasenya incorrectes");
	
	/* Supercats */
	define("_SUPERCAT", "Supercat");
	define("_LIST_SUPERCATS", "Llistar Supercats");
	define("_CATS_IN_SUPERCAT", "Categories a Supercat");
	define("_REMOVED_FROM_SUPERCAT", "Categoria eliminada de Supercat satisfactoriament!");
	define("_CATS_NOT_IN_SUPERCAT", "Categories f�ra de Supercat");
	define("_GROUP_SUPERCAT", "Supercat creada satisfactoriament!");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Indefinida', 1 => 'Baixa', 2 => 'Mitjana', 3 => 'Alta', 4 => 'Delayed', 5 => 'On Hold');
?>
