<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: portuguese.inc,v 1.3 2005/07/19 18:30:46 zoidian Exp $
*/
?>
<?php
	/* This is the Portuguese language file where you define all the strings
	* used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Mostrar Tarefas");
	define("_SHOW_DAILY", "Mostrar Tarefas Di�rias");
	define("_SHOW_OPEN", "Mostrar Tarefas em Aberto");
	define("_SHOW_COMPLETED", "Mostrar Tarefas Completadas");
	define("_SHOW_ALL", "Mostrar Todas as Tarefas");
	define("_THIS_WEEK", "Tarefas Dessa Semana");
	define("_TASKS_DAILY", "Tarefas Di�rias");
	define("_TASKS_OPEN", "Tarefas em Aberto");
	define("_TASKS_COMPLETED", "Tarefas Completadas");
	define("_TASKS_OVERDUE", "Tarefas Atrasadas");
	define("_TASKS_ALL", "Todas as Tarefas");
	define("_TASKS_THIS_WEEK", "Tarefas Dessa Semana");
	define("_TASKS_DATE", "Em ");
	define("_NEW_TASK", "Nova Tarefa");
	define("_CALENDAR", "Calend�rio");
	define("_ADMIN_MENU", "Menu do Administrador");
	define("_USER_MENU", "Menu do Usu�rio");
	define("_LOG_OUT", "Log Out");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "Somente o admnistrador pode fazer isso!");
	define("_ADD_USER_GROUP", "Adicionar Usu�rio ou Grupo");
	define("_REMOVE_USER_GROUP", "Remover Usu�rio ou Grupo");
	define("_EDIT_GROUPS", "Editar Grupos");
	define("_LIST_USERS", "Listar Usu�rios");
	define("_LIST_GROUPS", "Listar Grupos");
	define("_CHANGE_PASS", "Mudar Senha");
	define("_EDIT_INFO", "Editar Informa��es do Usu�rio");
	define("_LIST_CATEGORIES", "Listar Categorias");
	define("_REMOVE_CATEGORY", "Remover categoria");
	define("_RENAME_CATEGORY", "Renomear categoria");
	define("_CATEGORY_NAME", "Nome da categoria");
	define("_VIEW_GROUPS", "Ver grupos");
	define("_FILTER", "Filtro");
	define("_ADMIN_COMMAND", "Comando do Administrador");
	define("_COMMAND_ERROR", "� indefinido ou voc� n�o tem permiss�o!");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "Editar Tarefa");
	define("_DELETE_TASK", "Deletar Tarefa");
	define("_VIEW_TASK", "Ver Tarefa");
	define("_ADD_COMMENT", "Adiconar Coment�rio");
	define("_ACCESS_DENIED", "Acesso Negado");

	/* These are for the Search Menu. */
	define("_SEARCH_FOR", "Pesquisar por ");
	define("_SEARCH_IN", " rm ");
	define("_SEARCH_ALL", "Todas as Tarefas");
	define("_SEARCH_OPEN", "Tarefas em Aberto");
	define("_SEARCH_CLOSED", "Tarefas Completas");
	define("_SEARCH_DAILY", "Tarefas Di�rias");
	define("_SEARCH_EVERYONES", "Tarefa de todos");
	define("_SEARCH_MINE", "Todas as minhas Tarefas");
	define("_SEARCH_USERS", "'s Tarefas");
	define("_SEARCH", "Pesquisa");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "Tarefa adicionada com sucesso!");
	define("_TASK_UPDATED", "Tarefa atualizada com sucesso!");
	define("_TASK_DELETED", "Tarefa deletada com sucesso!");
	define("_TASK_OPENED", "Tarefa aberta com sucesso!");
	define("_TASK_CLOSED", "Tarefa fechada com sucesso!");
	define("_USER_ADDED", "Usu�rio adicionado com sucesso!");
	define("_USER_REMOVED", "Usu�rio removido com sucesso!");
	define("_USER_EXISTS", "O Usu�rio j� est� nesse grupo!");
	define("_USER_TO_GROUP", "Usu�rio adicionado ao grupo com sucesso!");
	define("_ENTER_USER", "Voc� deve digitar um nome de usu�rio!");
	define("_ENTER_PASS", "Voc� deve digitar uma senha!");
	define("_ENTER_COMMENT", "Voc� deve digitar um coment�rio!");
	define("_ENTER_TITLE", "Voc� deve digitar um t�tulo!");
	define("_PASS_CHANGED", "Senha alterada com sucesso!");
	define("_ENTER_GROUP", "Voc� deve digitar um nome para o grupo!");
	define("_GROUP_ADDED", "Grupo adicionado com sucesso!");
	define("_GROUP_REMOVED", "Grupo removido com sucesso!");
	define("_GROUP_UPDATED", "Grupo atualizado com sucesso!");
	define("_REMOVE_YOURSELF", "Voc� n�o pode se remover!");
	define("_REMOVED_FROM_GROUP", "Usu�rio removido do grupo com sucesso!");
	define("_ADDED_TO_GROUP", "Usu�rio adicionado ao grupo com sucesso!");
	define("_COMMENT_ADDED", "Coment�rio adicionado com sucesso!");
	define("_VERIFY_PASS", "Voc� deve verificar a senha do usu�rio!");
	define("_PASS_ERROR", "As senhas n�o conferem!");
	define("_SELECT_USER", "Voc� deve selecionar um usu�rio para adicionar ao grupo!");
	define("_THEME_CHANGED", "Tema alterado com sucesso!");
	define("_LANGUAGE_CHANGED", "Idioma alterado com sucesso!");
	define("_INFO_UPDATED", "Informa��o atualizada com sucesso!");
	define("_QUERY_ERROR", "Ocorreu um erro ao processar o pedido!");
	define("_COMMENT_REMOVED", "Coment�rio removido com sucesso!");
	define("_CATEGORY_CREATED", "Categoria adicionada com sucesso!");
	define("_CATEGORY_UPDATED", "Categoria atualizada com sucesso!");
	define("_CATEGORY_DELETED", "Categoria deletada com sucesso!");
	define("_ADDED_TO_SUPERCAT", "Categoria adicionada � SuperCategoria com sucesso!");
	define("_SUPERCAT_REMOVED", "SuperCategoria removida com sucesso!");
	define("_ENTER_CATEGORY", "Voc� deve dar um nome � categoria!");
	define("_NO_ID", "Digite um ID!");
	define("_ALREADY_USER", "J� existe um usu�rio com esse nome!");
	define("_ALREADY_GROUP", "J� h� um grupo com esse nome!");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "Janeiro");
	define("_FEBUARY", "Fevereiro");
	define("_MARCH", "Mar�o");
	define("_APRIL", "Abril");
	define("_MAY", "Maio");
	define("_JUNE", "Junho");
	define("_JULY", "Julho");
	define("_AUGUST", "Agosto");
	define("_SEPTEMBER", "Setembro");
	define("_OCTOBER", "Outubro");
	define("_NOVEMBER", "Novembro");
	define("_DECEMBER", "Dezembro");
	define("_SUNDAY", "Domingo");
	define("_MONDAY", "Segunda-Feira");
	define("_TUESDAY", "Ter�a-Feira");
	define("_WEDNESDAY", "Quarta-Feira");
	define("_THURSDAY", "Quinta-Feira");
	define("_FRIDAY", "Sexta-Feira");
	define("_SATURDAY", "S�bado");
	define("_SUN", "Dom");
	define("_MON", "Seg");
	define("_TUE", "Ter");
	define("_WED", "Qua");
	define("_THU", "Qui");
	define("_FRI", "Sex");
	define("_SAT", "Sab");

	/* Other various strings. */
    define("_DATE", "Data");
	define("_REDIRECT", "Redirecionar posts para ");
	define("_CATEGORY", "Categoria");
	define("_NO_CATEGORY", "Nenhuma Categoria");
    define("_ALL_CATEGORIES", "Todas as Categorias");
	define("_PRIORITY", "Prioridade");
	define("_CREATION", "Criada por");
	define("_AUTHORIZATION", "Autoriza��o Necess�ria!");
	define("_LOGGING_OUT", "Desligando-se...");
	define("_GROUP", "Grupo");
	define("_ACTIONS", "A��es");
	define("_USERS_IN_GROUP", "Usu�rios no grupo(clique para remover)");
	define("_USERS_NOT_IN_GROUP", "Usu�rio fora do grupo(clique para adicionar)");
	define("_USERS", "Usu�rios");
	define("_USER", "Usu�rio");
	define("_EMPTY", "Vazio!");
	define("_REMOVE", "Remover");
	define("_COMPLETED", "Completa?");
	define("_COMPLETE", "Completa");
	define("_OPEN", "Aberta");
	define("_OVERDUE", "Atrasada");
	define("_DAILY", "Di�ria");
	define("_COMPLETED_FOR_DAY", "Completa por hoje");
	define("_REOPEN_TASK", "Reabrir Tarefa?");
	define("_STATUS", "Status");
	define("_TITLE", "Tit�lo");
	define("_DEADLINE", "Prazo");
	define("_TASK", "Tarefa");
	define("_ASSIGNED_BY", "Atribu�da por");
	define("_ASSIGN_TO", "Atribu�da para");
	define("_COMPLETED_BY", "Completada por");
	define("_COMPLETE_TASK_BY", "Tarefa atrasada");
	define("_COMMENT", "Coment�rio");
	define("_COMMENT_BY", "Coment�rio de");
	define("_COMMENT_DATE", "Data do coment�rio");
	define("_REMOVE_COMMENT", "Remover coment�rio");
	define("_SUBMIT_TASK", "Enviar Tarefa");
	define("_DAILY_TASK", "Tarefa Di�ria");
	define("_UPDATE_TASK", "Atualizar Tarefa");
	define("_CURRENT_PASS", "Senha Atual");
	define("_ADD", "Adicionar");
	define("_VIEW_TASKS", "Ver tarefas");
	define("_EDIT", "Editat");
	define("_DELETE", "Deletat");
	define("_ALLOW_OTHERS", "Permitir que outros atribuam tarefas para mim?");
	define("_PUBLIC_LIST", "Permitir que outros vejam minha lista de coisas a fazer?");
	define("_ENABLE_EMAIL", "Notificar-me por email?");
	define("_UPDATE_INFO", "Atualizar inform��o");
	define("_PREV", "Anterior");
	define("_NEXT", "Pr�ximo");
	define("_YOUR_CURRENT_PASS", "Voc� deve digitar sua senha atual!");
	define("_YOUR_NEW_PASS", "Voc� deve digitar uma nova senha!");
	define("_YOUR_VERIFY_PASS", "Voc� deve repetir sua nova senha!");
	define("_NEW_PASS_ERROR", "Sua verifica��o n�o confere com sua senha!");
	define("_CURRENT_PASS_ERROR", "Sua senha atual est� errada!");
	define("_CHANGE", "Alterar");
	define("_QUERY", "Pedido");
	define("_OUTPUT", "Sa�da");
	define("_RETURNED", "Retornada");
    define("_COMMAND", "Commando");
    define("_IS_UNDEFINED", "� indefinida!");
    define("_PRINT", "Imprimir");

	/* These are used when adding a new user or group. */
	define("_ADD_USER", "Adicionar Usu�rio");
	define("_ADD_GROUP", "Adicionar Grupo");
	define("_USER_NAME", "Nome de Usu�rio");
	define("_NEW_PASS", "Nova senha");
	define("_VERIFY", "Verificar");
	define("_FIRST_NAME", "Nome");
	define("_LAST_NAME", "Sobrenome");
	define("_THEME", "Tema");
	define("_LANGUAGE", "Idioma");
	define("_EMAIL", "Email");
	define("_AIM", "AIM Screenname");
	define("_IS_ADMIN", "Administrador?");
	define("_GROUP_NAME", "Nome do Grupo");
	define("_GROUP_MEMBERS", "Membros");
	define("_UPDATE", "Atualizar");
	define("_SELECT_GROUP", "Selecione um grupo");
	define("_FINISH_CREATE_USER", "Alterar informa��o/senha do usu�rio");
	define("_AUTO_DAILY", "Deseja que novas tarefas sejam di�rias por padr�o?");

	/* These are for login stuff. */
	define("_LOGIN", "Login");
	define("_LOGON_SUCCESSFUL", "Login com sucesso");
	define("_PASSWORD", "Senha");
	define("_LOGGED_OUT", "Voc� desligou-se");
	define("_KEEP_LOGIN", "Guardar meu login nesse Computador");
	define("_WRONG_PASSWORD", "Senha ou nome de usu�rio incorretos");
	
	/* Supercats */
	define("_SUPERCAT", "SuperCategoria");
	define("_LIST_SUPERCATS", "List SuperCategoria");
	define("_CATS_IN_SUPERCAT", "Categorias na SuperCategoria");
	define("_REMOVED_FROM_SUPERCAT", "Categoria removida da SuperCategoria com sucesso!");
	define("_CATS_NOT_IN_SUPERCAT", "Categorias fora da SuperCategoria");
	define("_GROUP_SUPERCAT", "SuperCategoria criada com sucesso!");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Indefinida', 1 => 'Baixa', 2 => 'M�dia', 3 => 'Alta', 4 => 'Delayed', 5 => 'On Hold');
?>
