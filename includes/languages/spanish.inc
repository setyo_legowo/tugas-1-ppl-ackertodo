<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: spanish.inc,v 1.11 2005/07/19 18:30:46 zoidian Exp $
*/
?>
<?php
    /* This is the Spanish language file where you define all the strings
     * used. */

    /* These are for the Main Menu. */
    define("_SHOW_TASKS", "Mostrar Tareas");
    define("_SHOW_DAILY", "Mostrar Tareas Diarias");
    define("_SHOW_OPEN", "Mostrar Tareas Abiertas");
    define("_SHOW_COMPLETED", "Mostrar Tareas Completas");
    define("_SHOW_ALL", "Mostrar Todas las Tareas");
    define("_THIS_WEEK", "Tareas de la Semana");
    define("_TASKS_DAILY", "Tareas Diarias");
    define("_TASKS_OPEN", "Tareas Abiertas");
    define("_TASKS_COMPLETED", "Tareas Completas");
    define("_TASKS_OVERDUE", "Tareas Vencidas");
    define("_TASKS_ALL", "Todas las Tareas");
    define("_TASKS_THIS_WEEK", "Tareas de la Semana");
    define("_TASKS_DATE", "Activa ");
    define("_NEW_TASK", "Nueva Tarea");
    define("_CALENDAR", "Calendario");
    define("_ADMIN_MENU", "Administrador");
    define("_USER_MENU", "Usuarios");
    define("_LOG_OUT", "Salir");

    /* These are for the Admin Menu. */
    define("_ADMIN_ERROR", "Solo el administrador puede hacer esto!");
    define("_ADD_USER_GROUP", "A�adir Usuario/Grupo");
    define("_REMOVE_USER_GROUP", "Quitar Usuario/Grupo");
    define("_EDIT_GROUPS", "Editar Grupos");
    define("_LIST_USERS", "Listar Usuarios");
    define("_LIST_GROUPS", "Listar Grupos");
    define("_CHANGE_PASS", "Cambiar Contrase�a");
    define("_EDIT_INFO", "Cambiar Informaci�n del Usuario");
    define("_LIST_CATEGORIES", "Listar Categor�as");
    define("_REMOVE_CATEGORY", "Remover Categor�as");
    define("_RENAME_CATEGORY", "Renombrar Categor�a");
    define("_CATEGORY_NAME", "Nombre de la Categor�a");
    define("_VIEW_GROUPS", "Ver Grupos");
    define("_FILTER", "Filtrar");
    define("_ADMIN_COMMAND", "Comando de Administrador");
    define("_COMMAND_ERROR", "no esta definido o no tiene autorizaci�n!");
    define("_MODULES", "Modulos");

    /* These are used in the buttons for each task. */
    define("_EDIT_TASK", "Editar Tarea");
    define("_DELETE_TASK", "Borrar Tarea");
    define("_VIEW_TASK", "Ver Tarea");
    define("_ADD_COMMENT", "A�adir Comentario");
    define("_ACCESS_DENIED", "Acceso Denegado");

    /* These are for the Search Menu. */
    define("_SEARCH_FOR", "Buscar ");
    define("_SEARCH_IN", " en ");
    define("_SEARCH_ALL", "Todas las Tareas");
    define("_SEARCH_OPEN", "Tareas Abiertas");
    define("_SEARCH_CLOSED", "Tareas Cerradas");
    define("_SEARCH_DAILY", "Tareas Diarias");
    define("_SEARCH_EVERYONES", "Tarea de Todos");
    define("_SEARCH_MINE", "Todas mis Tareas");
    define("_SEARCH_USERS", " (tareas)");
    define("_SEARCH", "Buscar");

    /* Success/Failure strings. */
    define("_TASK_ADDED", "Tarea agregada con �xito!");
    define("_TASK_UPDATED", "Tarea actualizada con �xito!");
    define("_TASK_DELETED", "Tarea borrada con �xito!");
    define("_TASK_OPENED", "Tarea abierta con �xito!");
    define("_TASK_CLOSED", "Tarea cerrada con �xito!");
    define("_USER_ADDED", "Usuario agregado con �xito!");
    define("_USER_REMOVED", "Usuario borrado con �xito!");
    define("_USER_EXISTS", "El usuario ya existe en ese grupo!");
    define("_USER_TO_GROUP", "Usuario agregado al grupo con �xito!");
    define("_ENTER_USER", "Debe introducir un nombre de usuario!");
    define("_ENTER_PASS", "Debe introducir una contrase�a!");
    define("_ENTER_COMMENT", "Debe poner un comentario!");
    define("_ENTER_TITLE", "Tiene que poner un titulo!");
    define("_PASS_CHANGED", "Contrase�a actualizada con �xito!");
    define("_ENTER_GROUP", "Debe de poner un nombre de grupo!");
    define("_GROUP_ADDED", "Se agrego el grupo con �xito!");
    define("_GROUP_REMOVED", "Se quito el grupo con �xito!");
    define("_GROUP_UPDATED", "Grupo actualizado con �xito!");
    define("_REMOVE_YOURSELF", "No te puedes borrar a ti mismo!");
    define("_REMOVED_FROM_GROUP", "Usuario removido del grupo con �xito!");
    define("_ADDED_TO_GROUP", "Usuario agregado al grupo con �xito!");
    define("_COMMENT_ADDED", "Comentario agregado con �xito!");
    define("_VERIFY_PASS", "Verifique la contrase�a del usuario!");
    define("_PASS_ERROR", "Las contrase�as no coinciden!");
    define("_SELECT_USER", "Debe seleccionar un usuario a agregar al grupo!");
    define("_THEME_CHANGED", "Se cambio el ambiente con �xito!");
    define("_LANGUAGE_CHANGED", "Se cambio el idioma con �xito!");
    define("_INFO_UPDATED", "Informaci�n actualizada con �xito!");
    define("_QUERY_ERROR", "La busqueda fue exitosa!");
    define("_COMMENT_REMOVED", "Se quito el comentario con �xito!");
    define("_CATEGORY_CREATED", "Se agrego la categor�a con �xito!");
    define("_CATEGORY_UPDATED", "Categor�a actualizada con exito!");
    define("_CATEGORY_DELETED", "Categor�a borrada con �xito!");
    define("_ADDED_TO_SUPERCAT", "Categor�a agregada al SuperCatalogo con exito!");
    define("_SUPERCAT_REMOVED", "SuperCategoria borrada con �xito!");
    define("_ENTER_CATEGORY", "Debe de poner un nombre de SuperCategoria!");
    define("_NO_ID", "No se proporciono identificaci�n!");
    define("_ALREADY_USER", "Ya existe un usuario con ese nombre!");
    define("_ALREADY_GROUP", "Ya existe un grupo con ese nombre!");
    define("_MODULE_ENABLE", "Modulo habilitado con �xito!");
    define("_MODULE_DISABLE", "Modulo deshabilitado con �xito!");
    define("_MODULE_INSTALL", "Modulo instalado con �xito!");
    define("_MODULE_UNINSTALL", "Modulo desinstalado con �xito!");

    /* These are used for the Calendar and formatting dates. */
    define("_JANUARY", "Enero");
    define("_FEBUARY", "Febrero");
    define("_MARCH", "Marzo");
    define("_APRIL", "Abril");
    define("_MAY", "Mayo");
    define("_JUNE", "Junio");
    define("_JULY", "Julio");
    define("_AUGUST", "Agosto");
    define("_SEPTEMBER", "Septiembre");
    define("_OCTOBER", "Octubre");
    define("_NOVEMBER", "Noviembre");
    define("_DECEMBER", "Diciembre");
    define("_SUNDAY", "Domingo");
    define("_MONDAY", "Lunes");
    define("_TUESDAY", "Martes");
    define("_WEDNESDAY", "Mi�rcoles");
    define("_THURSDAY", "Jueves");
    define("_FRIDAY", "Viernes");
    define("_SATURDAY", "S�bado");
    define("_SUN", "Dom");
    define("_MON", "Lun");
    define("_TUE", "Mar");
    define("_WED", "Mie");
    define("_THU", "Jue");
    define("_FRI", "Vie");
    define("_SAT", "Sab");

    /* Other various strings. */
    define("_DATE", "Fecha");
    define("_REDIRECT", "Redireccionar en pegar a ");
    define("_CATEGORY", "Categor�a");
    define("_NO_CATEGORY", "Sin categor�a");
    define("_ALL_CATEGORIES", "Todas las categor�as");
    define("_PRIORITY", "Prioridad");
    define("_CREATION", "Creaci�n");
    define("_AUTHORIZATION", "Se requiere autorizaci�n!");
    define("_LOGGING_OUT", "Saliendo...");
    define("_GROUP", "Grupo");
    define("_ACTIONS", "Acci�n");
    define("_USERS_IN_GROUP", "Usuario en grupo (seleccionar para borrar)");
    define("_USERS_NOT_IN_GROUP", "Usuario no en grupo (seleccionar para a�adir)");
    define("_USERS", "Usuarios");
    define("_USER", "Usuario");
    define("_EMPTY", "Vaci�!");
    define("_REMOVE", "Remover");
    define("_COMPLETED", "�Completo?");
    define("_COMPLETE", "Completo");
    define("_OPEN", "Abrir");
    define("_OVERDUE", "Vencido");
    define("_TODAY", "Hoy");
    define("_DAILY", "Diario");
    define("_COMPLETED_FOR_DAY", "Completo por el d�a");
    define("_REOPEN_TASK", "�Reabrir tarea?");
    define("_STATUS", "Estatus");
    define("_TITLE", "Titulo");
    define("_DEADLINE", "Vencimiento");
    define("_TASK", "Tarea");
    define("_ASSIGNED_BY", "Asignada por");
    define("_ASSIGN_TO", "Asignada a");
    define("_COMPLETED_BY", "Terminada por");
    define("_COMPLETE_TASK_BY", "Tarea para");
    define("_COMMENT", "Comentario");
    define("_COMMENT_BY", "Comentario por");
    define("_COMMENT_DATE", "Fecha del comentario");
    define("_REMOVE_COMMENT", "Quitar comentario");
    define("_SUBMIT_TASK", "Enviar Tarea");
    define("_DAILY_TASK", "Tarea Diaria");
    define("_UPDATE_TASK", "Actualizar Tarea");
    define("_CURRENT_PASS", "Contrase�a Actual");
    define("_ADD", "Agregar");
    define("_VIEW_TASKS", "Ver tareas");
    define("_EDIT", "Editar");
    define("_DELETE", "Borrar");
    define("_ALLOW_OTHERS", "Permitir que otros me agreguen tareas");
    define("_PUBLIC_LIST", "Permitir a otros ver mi lista de pendientes");
    define("_ENABLE_EMAIL", "Habilitar notificaciones por correo electr�nico");
    define("_ENABLE_AIM", "Habilitar notificaciones AIM");
    define("_UPDATE_INFO", "Actualizar informaci�n");
    define("_PREV", "Anterior");
    define("_NEXT", "Siguiente");
    define("_YOUR_CURRENT_PASS", "Introduzca su contrase�a actual!");
    define("_YOUR_NEW_PASS", "Debe introducir una nueva contrase�a!");
    define("_YOUR_VERIFY_PASS", "Tiene que verificar su nueva contrase�a!");
    define("_NEW_PASS_ERROR", "Su nueva contrase�a no coincide con la verificacion!");
    define("_CURRENT_PASS_ERROR", "Su contrase�a actual no coincide!");
    define("_CHANGE", "Cambiar");
    define("_QUERY", "B�squeda");
    define("_OUTPUT", "Salida");
    define("_RETURNED", "Regresado");
    define("_COMMAND", "Comando");
    define("_IS_UNDEFINED", "esta definido!");
    define("_PRINT", "Imprimir");
    define("_DESCRIPTION", "Descripci�n");
    define("_DISABLE", "Deshabilitar");
    define("_ENABLE", "Habilitar");
    define("_INSTALL", "Instalar");
    define("_UNINSTALL", "Desinstalar");
    define("_MODULE", "Modulo");
    define("_CONFIGURE", "Configure");

    /* These are used when adding a new user or group. */
    define("_ADD_USER", "Agregar Usuario");
    define("_ADD_GROUP", "Agregar Grupo");
    define("_USER_NAME", "Nombre de Usuario");
    define("_NEW_PASS", "Nueva Contrase�a");
    define("_VERIFY", "Verificar");
    define("_FIRST_NAME", "Nombre");
    define("_LAST_NAME", "Apellidos");
    define("_THEME", "Tema");
    define("_LANGUAGE", "Idioma");
    define("_DATE_FORMAT", "Formato de fechas");
    define("_EMAIL", "Correo electr�nico");
    define("_AIM", "Nombre de AIM");
    define("_IS_ADMIN", "�Administrador?");
    define("_GROUP_NAME", "Nombre del grupo");
    define("_GROUP_MEMBERS", "Miembros");
    define("_UPDATE", "Actualizar");
    define("_SELECT_GROUP", "Seleccionar un grupo");
    define("_FINISH_CREATE_USER", "Cambiar informaci�n o contrase�a del usuario");
    define("_AUTO_DAILY", "�Tarea nueva diaria por default?");

    /* These are for login stuff. */
    define("_LOGIN", "Usuario");
    define("_LOGON_SUCCESSFUL", "Entrada exitosa");
    define("_PASSWORD", "Contrase�a");
    define("_LOGGED_OUT", "A salido del sistema");
    define("_KEEP_LOGIN", "Conservarme mis datos en esta maquina");
    define("_WRONG_PASSWORD", "Nombre de usuario o contrase�a no valida");
    
    /* Supercats */
    define("_SUPERCAT", "SuperCategoria");
    define("_LIST_SUPERCATS", "Listar SuperCategorias");
    define("_CATS_IN_SUPERCAT", "Categorias en SuperCategorias");
    define("_REMOVED_FROM_SUPERCAT", "Categor�a removida de SuperCategorias con �xito!");
    define("_CATS_NOT_IN_SUPERCAT", "La categor�a no esta en la SuperCategorias");
    define("_GROUP_SUPERCAT", "SuperCategorias creada con �xito!");

    /* Pr3c0gS. Added Translations 2005-07-13*/
    define("_NO_RECURRENCE", "No Recurrente");
    define("_WEEKLY", "Semanalmente");
    define("_MONTHLY", "Mensualmente");
    define("_YEARLY", "Anualmente");
    define("_OPEN_ENDED", "Abierto-Finalizado");
    define("_RECURRE_ON", "Ocurre el");
    define("_EVERY_MONTH", "De Cada Mes");
    define("_RECURRE_EVERY", "Ocurre Los");
    define("_SET_VIEW", "Vista por defecto");
    define("_TASKS_WEEKLY", "Tareas Semanales");
    define("_TASKS_MONTHLY", "Tareas Mensuales");
    define("_TASKS_YEARLY", "Tareas Anuales");
    /* Pr3c0gS. End */
    
    global $_PRIORITY;
    $_PRIORITY = array(0 => 'Sin definir', 1 => 'Baja', 2 => 'Media', 3 => 'Alta', 4 => 'Delayed', 5 => 'On Hold');
?>
