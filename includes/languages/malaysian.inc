<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: malaysian.inc,v 1.3 2005/07/19 18:30:46 zoidian Exp $
*/
?>
<?php
	/* This is the Malaysian language file where you define all the strings
	 * used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Tampilkan Tugas");
	define("_SHOW_DAILY", "Tampilkan Tugas Harian");
	define("_SHOW_OPEN", "Tampilkan Tugas yang Dibuka");
	define("_SHOW_COMPLETED", "Tampilkan Tugas yang sudah Selesai");
	define("_SHOW_ALL", "Tampilkan Semua Tugas");
	define("_THIS_WEEK", "Tugas Minggu Ini");
	define("_TASKS_DAILY", "Tugas Harian");
	define("_TASKS_OPEN_ENDED", "Tugas yang Dibuka-Diselesaikan");
	define("_TASKS_WEEKLY", "Tugas Mingguan");
	define("_TASKS_MONTHLY", "Tugas Bulanan");
	define("_TASKS_YEARLY", "Tugas Tahunan");
	define("_TASKS_OPEN", "Tugas Buka");
	define("_TASKS_COMPLETED", "Tugas Diselesaikan");
	define("_TASKS_OVERDUE", "Tugas yang melewati batas waktu");
	define("_TASKS_ALL", "Semua Tugas");
	define("_TASKS_THIS_WEEK", "Tugas Minggu Ini");
	define("_TASKS_DATE", "Pada");
	define("_NEW_TASK", "Tugas Baru");
	define("_CALENDAR", "Kalendar");
	define("_ADMIN_MENU", "Menu Admin");
	define("_USER_MENU", "Menu Pengguna");
	define("_LOG_OUT", "Logout");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "Hanya admin yang dapat melakukan hal tersebut!");
	define("_ADD_USER_GROUP", "Tambah Pengguna/Grup");
	define("_REMOVE_USER_GROUP", "Hapus Pengguna/Grup");
	define("_EDIT_GROUPS", "Ubah Grup");
	define("_LIST_USERS", "Daftar Pengguna");
	define("_LIST_GROUPS", "Daftar Grup");
	define("_CHANGE_PASS", "Ubah Sandi");
	define("_EDIT_INFO", "Ubah Informasi Pengguna");
	define("_LIST_CATEGORIES", "Daftar Kategori");
	define("_REMOVE_CATEGORY", "Hapus Kategori");
	define("_RENAME_CATEGORY", "Ubah Nama Kategori");
	define("_CATEGORY_NAME", "Nama Kategori");
	define("_VIEW_GROUPS", "Lihat Grup");
	define("_FILTER", "Filter");
	define("_ADMIN_COMMAND", "Perintah Admin");
	define("_COMMAND_ERROR", "tidak didefinisikan atau anda tidak memiliki izin!");
	define("_MODULES", "Modul");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "Ubah Tugas");
	define("_DELETE_TASK", "Hapus Tugas");
	define("_VIEW_TASK", "Lihat Tugas");
	define("_ADD_COMMENT", "Tambah Komentar");
	define("_ACCESS_DENIED", "Akses Ditolak");

	/* These are for the Search Menu. */
	define("_SEARCH_FOR", "Cari untuk");
	define("_SEARCH_IN", " di");
	define("_SEARCH_ALL", "Semua Tugas");
	define("_SEARCH_OPEN", "Tugas Dibuka");
	define("_SEARCH_CLOSED", "Tugas Ditutup");
	define("_SEARCH_EVERYONES", "Tugas Setiap Orang");
	define("_SEARCH_MINE", "Semua Tugasku");
	define("_SEARCH_USERS", "Tugas");
	define("_SEARCH", "Cari");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "Tugas berhasil ditambah!");
	define("_TASK_UPDATED", "Tugas berhasil diperbaharui!");
	define("_TASK_DELETED", "Tugas berhasil dihapus!");
	define("_TASK_OPENED", "Tugas berhasil dibuka!");
	define("_TASK_CLOSED", "Tugas berhasil ditutup!");
	define("_USER_ADDED", "Pengguna berhasil ditambah!");
	define("_USER_REMOVED", "Pengguna berhasil dipindahkan!");
	define("_USER_EXISTS", "Pengguna ini sudah ada di dalam grup tersebut!");
	define("_USER_TO_GROUP", "Pengguna berhasil ditambahkan ke dalam grup!");
	define("_ENTER_USER", "Anda harus memasukkan nama pengguna!");
	define("_ENTER_PASS", "Anda harus memasukkan sandi!");
	define("_ENTER_COMMENT", "Anda harus memasukkan komentar!");
	define("_ENTER_TITLE", "Anda harus memasukkan judul!");
	define("_PASS_CHANGED", "Sandi berhasil diubah!");
	define("_ENTER_GROUP", "Anda harus memasukkan nama grup!");
	define("_GROUP_ADDED", "Grup berhasil ditambah!");
	define("_GROUP_REMOVED", "Grup berhasil dihapus!");
	define("_GROUP_UPDATED", "Grup  berhasil diperbaharui!");
	define("_REMOVE_YOURSELF", "Anda tidak dapat menghapusnya sendiri!");
	define("_REMOVED_FROM_GROUP", "Pengguna berhasil dihapus dari grup!");
	define("_ADDED_TO_GROUP", "Pengguna berhasil ditambahkan ke dalam grup!");
	define("_COMMENT_ADDED", "Komentar berhasil ditambah!");
	define("_COMMENT_UPDATED", "Komentar berhasil diperbaharui!");
	define("_VERIFY_PASS", "Anda harus memeriksa sandi pengguna!");
	define("_PASS_ERROR", "Sandi tidak sama!");
	define("_SELECT_USER", "Anda harus memilih pengguna untuk ditambahkan ke dalam grup!");
	define("_THEME_CHANGED", "Tema berhasil diubah!");
	define("_LANGUAGE_CHANGED", "Bahasa berhasil diubah");
	define("_INFO_UPDATED", "Informasi berhasil diperbaharui");
	define("_QUERY_ERROR", "Query tidak berhasil!");
	define("_COMMENT_REMOVED", "Komentar berhasil dihapus!");
	define("_CATEGORY_CREATED", "Kategori berhasil ditambah!");
	define("_CATEGORY_UPDATED", "Kategori berhasil diperbaharui!");
	define("_CATEGORY_DELETED", "Kategori berhasil dihapus!");
	define("_ADDED_TO_SUPERCAT", "Kategori berhasil ditambahkan ke dalam supercat!");
	define("_SUPERCAT_REMOVED", "Supercat berhasil dihapus!");
	define("_ENTER_CATEGORY", "Anda harus memasukkan nama kategori!");
	define("_NO_ID", "Tidak ada id yang disediakan!");
	define("_ALREADY_USER", "Sudah ada pengguna yang menggunakan nama tersebut!");
	define("_ALREADY_GROUP", "Sudah ada grup yang menggunakan nama tersebut!");
	define("_MODULE_ENABLE", "Modul berhasil diaktifkan!");
	define("_MODULE_DISABLE", "Modul berhasil dinon-aktifkan!");
	define("_MODULE_INSTALL", "Modul berhasil diinstalasi!");
	define("_MODULE_UNINSTALL", "Modul tidak berhasil diinstalasi!");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "Januari");
	define("_FEBUARY", "Februari");
	define("_MARCH", "Maret");
	define("_APRIL", "April");
	define("_MAY", "Mei");
	define("_JUNE", "Juni");
	define("_JULY", "Juli");
	define("_AUGUST", "Agustus");
	define("_SEPTEMBER", "September");
	define("_OCTOBER", "Oktober");
	define("_NOVEMBER", "Nopember");
	define("_DECEMBER", "Desember");
	define("_SUNDAY", "Minggu");
	define("_MONDAY", "Senin");
	define("_TUESDAY", "Selasa");
	define("_WEDNESDAY", "Rabu");
	define("_THURSDAY", "Kamis");
	define("_FRIDAY", "Jumat");
	define("_SATURDAY", "Sabtu");
	define("_SUN", "Min");
	define("_MON", "Sen");
	define("_TUE", "Sel");
	define("_WED", "Rab");
	define("_THU", "Kms");
	define("_FRI", "Jmt");
	define("_SAT", "Sab");

	/* Other various strings. */
	define("_DATE", "Tanggal");
	define("_REDIRECT", "Alihkan pada tempat ke");
	define("_CATEGORY", "Kategori");
	define("_NO_CATEGORY", "Tidak ada Kategori");
	define("_ALL_CATEGORIES", "Semua Kategori");
	define("_PRIORITY", "Prioritas");
	define("_CREATION", "Ciptaan");
	define("_AUTHORIZATION", "Otorisasi diperlukan!");
	define("_LOGGING_OUT", "Anda telah Logout...");
	define("_GROUP", "Grup");
	define("_ACTIONS", "Aksi");
	define("_USERS_IN_GROUP", "Pengguna ada di dalam grup (klik untuk pindah)");
	define("_USERS_NOT_IN_GROUP", "Pengguna tidak ada di dalam grup (klik untuk tambah)");
	define("_USERS", "Para Pengguna");
	define("_USER", "Pengguna");
	define("_EMPTY", "Kosong!");
	define("_REMOVE", "Hapus");
	define("_COMPLETED", "Diselesaikan?");
	define("_COMPLETE", "Selesai");
	define("_OPEN", "Buka");
	define("_OVERDUE", "Melewati batas waktu");
	define("_TODAY", "Hari ini");
	define("_DAILY", "Harian");
	define("_OPEN_ENDED", "Dibuka-Diselesaikan");
	define("_WEEKLY", "Mingguan");
	define("_MONTHLY", "Bulanan");
	define("_YEARLY", "Tahunan");
	define("_NO_RECURRENCE", "Kejadian tidak berulang secara periodik");
	define("_RECURRE_EVERY", "Berulang Setiap");
	define("_RECURRE_ON", "Berulang Pada");
	define("_EVERY_MONTH", "Setiap Bulan");
	define("_COMPLETED_FOR_DAY", "Diselesaikan selama hari");
	define("_REOPEN_TASK", "Tugas dibuka kembali?");
	define("_STATUS", "Status");
	define("_TITLE", "Judul");
	define("_DEADLINE", "Batas Waktu");
	define("_TASK", "Tugas");
	define("_ASSIGNED_BY", "Ditugaskan oleh");
	define("_ASSIGN_TO", "Ditugaskan ke");
	define("_COMPLETED_BY", "Diselesaikan oleh");
	define("_COMPLETE_TASK_BY", "Batas waktu Tugas");
	define("_COMMENT", "Komentar");
	define("_COMMENT_BY", "Komentar Oleh");
	define("_COMMENT_DATE", "Tanggal Komentar");
	define("_REMOVE_COMMENT", "Hapus Komentar");
	define("_SUBMIT_TASK", "Serahkan Tugas");
	define("_DAILY_TASK", "Tugas Harian");
	define("_WEEKLY_TASK", "Tugas Mingguan");
	define("_UPDATE_TASK", "Tugas Diperbaharui");
	define("_CURRENT_PASS", "Sandi Sekarang");
	define("_ADD", "Tambah");
	define("_VIEW_TASKS", "Lihat Tugas");
	define("_EDIT", "Ubah");
	define("_DELETE", "Hapus");
	define("_ALLOW_OTHERS", "Mengizinkan orang lain memberikan tugas kepada saya?");
	define("_PUBLIC_LIST", "Mengizinkan orang lain melihat daftar yang harus dilakukan saya?");
	define("_ENABLE_EMAIL", "Mengaktifkan Pemberitahuan Email?");
	define("_ENABLE_AIM", "Mengaktifkan Pemberitahuan AIM?");
	define("_UPDATE_INFO", "Informasi Diperbaharui");
	define("_PREV", "Sebelum");
	define("_NEXT", "Sesudah");
	define("_YOUR_CURRENT_PASS", "Anda harus memasukkan sandi anda yang sekarang!");
	define("_YOUR_NEW_PASS", "Anda harus memasukkan sandi yang baru!");
	define("_YOUR_VERIFY_PASS", "Anda harus memeriksa sandi baru anda!");
	define("_NEW_PASS_ERROR", "Sandi baru anda tidak sama dengan verifikasi anda!");
	define("_CURRENT_PASS_ERROR", "Sandi anda yang sekarang tidak sama!");
	define("_CHANGE", "Ubah");
	define("_QUERY", "Query");
	define("_OUTPUT", "Hasil");
	define("_RETURNED", "Dikembalikan");
	define("_COMMAND", "Perintah");
	define("_IS_UNDEFINED", "tidak didefinisikan!");
	define("_AS_OF", "");
	define("_PRINT", "Cetak");
	define("_DESCRIPTION", "Deskripsi");
	define("_DISABLE", "Dinon-aktifkan");
	define("_ENABLE", "Diaktifkan");
	define("_INSTALL", "Instalasi");
	define("_UNINSTALL", "Tidak Diinstalasi");
	define("_MODULE", "Modul");
	define("_CONFIGURE", "Konfigurasi");
	define("_CANCEL", "Batal");
	define("_WHAT_WOULD_YOU_LIKE", "Apa yang ingin anda lakukan?");
	define("_RESET_ALL", "Reset all tasks to 'No Category'?");
	define("_REMOVE_ALL", "Remove all tasks in the category?");

	/* These are used when adding a new user or group. */
	define("_ADD_USER", "Tambah Pengguna");
	define("_ADD_GROUP", "Tambah Grup");
	define("_USER_NAME", "Nama Pengguna");
	define("_NEW_PASS", "Sandi baru");
	define("_VERIFY", "Konfirmasi Sandi");
	define("_FIRST_NAME", "Nama Depan");
	define("_LAST_NAME", "Nama Belakang");
	define("_THEME", "Tema");
	define("_LANGUAGE", "Bahasa");
	define("_DATE_FORMAT", "Format Tanggal");
	define("_EMAIL", "Email");
	define("_AIM", "Nama Layar AIM");
	define("_IS_ADMIN", "Administrator?");
	define("_GROUP_NAME", "Nama Grup");
	define("_GROUP_MEMBERS", "Para Anggota");
	define("_UPDATE", "Diperbaharui");
	define("_SELECT_GROUP", "Pilih grup");
	define("_FINISH_CREATE_USER", "Tetapkan info pengguna/lewatkan");
	define("_AUTO_DAILY", "Tugas harian baru sesuai dengan default?");

	/* These are for login stuff. */
	define("_LOGIN", "Login");
	define("_LOGON_SUCCESSFUL", "Berhasil Login");
	define("_PASSWORD", "Sandi");
	define("_LOGGED_OUT", "Anda sudah logged out");
	define("_KEEP_LOGIN", "Simpan catatanku agar selalu berada pada mesin ini");
	define("_WRONG_PASSWORD", "Nama Pengguna atau Sandi salah");
	
	/* Supercats */
	define("_SUPERCAT", "Supercat");
	define("_LIST_SUPERCATS", "Daftar Supercats");
	define("_CATS_IN_SUPERCAT", "Kategori di dalam Supercat");
	define("_REMOVED_FROM_SUPERCAT", "Kategori berhasil dihapus dari Supercat!");
	define("_CATS_NOT_IN_SUPERCAT", "Kategori tidak ada di dalam Supercat");
	define("_GROUP_SUPERCAT", "Supercat berhasil diciptakan!");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Tidak Didefinisikan', 1 => 'Rendah', 2 => 'Sedang', 3 => 'Tinggi', 4 => 'Delayed', 5 => 'On Hold');
?>
