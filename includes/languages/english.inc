<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: english.inc,v 1.72 2005/12/30 17:44:08 zoidian Exp $
*/
?>
<?php
	/* This is the English language file where you define all the strings
	* used. */

	/* These are for the Main Menu. */
	define("_SHOW_TASKS", "Show Tasks");
	define("_THIS_WEEK", "This Weeks Tasks");

    /* These are for the Status drop down. */
	define("_TASKS_DAILY", "Daily Tasks");
	define("_TASKS_OPEN_ENDED", "Open-Ended Tasks");
	define("_TASKS_WEEKLY", "Weekly Tasks");
	define("_TASKS_MONTHLY", "Monthly Tasks");
	define("_TASKS_YEARLY", "Yearly Tasks");
	define("_TASKS_OPEN", "Open Tasks");
	define("_TASKS_COMPLETED", "Completed Tasks");
	define("_TASKS_OVERDUE", "Overdue Tasks");
	define("_TASKS_ALL", "All Tasks");
	define("_TASKS_THIS_WEEK", "This Weeks Tasks");
	define("_SEARCH_EVERYONES", "Everyone's Tasks");
	define("_SEARCH_MINE", "All My Tasks");
	define("_SEARCH_USERS", "'s Tasks");
	define("_TASKS_DATE", "On ");
    
    /* These are for the Main Menu. */
	define("_NEW_TASK", "New Task");
	define("_CALENDAR", "Calendar");
	define("_ADMIN_MENU", "Admin Menu");
	define("_USER_MENU", "User Menu");
	define("_LOG_OUT", "Log Out");

	/* These are for the Admin Menu. */
	define("_ADMIN_ERROR", "Only the admin can do that!");
	define("_EDIT_GROUPS", "Edit Groups");
	define("_LIST_USERS", "List Users");
	define("_LIST_GROUPS", "List Groups");
	define("_CHANGE_PASS", "Change Password");
	define("_EDIT_INFO", "Edit User Information");
	define("_LIST_CATEGORIES", "List Categories");
	define("_REMOVE_CATEGORY", "Remove category");
	define("_RENAME_CATEGORY", "Rename category");
	define("_USER_CATEGORY_NAME", "User Category Name");
	define("_GLOBAL_CATEGORY_NAME", "Global Category Name");
	define("_ADMIN_COMMAND", "Admin command");
    define("_COMMAND", "Command");
	define("_COMMAND_ERROR", "is undefined or you don't have permission!");
    define("_MODULES", "Modules");

	/* These are used in the buttons for each task. */
	define("_EDIT_TASK", "Edit Task");
	define("_DELETE_TASK", "Delete Task");
	define("_VIEW_TASK", "View Task");
	define("_ACCESS_DENIED", "Access Denied");

	/* Success/Failure strings. */
	define("_TASK_ADDED", "Task added successfully!");
	define("_TASK_UPDATED", "Task updated successfully!");
	define("_TASK_DELETED", "Task deleted successfully!");
	define("_TASK_OPENED", "Task opened successfully!");
	define("_TASK_CLOSED", "Task successfully closed!");
	define("_USER_ADDED", "User added successfully!");
	define("_USER_REMOVED", "User removed successfully!");
	define("_ENTER_USER", "You must enter a user name!");
	define("_ENTER_COMMENT", "You must enter a comment!");
	define("_ENTER_TITLE", "You must enter a title!");
	define("_PASS_CHANGED", "Password changed successfully!");
	define("_ENTER_GROUP", "You must enter a group name!");
	define("_GROUP_ADDED", "Group added successfully!");
	define("_GROUP_REMOVED", "Group removed successfully!");
	define("_GROUP_UPDATED", "Group updated successfully!");
	define("_REMOVE_YOURSELF", "You can't remove yourself!");
	define("_REMOVED_FROM_GROUP", "User removed from group successfully!");
	define("_ADDED_TO_GROUP", "User added to group successfully!");
	define("_COMMENT_ADDED", "Comment added successfully!");
	define("_COMMENT_UPDATED", "Comment updated successfully!");
	define("_VERIFY_PASS", "You must verify the users password!");
	define("_PASS_ERROR", "The passwords don't match!");
	define("_INFO_UPDATED", "Information updated successfully!");
	define("_QUERY_ERROR", "The query was unsuccessful!");
	define("_COMMENT_REMOVED", "Comment removed successfully!");
	define("_CATEGORY_CREATED", "Category added successfully!");
	define("_CATEGORY_UPDATED", "Category updated successfully!");
	define("_CATEGORY_DELETED", "Category deleted successfully!");
	define("_ADDED_TO_SUPERCAT", "Category added to supercat successfully!");
	define("_SUPERCAT_REMOVED", "Supercat removed successfully!");
	define("_ENTER_CATEGORY", "You must enter a category name!");
	define("_NO_ID", "No id provided!");
	define("_ALREADY_USER", "There is already a user with that name!");
	define("_ALREADY_GROUP", "There is already a group with that name!");
    define("_MODULE_ENABLE", "Module enabled successfully!");
    define("_MODULE_DISABLE", "Module disabled successfully!");
    define("_MODULE_INSTALL", "Module installed successfully!");
    define("_MODULE_UNINSTALL", "Module uninstalled successfully!");

	/* These are used for the Calendar and formatting dates. */
	define("_JANUARY", "January");
	define("_FEBUARY", "February");
	define("_MARCH", "March");
	define("_APRIL", "April");
	define("_MAY", "May");
	define("_JUNE", "June");
	define("_JULY", "July");
	define("_AUGUST", "August");
	define("_SEPTEMBER", "September");
	define("_OCTOBER", "October");
	define("_NOVEMBER", "November");
	define("_DECEMBER", "December");
	define("_SUNDAY", "Sunday");
	define("_MONDAY", "Monday");
	define("_TUESDAY", "Tuesday");
	define("_WEDNESDAY", "Wednesday");
	define("_THURSDAY", "Thursday");
	define("_FRIDAY", "Friday");
	define("_SATURDAY", "Saturday");
	define("_SUN", "Sun");
	define("_MON", "Mon");
	define("_TUE", "Tue");
	define("_WED", "Wed");
	define("_THU", "Thu");
	define("_FRI", "Fri");
	define("_SAT", "Sat");

	/* Other various strings. */
    define("_CHANGE_DUE_DATE", "Change Due Date");
    define("_VIEW_UPDATED", "Default view updated successfully!");
	define("_CATEGORY", "Category");
	define("_NO_CATEGORY", "No Category");
    define("_ALL_CATEGORIES", "All Categories");
	define("_PRIORITY", "Priority");
	define("_CREATION", "Creation");
	define("_GROUP", "Group");
	define("_ACTIONS", "Actions");
	define("_WHOS_TASK", "Who's Task");
	define("_WHO", "Who");
	define("_USERS_IN_GROUP", "Users in group (click to remove)");
	define("_USERS_NOT_IN_GROUP", "Users not in group (click to add)");
	define("_USER", "User");
	define("_EMPTY", "Empty!");
	define("_REMOVE", "Remove");
	define("_COMPLETE", "Complete");
	define("_OPEN", "Open");
	define("_OVERDUE", "Overdue");
	define("_TODAY", "Today");
	define("_DAILY", "Daily");
	define("_OPEN_ENDED", "Open-Ended");
	define("_WEEKLY", "Weekly");
	define("_MONTHLY", "Monthly");
	define("_YEARLY", "Yearly");
    define("_DATE", "Date");
    define("_DAY", "Day");
    define("_DAYS", "Days");
	define("_NO_RECURRENCE", "No Recurrence");
    define("_RECURRENCE", "Recurrence");
	define("_RECURRE_EVERY", "Recurre Every");
	define("_RECURRE_ON", "Recurre On");
	define("_EVERY_MONTH", "Every Month");
	define("_STATUS", "Status");
	define("_TITLE", "Title");
	define("_DEADLINE", "Deadline");
	define("_TASK", "Task");
	define("_ASSIGNED_BY", "Assigned By");
	define("_ASSIGN_TO", "Assign To");
	define("_COMPLETED_BY", "Completed By");
	define("_COMPLETE_TASK_BY", "Task Due");
	define("_COMMENT", "Comment");
	define("_COMMENT_BY", "Comment By");
	define("_COMMENT_DATE", "Comment Date");
	define("_UPDATE_TASK", "Update Task");
	define("_CURRENT_PASS", "Current Password");
	define("_ADD", "Add");
	define("_FILTER", "Filter");
	define("_VIEW_TASKS", "View Tasks");
	define("_EDIT", "Edit");
	define("_DELETE", "Delete");
    define("_ENABLE_AJAX", "Enable Ajax?");
	define("_ALLOW_OTHERS", "Allow others to assign tasks to me?");
	define("_PUBLIC_LIST", "Allow others to view my todo list?");
	define("_UPDATE_INFO", "Update Information");
	define("_PREV", "Prev");
	define("_NEXT", "Next");
	define("_YOUR_CURRENT_PASS", "You must enter your current password!");
	define("_YOUR_NEW_PASS", "You must enter a new password!");
	define("_YOUR_VERIFY_PASS", "You must verify your new password!");
	define("_NEW_PASS_ERROR", "Your new password doesn't match your verification!");
	define("_CURRENT_PASS_ERROR", "Your current password doesn't match!");
    define("_IS_UNDEFINED", "is undefined!");
    define("_AS_OF", "as of");
    define("_PRINT", "Print");
    define("_EXPORT", "Export");
    define("_SET_VIEW", "Set as Default View");
    define("_DESCRIPTION", "Description");
    define("_DISABLE", "Disable");
    define("_ENABLE", "Enable");
    define("_INSTALL", "Install");
    define("_UNINSTALL", "Uninstall");
    define("_MODULE", "Module");
    define("_CONFIGURE", "Configure");
    define("_CANCEL", "Cancel");
    define("_WHAT_WOULD_YOU_LIKE", "What would you like to do?");
    define("_RESET_ALL", "Reset all tasks to 'No Category'?");
    define("_REMOVE_ALL", "Remove all tasks in the category?");

	/* These are used when adding a new user or group. */
	define("_USER_NAME", "User Name");
	define("_NEW_PASS", "New Password");
	define("_VERIFY", "Verify");
	define("_FIRST_NAME", "First Name");
	define("_LAST_NAME", "Last Name");
	define("_THEME", "Theme");
	define("_LANGUAGE", "Language");
	define("_DATE_FORMAT", "Date Format");
	define("_EMAIL", "Email");
	define("_AIM", "AIM Screenname");
	define("_IS_ADMIN", "Administrator?");
	define("_GROUP_NAME", "Group Name");
	define("_GROUP_MEMBERS", "Members");
	define("_UPDATE", "Update");
	define("_SELECT_GROUP", "Select a group");
	define("_FINISH_CREATE_USER", "Set user info/pass");
	define("_AUTO_DAILY", "New tasks daily by default?");

	/* These are for login stuff. */
	define("_LOGIN", "Login");
	define("_LOGON_SUCCESSFUL", "Login Successful");
	define("_PASSWORD", "Password");
	define("_LOGGED_OUT", "You have been logged out");
	define("_KEEP_LOGIN", "Keep me logged in on this machine");
	define("_WRONG_PASSWORD", "Wrong username or password");
	
	/* Supercats */
	define("_SUPERCAT", "Supercat");
	define("_LIST_SUPERCATS", "List Supercats");
	define("_CATS_IN_SUPERCAT", "Categories in Supercat");
	define("_REMOVED_FROM_SUPERCAT", "Category removed from Supercat successfully!");
	define("_CATS_NOT_IN_SUPERCAT", "Categories not in Supercat");
	define("_GROUP_SUPERCAT", "Supercat created successfully!");
	define("_ALREADY_SUPERCAT", "There is already a Supercat with that name!");

    /* Email Notifications */
    define("_SUBJECT", "Task Assignment Notification!");
    define("_GREETING", "You've been assigned a task!");
    define("_CLOSING", "This notification was brought to you by:");
	
	global $_PRIORITY;
	$_PRIORITY = array(0 => 'Undefined', 1 => 'Low', 2 => 'Medium', 3 => 'High', 4 => 'Delayed', 5 => 'On Hold');
?>
