<?php
/* vim: set ts=4 sw=4 si::
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: ajax.php,v 1.15 2006/09/22 14:01:24 zoidian Exp $
*/
?>
<?php
global $login, $isadmin;

function changeText($sValue) {
	global $table_prefix;

    $sValue_array = explode("~~|~~", $sValue);
    $parsedInput = $sValue_array[0];
    $parsedInput = strip_tags($parsedInput, '<b><i><u><br><li>');
    $parsedInput = db_sanitize($parsedInput);
    $id = $sValue_array[1];
    $type = $sValue_array[2];

    if($type != 'task') {
        $id = explode("_", $id);
        $id = $id[1];
    }

    if($id) {
        if($type == 'task') {
    	    $sql = db_query("UPDATE ".$table_prefix."tasks "
                           ."SET title='$parsedInput' "
                           ."WHERE id='$id'");
        } else {
    	    $sql = db_query("UPDATE ".$table_prefix."comments "
                           ."SET comment='$parsedInput',date=now() "
                           ."WHERE id='$id'");
        }
    }
    $parsedInput = stripslashes($parsedInput);
    
    $result = db_query("SELECT * FROM ".$table_prefix."comments "
                      ."WHERE task_id='$id'");
    $num_comments = mysql_num_rows($result);
    
    if($num_comments > 0) {
        $ast = '*';
        $title = $num_comments.' Comments';
    } else {
        $ast = '';
        $title = 'No Comments';
    }

    if($type == 'task') {
        $newText = '<span class="tasktitle"><span id="com_'.$id.'_plus_minus"><a href="#" style="text-decoration:none;" onclick="toggleItem(\''.$id.'\', '.$num_comments.');"><img src="images/plus.gif" /></a>&nbsp;</span><a href="index.php?cmd=view_task&amp;task_id='.$id.'" title="'.$title.'" class="task">'.$parsedInput.' '.$ast.'</a></span><span class="editbox"><a href="#" onclick="editCell(\''.$id.'\', this, \''.$parsedInput.'\', \'task\');return flase;">'._EDIT.'</a></span>~~|~~'.$id.'~~|~~'.$type;
    } else {
        $newText = '<span class="tasktitle">'.$parsedInput.'</span><span class="editbox"><a href="#" onclick="return false;">'._DELETE.'</a></span><span class="editbox"><a href="#" onclick="editCell(\'num_'.$id.'\', this, \''.$parsedInput.'\', \''.$type.'\');return flase;">'._EDIT.'</a></span>~~|~~num_'.$id.'~~|~~'.$type;
    }
    
    return $newText;
}

function delTask($sValue) {
	global $table_prefix;

    if(isset($sValue)) {
        $sql = @mysql_query("DELETE FROM ".$table_prefix."tasks WHERE id='$sValue'");
    }

    return $sValue;
}

function delComment($sValue) {
	global $table_prefix;

    $sValue_array = explode("~~|~~", $sValue);
    $task_id = $sValue_array[0];
    $com_id = $sValue_array[1];
    $row_num = $sValue_array[2];

    
    if(isset($sValue)) {
        $sql = @mysql_query("DELETE FROM ".$table_prefix."comments WHERE id='$com_id'");
    }

    return $sValue;
}

function updatePriority($sValue) {
	global $table_prefix;

    $sValue_array = explode("~~|~~", $sValue);
    $id = $sValue_array[0];
    $new_prio = $sValue_array[1];

    if(!empty($new_prio) || $new_prio == 0) {
        $sql = @mysql_query("UPDATE ".$table_prefix."tasks SET priority='$new_prio' "
                           ."WHERE id='$id'");
    }

    return $sValue;
}

function updateTask($sValue) {
	global $table_prefix;

    $sValue_array = explode("~~|~~", $sValue);
    $id = $sValue_array[0];
    $new_text = $sValue_array[1];

    if(!empty($new_text)) {
        $sql = db_query("UPDATE ".$table_prefix."tasks SET task='$new_text' WHERE id='$id'");
    }

    return $sValue;
}

function updateCategory($sValue) {
	global $table_prefix;

    $sValue_array = explode("~~|~~", $sValue);
    $id = $sValue_array[0];
    $new_cat = $sValue_array[1];

    if(!empty($new_cat) || $new_cat == 0) {
        $sql = @mysql_query("UPDATE ".$table_prefix."tasks SET category='$new_cat' "
                           ."WHERE id='$id'");
    }

    return $sValue;
}

function updateDate($sValue) {
	global $table_prefix;

    $sValue_array = explode("~~|~~", $sValue);
    $id = $sValue_array[0];
    $new_date = $sValue_array[1];
    $date_format = $_SESSION['date_format'];
    $theme = $_SESSION['theme'];

    $result = db_query("SELECT * FROM ".$table_prefix."tasks WHERE id='$id'");
    $myrow = mysql_fetch_array($result);
    $myrow['date'] = $new_date;

    list($status, $image) = get_status($myrow, $date_format);

    if(!empty($new_date)) {
        $sql = @mysql_query("UPDATE ".$table_prefix."tasks SET date='$new_date' "
                           ."WHERE id='$id'");
    }

    if(isset($image)) {
        $img = '<img align="middle" src="themes/'.$theme.'/images/'.$image.'" alt="'.$status.'" />&nbsp;';
    }

    $newText = '<span class="tasktitle">'.$img.'&nbsp;'.$status.'</span><span class="editbox"><a href="index.php?cmd=popcal&amp;id='.$id.'&amp;pop_how=list" onClick="javascript:nw=window.open(\'index.php?cmd=popcal&amp;id='.$id.'&amp;pop_how=list\',\'popcal\',\'height=500,width=500,location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes\');nw.opener=self;return false;"><img src="themes/'.$theme.'/images/calendar.gif" alt="'._CHANGE_DUE_DATE.'" /></a></span>~~|~~'.$id;
    
    return $newText;
}

function addComment($sValue) {
	global $table_prefix;

    $sValue_array = explode("~~|~~", $sValue);
    $id = $sValue_array[0];
    $login = $_SESSION['login'];
    $comment = $sValue_array[1];
    $date = date("m/d/Y");

    if(isset($comment)) {
        $sql = @mysql_query("INSERT INTO ".$table_prefix."comments (task_id,login,date,comment) "
                           ."VALUES ('$id','$login',now(),'$comment')");
    }
    
    $full = $login . "~~|~~" . $comment . "~~|~~" . $date;

    return $full;
}
?>
