<?php

    session_start();

    // if (isset($_GET['name']) && isset($_SESSION['userid'])):
    if (isset($_GET['name']) && isset($_SESSION['login'])): 
    $_SESSION['userid'] = $_SESSION['login'];
      require_once("../dbcon.php");
  
      $name = cleanInput($_GET['name']);
      $me = $_SESSION['login'];
      $getRooms = "SELECT *
  			           FROM chat_people
  		             WHERE people1 = '$me' AND people2 = '$name'";
  		         
      $roomResults = mysql_query($getRooms);

      $userid = $me;
      $getRoom = "SELECT *
               FROM chat_group WHERE login='$userid'";
      $roomResult = mysql_query($getRoom);
		
	  	if (mysql_num_rows($roomResults) < 1) {
  			header("Location: ../chatrooms.php");
  			die();
  		}
        	
      while ($rooms = mysql_fetch_array($roomResults)) {
          $file =  $rooms['namafile'];
      }

?>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title><?php echo $name; ?></title>
    
    <link rel="stylesheet" type="text/css" href="main.css"/>
    <link rel="stylesheet" href="../css/foundation.min.css">
    <script src="../jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="chat.js"></script>
    <script type="text/javascript">
    	var chat = new Chat('<?php echo $file; ?>');
    	chat.init();
    	chat.getUsers(<?php echo "'" . $name ."','" .$_SESSION['userid'] . "'"; ?>);
    	var name = '<?php echo $_SESSION['userid'];?>';
    </script>
    <script type="text/javascript" src="settings.js"></script>

</head>

<body >
  <nav class="top-bar" data-topbar="">
    <ul class="title-area">
      <li class="name">
        <h1><a href="../../">AckerTodo</a></h1>
      </li>
      <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
    </ul>

    
  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="right">
    <li class="divider"></li>
        <li><a href="../" accesskey="k">Chat</a></li>
    <li class="divider"></li>
        <li><a href="../../index.php?cmd=show" accesskey="s">Show Tasks</a></li>
    <li class="divider"></li>
        <li><a href="../../index.php?cmd=new_task" accesskey="n">New Task</a></li>
    <li class="divider"></li>
        <li><a href="../../index.php?cmd=calendar" accesskey="c">Calendar</a></li>
    <li class="divider"></li>
        <li><a href="../../index.php?cmd=admin_menu" accesskey="m">Admin Menu</a></li>
    <li class="divider"></li>
        <li><a href="../../index.php?cmd=do_logout" accesskey="l">Log Out</a></li>
      </ul>
    </section></nav>

    <div class="row">
      <div class="large-9 push-3 columns">
        <h2><?php echo $name; ?></h2>
        <hr>          
        <div class="panel">
          <div id="chat-area"></div>
          <form id="send-message-area" action="">
            <label>Message:
              <textarea id="sendie" maxlength='100'></textarea>
            </label>
          </form>
        </div>
      </div>
      
      <div class="large-3 pull-9 columns">
        <h3>Groups</h3>
                <ul>
                    <?php 
                        $listfriend = "";
                        while($rooms = mysql_fetch_array($roomResult)):
                            $room = $rooms['groups'];
                            //get friends of group
                            global $usersamegrup;
                            $friends = mysql_query("SELECT * FROM online_user WHERE groupname = '$room' GROUP BY login") or die("Cannot find data". mysql_error());
                            while($friend = mysql_fetch_array($friends)) {
                                $name = $friend['login'];
                                if ($name != $_SESSION['login']) {
                                    $fgroup = $friend['groupname'];
                                    $listfriend = $listfriend . '<li><a href="?name='. $name . '">' . $name . '</a></li>';
                                    $usersamegrup = $name;
                                }
                            }
                            $query = mysql_query("SELECT * FROM `chat_users_rooms` WHERE `room` = '$room' ") or die("Cannot find data". mysql_error());
                            $numOfUsers = mysql_num_rows($query);
                    ?>
                    <li>
                        <a href="../room/?name=<?php echo urlencode($rooms['groups'])?>"><?php echo $rooms['groups'] . '  <span class="round label">' . $numOfUsers . '</span>' ?></a>
                    </li>
                    <?php endwhile; ?>
                </ul>
                <br />
                <h3>Friends</h3>
                <ul>
                    <?php echo $listfriend;
                    ?>
                </ul>
      </div>
    </div>

    
        
</body>

</html>

<?php
    else:
            header('Location: ../index.php');
    endif; 
?>
