<?php 
    
    // session_start();

    require_once("dbcon.php");

    // if (checkVar($_SESSION['userid'])): 
    //     $userid = $_SESSION['userid'];
    if (checkVar($_SESSION['login'])): 
        $userid = $_SESSION['login'];
        $_SESSION['userid'] = $_SESSION['login'];
        $getRooms = "SELECT *
        			 FROM chat_group WHERE login='$userid'";
        $roomResults = mysql_query($getRooms);		  

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Chat Area</title>
    <link rel="stylesheet" href="../css/foundation.min.css">
    <link rel="stylesheet" type="text/css" href="main.css"/>
</head>

<body>

    
    
    	<div id="theheader">
    	
        	<nav class="top-bar" data-topbar="">
    <ul class="title-area">
      <li class="name">
        <h1><a href="../">AckerTodo</a></h1>
      </li>
      <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
    </ul>

    
  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="right">
    <li class="divider"></li>
        <li><a href="../chat" accesskey="k">Chat</a></li>
    <li class="divider"></li>
        <li><a href="../index.php?cmd=show" accesskey="s">Show Tasks</a></li>
    <li class="divider"></li>
        <li><a href="../index.php?cmd=new_task" accesskey="n">New Task</a></li>
    <li class="divider"></li>
        <li><a href="../index.php?cmd=calendar" accesskey="c">Calendar</a></li>
    <li class="divider"></li>
        <li><a href="../index.php?cmd=admin_menu" accesskey="m">Admin Menu</a></li>
    <li class="divider"></li>
        <li><a href="../index.php?cmd=do_logout" accesskey="l">Log Out</a></li>
      </ul>
    </section></nav>
        	
        </div>
    
    <div id="page-wrap">     
    	<div id="section">
    	
            <div id="rooms">
            	<h3>Groups</h3>
                <ul>
                    <?php 
                        $listfriend = "";
                        while($rooms = mysql_fetch_array($roomResults)):
                            $room = $rooms['groups'];
                            //get friends of group
                            global $usersamegrup;
                            $friends = mysql_query("SELECT * FROM online_user WHERE groupname = '$room' GROUP BY login") or die("Cannot find data". mysql_error());
                            while($friend = mysql_fetch_array($friends)) {
                              
                                $name = $friend['login'];
                                if (($name != $_SESSION['login']) && ($name != $usersamegrup)) {
                                    $fgroup = $friend['groupname'];
                                    $listfriend = $listfriend . '<li><a href="message/?name='. $name . '">' . $name . '<span><strong>' . '</strong></span></a></li>';
                                    $usersamegrup = $name;
                                }
                            }
                            $query = mysql_query("SELECT * FROM `chat_users_rooms` WHERE `room` = '$room' ") or die("Cannot find data". mysql_error());
                            $numOfUsers = mysql_num_rows($query);
                    ?>
                    <li>
                        <a href="room/?name=<?php echo $rooms['groups']?>"><?php echo $rooms['groups'] . "<span>Users chatting: <strong>" . $numOfUsers . "</strong></span>" ?></a>
                    </li>
                    <?php endwhile; ?>
                </ul>
                <hr>
                <h3>Friends</h3>
                <ul>
                    <?php echo $listfriend;
                    ?>
                </ul>
            </div>
        </div>
        
    </div>

</body>

</html>

<?php 

    else: 
	   header('Location: ../index.php');
	   
	endif;
	
?>