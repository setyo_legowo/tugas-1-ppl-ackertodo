function addComment_cb(result) {
    var result_array = result.split("~~|~~");
        
    var tbl = document.getElementById('comments');
    var lastRow = tbl.rows.length;
    var iteration = lastRow;
    var row = tbl.insertRow(lastRow-1);

    row.setAttribute('class', 'odd');

    var cellComment = row.insertCell(0);
    var textComment = document.createTextNode(result_array[1]);
    cellComment.appendChild(textComment);

    var cellName = row.insertCell(1);
    var textName = document.createTextNode(result_array[0]);
    cellName.setAttribute('align', 'center');
    cellName.appendChild(textName);

    var cellDate = row.insertCell(2);
    var textDate = document.createTextNode(result_array[2]);
    cellDate.setAttribute('align', 'center');
    cellDate.appendChild(textDate);

    var cellActions = row.insertCell(3);
    var blank = document.createTextNode(' ');
    var textRemove = document.createElement('a');
    textRemove.innerHTML = "Remove";
    textRemove.setAttribute('class', 'commentlink');
    textRemove.setAttribute('style', "font-family: Verdana, 'trebuchet ms', helvetica, sans-serif;font-weight: bold;color: black;background-color: #CEC;font-size: 75%;border: 1px solid;border-top-color: #696;border-left-color: #696;border-right-color: #363;border-bottom-color: #363;");
    textRemove.setAttribute('href', 'index.php?cmd=do_remove_comment&amp;comment_id=');
    var textEdit = document.createElement('a');
    textEdit.innerHTML = "Edit";
    textEdit.setAttribute('style', "font-family: Verdana, 'trebuchet ms', helvetica, sans-serif;font-weight: bold;color: black;background-color: #CEC;font-size: 75%;border: 1px solid;border-top-color: #696;border-left-color: #696;border-right-color: #363;border-bottom-color: #363;");
    textEdit.setAttribute('href', 'index.php?cmd=view_task&amp;task_what=edit_comment&amp;comment_id=');
    cellActions.appendChild(textRemove);
    cellActions.appendChild(blank);
    cellActions.appendChild(textEdit);

    document.getElementById('newComment').innerHTML = '';
}   

function prepareComment(task_id) {
    var temp = document.getElementById('newComment').value;
    var full = task_id + "~~|~~" + temp;
    x_addComment(full, addComment_cb)
}
