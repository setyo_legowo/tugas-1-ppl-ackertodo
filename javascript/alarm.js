var jsalarm={
	padfield:function(f){
		return (f<10)? "0"+f : f
	},
	showcurrenttime:function(){
		var dateobj=new Date()
		var ct=this.padfield(dateobj.getHours())+":"+this.padfield(dateobj.getMinutes())+":"+this.padfield(dateobj.getSeconds())+" ,"+this.padfield(dateobj.getDate())+"-"+this.padfield(dateobj.getMonth()+1)+"-"+this.padfield(dateobj.getFullYear())
		this.ctref.innerHTML=ct
		this.ctref.setAttribute("title", ct)
	},
	
	showalarm:function(year, month, day, hours, minutes, seconds, milliseconds){
		var dateobj=new Date(year, month, day, hours, minutes, seconds, milliseconds)
		var ct=this.padfield(hours)+":"+this.padfield(minutes)+":"+this.padfield(seconds)+" ,"+this.padfield(day)+"-"+this.padfield(month)+"-"+this.padfield(year)
		this.ctref.innerHTML=ct
		this.ctref.setAttribute("title", ct)
	},
	
	showremaining:function(remain){
		if (remain>0)
		var rem=""+Math.floor(remain/(3600*24))+" days "+(Math.floor(remain/3600))%60+" hours "+(Math.floor(remain/60))%60+" minutes "+(remain%60)+" seconds"
		else
		var rem="Times Up!!!"
		this.ctref.innerHTML=rem
		this.ctref.setAttribute("title", rem)
	},
	
	init:function(year, month, day, hours, minutes, seconds, milliseconds, alarm){
		var set_alarm = alarm*60
		var date_al=new Date(year, month-1, day, hours, minutes, seconds, milliseconds)
		var date_ct=new Date()
		var remaining = Math.floor((date_al.getTime()-date_ct.getTime())/1000)+1
		
		this.ctref=document.getElementById("jsalarm_al")
		jsalarm.showalarm(year, month, day, hours, minutes, seconds, milliseconds)
		this.ctref=document.getElementById("jsalarm_ct")
		jsalarm.showcurrenttime()
		this.ctref=document.getElementById("jsalarm_rt")
		jsalarm.showremaining(remaining)
		if (remaining==set_alarm){
			playSound('sound/a')
		}
	},
	
}

 function playSound(filename){   
                document.getElementById("sound").innerHTML='<audio autoplay="autoplay"><source src="' + filename + '.mp3" type="audio/mpeg" /><source src="' + filename + '.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="' + filename +'.mp3" /></audio>';
            }