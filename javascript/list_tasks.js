function textChanger_cb(result) {
   var result_array = result.split("~~|~~");
   var text = result_array[0];
   var id = result_array[1];
   var type = result_array[2];

   document.getElementById(type+'_'+id).innerHTML = text;
   Effect.Pulsate(document.getElementById(type+'_'+id));
}
    
function parseForm(cellID, inputID, type) {
   var temp = document.getElementById(inputID).value;
   var obj = /^(\s*)([\W\w]*)(\b\s*$)/;
   var st = document.getElementById(inputID).value+'~~|~~'+cellID+'~~|~~'+type;
   
   if(obj.test(temp)) {
      temp = temp.replace(obj, '$2');
   }
   
   var obj = /  /g;
   while(temp.match(obj)) {
      temp = temp.replace(obj, " ");
   }
   
   if(temp == " ") {
      temp = "";
   }
   
   if(!temp) {
      alert("This field must contain at least one non-whitespace character.");
      return;
   }
   
   document.getElementById(type+'_'+cellID).innerHTML = "<span class=\"update\">Updating...</span>";
   x_changeText(st, textChanger_cb);
}

function editCell(id, cellSpan, text, type) {
   var inputWidth = (document.getElementById(type+'_'+id).offsetWidth / 7);
   var oldCellSpan = text;
   var cell = type + '_input_' + id;

   document.getElementById(type+'_'+id).innerHTML = "<form name=\"activeForm\" onsubmit=\"parseForm('"+id+"', '"+cell+"', '"+type+"');return false;\" style=\"margin:0;\" action=\"\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"text\" id=\""+cell+"\" size=\""+ inputWidth + "\" maxlength=\"50\" onblur=\"parseForm('"+id+"', '"+cell+"', '"+type+"');return false;\"><br /><noscript><input value=\"OK\" type=\"submit\"></noscript></form>";

   document.getElementById(cell).value = oldCellSpan;
   document.getElementById(cell).focus();
}

function delTask_cb(id) {
   Effect.Fade(document.getElementById('delRow_'+id).parentNode.parentNode);
   Effect.Fade(document.getElementById('edit_'+id));

   var com_head = document.getElementById('row_'+id);

   if (undefined != com_head) {
      Effect.Fade(com_head);

      var i = 0;
      var com_row = document.getElementById('com_'+id+'_num_'+i);
      while (undefined != com_row) {
         Effect.Fade(com_row);
         i++;
         var com_row = document.getElementById('com_'+id+'_num_'+i);
      }
   }
}

function startDeleteTask(task_id) {
   x_delTask(task_id, delTask_cb);
}

function delComment_cb(result) {
   var result_array = result.split("~~|~~");
   var task_id = result_array[0];
   var com_id = result_array[1];
   var row_num = result_array[2];
   
   Effect.Fade(document.getElementById('com_'+task_id+'_num_'+row_num));
}

function startDeleteComment(task_id, com_id, row_num) {
   var st = task_id + '~~|~~' + com_id + '~~|~~' + row_num;
   x_delComment(st, delComment_cb);
}

function updatePriority_cb(result) {
   var result_array = result.split("~~|~~");
   var new_class = 'even';
   var id = result_array[0];
   var new_prio = result_array[1];
   var cell = result_array[2];

   if(new_prio != 0)
      new_class = 'odd_' + new_prio;

   document.getElementById('prio_'+id).parentNode.parentNode.setAttribute('class', new_class);
   document.getElementById('prio_'+id).parentNode.parentNode.setAttribute('className', new_class);
}

function startUpdatePriority(id, new_prio, cell) {
   var st = id + '~~|~~' + new_prio + '~~|~~' + cell;
   x_updatePriority(st, updatePriority_cb);
}

function updateCategory_cb(result) {
   var result_array = result.split("~~|~~");
   var id = result_array[0];
   var new_cat = result_array[1];
   var cell = result_array[2];

   Effect.Pulsate(document.getElementById('cat_'+id).parentNode);
}

function startUpdateCategory(id, new_cat, cell) {
   var st = id + '~~|~~' + new_cat + '~~|~~' + cell;
   x_updateCategory(st, updateCategory_cb);
}

function updateTask_cb(result) {
   var result_array = result.split("~~|~~");
   var id = result_array[0];
   document.getElementById('in_edit_'+id+'_status').innerHTML = "Update Successful!";
}

function startUpdateTask(id) {
   var temp = document.getElementById('in_edit_'+id).value;
   var obj = /^(\s*)([\W\w]*)(\b\s*$)/;
   var st = id + '~~|~~' + document.getElementById('in_edit_'+id).value;

   if(obj.test(temp)) {
      temp = temp.replace(obj, '$2');
   }
   
   var obj = /  /g;
   while(temp.match(obj)) {
      temp = temp.replace(obj, " ");
   }
   
   if(temp == " ") {
      temp = "";
   }
   
   if(!temp) {
      alert("This field must contain at least one non-whitespace character.");
      return;
   }

   x_updateTask(st, updateTask_cb);
}

function getItem(id) {
   var itm = false;
   
   if(document.getElementById) {
      itm = document.getElementById(id);
   } else if(document.all) {
      itm = document.all[id];
   } else if(document.layers) {
      itm = document.layers[id];
   }

   return itm;
}

function toggleItem(id, num) {
   itm = getItem('row_'+id);
   pm = getItem('plus_minus_'+id);
   edit = getItem('edit_'+id);
   task_class = edit.getAttribute('class');

   if(task_class == 'hide' || task_class  == null) {
      edit.setAttribute('class', 'show');
      edit.setAttribute('className', 'show');
      
      if(itm) {
         itm.setAttribute('class', 'show');
         itm.setAttribute('className', 'show');
      }

      pm.innerHTML = '<a href="#" style="text-decoration:none;" onclick="toggleItem(\''+id+'\', '+num+');"><img src="images/minus.gif" alt="Collapse" /></a>';
   } else {
      edit.setAttribute('class', 'hide');
      edit.setAttribute('className', 'hide');
      
      if(itm) {
         itm.setAttribute('class', 'hide');
         itm.setAttribute('className', 'hide');
      }
      pm.innerHTML = '<a href="#" style="text-decoration:none;" onclick="toggleItem(\''+id+'\', '+num+');"><img src="images/plus.gif" alt="Expand" /></a>';
   }

   if(num > 0) {
      for(i = 0; i < num; i++) {
         tmp = getItem('com_'+id+'_num_'+i);
         row_class = tmp.getAttribute('class');

         if(row_class == 'hide' || row_class == null) {
            tmp.setAttribute('class', 'show');
            tmp.setAttribute('className', 'show');
         } else {
            tmp.setAttribute('class', 'hide');
            tmp.setAttribute('className', 'hide');
         }
      }
   }

   return false;
}
