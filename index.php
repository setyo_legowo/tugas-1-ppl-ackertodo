<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: index.php,v 1.130 2006/09/24 23:02:57 zoidian Exp $
*/
?>
<?php

// fixme: DO NOT USE $_REQUEST, a post is a post and a get is a get.

// fix URLs for XHTML
ini_set('arg_separator.output', '&amp;');

include('config/config.inc.php');

if (isset($_REQUEST['validator_mode'])) {
	$debug = false;
}

include('includes/groups.inc');
include('includes/users.inc');
include('includes/tasks.inc');
include('includes/categories.inc');
include('includes/funcs.inc');
include('includes/additional.inc'); // BY: ANDREAS DWI NUGROHO
include('includes/session.inc');
include_once('includes/db.inc');

// enable strict error reporting
if ($debug) {
	ini_set('error_reporting', E_ALL);
	set_error_handler('error_handler');
}

$login = $_SESSION['login'];
$isadmin = $_SESSION['isadmin'];
$theme = $_SESSION['theme'];
$language = $_SESSION['language'];
if(!isset($_SESSION['extra']))
    $_SESSION['extra'] = null;

load_language($language);

$PHP_SELF = $_SERVER['PHP_SELF'];

// frequently-used variables
if(isset($_REQUEST['users'])) {
	$users = $_REQUEST['users'];
}

// the command the user selected
if(isset($_REQUEST['cmd']) && strlen(trim($_REQUEST['cmd'])) > 0) {
    $cmd = htmlentities($_REQUEST['cmd'], ENT_QUOTES, 'UTF-8');
} else {
    $cmd = "";
}

/* No debug statements before this point, they will be overwritten... */

// an object used to communicate data between the controller (this class) and the view (the html template)
$output = array();
$output['login'] = $login;

// get info stored in the session before a redirect
if (isset($_SESSION['error'])) {
	$output['error'] = $_SESSION['error'];
	unset($_SESSION['error']);
}
if (isset($_SESSION['message'])) {
	$output['message'] = $_SESSION['message'];
	unset($_SESSION['message']);
}
if (isset($_SESSION['next_view'])) {
	$cmd = $_SESSION['next_view'];
	unset($_SESSION['next_view']);
}
if (isset($_SESSION['debug_output'])) {
	$debug_output = $_SESSION['debug_output'];
	unset($_SESSION['debug_output']);
} else {
	$debug_output = '';
}

if ($debug) {
    debug('GET: ' . print_r($_GET, true));
    debug('POST: ' . print_r($_POST, true));
    debug('COOKIE: ' . print_r($_COOKIE, true));
    debug('SESSION: ' . print_r($_SESSION, true));
}

/*
 * ***************** VIEWS ******************
 */
if ($cmd == '') {

    $cur_dow = "1" . date("w", strtotime(date("Ymd")));
    $this_year = date("Y");
    $this_month = date("m");
    $this_day = date("d");
    $today = $this_year . $this_month . $this_day;
    $next_yearly = $this_year + 1 . $this_month . $this_day;

    if($this_month == 12) {
        $next_month = '01';
        $next_year = $this_year + 1;
    } else {
        $next_month = $this_month + 1;
        $next_year = $this_year;
    }

    $next_monthly = $next_year . $next_month . $this_day;

    /* START REOPEN DAILY */
    // SQL Injection #1
    $login = mysql_real_escape_string($login);
    
	if ($reopen_daily_tasks) {
		$result = db_query("SELECT id FROM ".$table_prefix."tasks "
						  ."WHERE login='$login' "
                          ."AND recurrence='1' "
                          ."AND completed='1' "
                          ."AND compdate < CURDATE()");

		$ids = array();
		while($myrow = mysql_fetch_array($result)) {
			$ids[] = $myrow['id'];
		}

		if (!empty($ids)) {
			db_query("UPDATE ".$table_prefix."tasks "
                    ."SET completed='0',compdate='00000000',compby='' "
					.'WHERE id IN (' . implode(',', $ids) . ')');
		}
    }
    /* END REOPEN DAILY */

    /* START REOPEN WEEKLY */
    $result = db_query("SELECT id,date FROM ".$table_prefix."tasks "
                      ."WHERE login='$login' "
                      ."AND recurrence='2' "
                      ."AND completed='1' "
                      ."AND compdate != CURDATE()");

	$ids = array();
	while($myrow = mysql_fetch_array($result)) {
        if($cur_dow != substr($myrow['date'], 6, 2)) {
    	    $ids[] = $myrow['id'];
        }
	}

	if (!empty($ids)) {
	    db_query("UPDATE ".$table_prefix."tasks "
                ."SET completed='0',compdate='00000000',compby='' "
                .'WHERE id IN (' . implode(',', $ids) . ')');
	}
    /* END REOPEN WEEKLY */

    /* START REOPEN MONTHLY */
    $result = db_query("SELECT id,date FROM ".$table_prefix."tasks "
                      ."WHERE login='$login' "
                      ."AND recurrence='3' "
                      ."AND completed='1'");

    $ids = array();
    while($myrow = mysql_fetch_array($result)) {
        $temp_day = substr($myrow['date'], 6, 2);

        if($temp_day == $this_day) {
            $ids[] = $myrow['id'];
        }
    }

    if (!empty($ids)) {
        db_query("UPDATE ".$table_prefix."tasks "
                ."SET completed='0',compdate='00000000000000',"
                    ."compby='',date='$next_monthly' "
                .'WHERE id IN (' . implode(',', $ids) . ')');
    }
    /* END REOPEN MONTHLY */

    /* START REOPEN YEARLY */
    $result = db_query("SELECT id,date FROM ".$table_prefix."tasks "
                      ."WHERE login='$login' "
                      ."AND recurrence='5' "
                      ."AND completed='1'");

    $ids = array();
    while($myrow = mysql_fetch_array($result)) {
        $temp_day = substr($myrow['date'], 6, 2);

        if($temp_day == $this_day) {
            $ids[] = $myrow['id'];
        }
    }

    if (!empty($ids)) {
        db_query("UPDATE ".$table_prefix."tasks "
                ."SET completed='0',compdate='00000000000000',"
                    ."compby='',date='$next_yearly' "
                .'WHERE id IN (' . implode(',', $ids) . ')');
    }
    /* END REOPEN YEARLY */

	show_default_view();

} elseif ($cmd == 'new_task') {

	$output['task_id'] = -1;

	if (isset($_REQUEST['date'])) {
		$output['date'] = $_REQUEST['date'];
	}

	show_view('edit_task');

} elseif($cmd == 'admin_menu') {

	show_view('admin_menu');

} elseif($cmd == 'edit_task') {

	show_view('edit_task');

} elseif($cmd == 'view_task') {

	show_view(array('view_task', 'list_comments'));

} elseif($cmd == 'show') {

	show_view('list_tasks');

} elseif($cmd == 'calendar') {

	show_view('calendar');
    
} elseif($cmd == 'popcal') {
    
    show_view('popcal');
    
} elseif($cmd == 'remove_category') {

    show_view('admin_remove_category');
    
/*
 * ***************** CONTROLLERS ******************
 */

} elseif($cmd == 'do_add_task_quick') {

	if (isset($_REQUEST['task_name']) && $_REQUEST['task_name']) {
		$task_name = strip_tags($_REQUEST['task_name'], '<b><i>');
		
		// set category if filter is set on list_tasks
		$category = get_value_session_request('filter_category');
		if ($category) {
			// don't do it if supercat
			if (substr($category, 0, 2) == 's_') {
				$category = '';
			}
		}

		add_task($task_name, '', null, 4, $login, 0, $category);

		$output['message'] = _TASK_ADDED;

		redirect_on_post();
	} else {
		$output['error'] = _QUERY_ERROR;

		show_default_view();
	}

} elseif($cmd == 'do_add_task') {

    if($_REQUEST['recurrence'] == 2) {
        $_REQUEST['day'] = $_REQUEST['dow'];
    }

    if($_REQUEST['recurrence'] == 3) {
        $_REQUEST['day'] = $_REQUEST['mday'];
    }

	if(isset($_REQUEST['year'])) {
		$date_array = array($_REQUEST['year'], $_REQUEST['month'], $_REQUEST['day']);
		$final_date = implode("", $date_array);
	}

	$title = strip_tags($_REQUEST['title'], '<b><i>');
	$task = strip_tags($_REQUEST['task'], '<b><i><br><li>');
	$recurrence = $_REQUEST['recurrence'];
	$users = $_REQUEST['users'];
	$priority = $_REQUEST['priority'];
	$category = $_REQUEST['category'];

    if($title == "") {
        $output['error'] = _ENTER_TITLE;
    } else {
    	add_task($title, $task, $final_date, $recurrence, $users, $priority, $category);
        if($users != $login) {
            $task_array = array($title, $task, $recurrence, $priority, 
                                $category, $_SESSION['login']);
            $email_user = db_query("SELECT * FROM ".$table_prefix."users "
                                  ."WHERE login='$users'");
            send_email($email_user, $task_array);
        }
	    $output['message'] = _TASK_ADDED;
    }

	redirect_on_post();

} elseif($cmd == 'do_update_task') {

    if($_REQUEST['recurrence'] == 2) {
        $_REQUEST['day'] = $_REQUEST['dow'];
    }
    
    if($_REQUEST['recurrence'] == 3) {
        $_REQUEST['day'] = $_REQUEST['mday'];
    }

	if(isset($_REQUEST['year'])) {
		$date_array = array($_REQUEST['year'], $_REQUEST['month'], $_REQUEST['day']);
		$final_date = implode("", $date_array);
	}

    if(isset($_REQUEST['recurrence'])) {
    	$recurrence = $_REQUEST['recurrence'];
    } else {
        $recurrence = "";
    }

	$title = strip_tags($_REQUEST['title'], '<b><i>');
	$task = strip_tags($_REQUEST['task'], '<b><i><br><li>');
	$users = $_REQUEST['users'];
	$priority = $_REQUEST['priority'];
	$category = $_REQUEST['category'];
	$task_id = $_REQUEST['task_id'];

    if (!is_numeric($task_id)) {
        exit;
    }

	update_task($title, $task, $final_date, $recurrence, $users, $priority, $category, $task_id);

	$output['message'] = _TASK_UPDATED;

	redirect_on_post();

} elseif($cmd == 'do_install_module') {

    if($isadmin) {
        $result = db_query("INSERT INTO ".$table_prefix."modules (name) "
                          ."VALUES ('" . $_REQUEST['module'] . "')");
    }

	if($result == true) {
	    $output['message'] = _MODULE_INSTALL;
	} else {
		$output['error'] = _QUERY_ERROR;
	}

	$output['next_view'] = 'admin_menu';
    redirect_on_post();
    
} elseif($cmd == 'do_uninstall_module') {

    if($isadmin) {
        $result = db_query("DELETE FROM ".$table_prefix."modules "
                          ."WHERE name='" . $_REQUEST['module'] . "'");
    }
    
	if($result == true) {
	    $output['message'] = _MODULE_UNINSTALL;
	} else {
		$output['error'] = _QUERY_ERROR;
	}

	$output['next_view'] = 'admin_menu';
    redirect_on_post();

} elseif($cmd == 'do_disable_module') {

    if($isadmin) {
        $file = "modules/" . $_REQUEST['module'] . "/module.inc";
        include($file);
        $function = $_REQUEST['module'] . "_disable";
        
        $result = db_query("UPDATE ".$table_prefix."modules "
                          ."SET enabled='0' "
                          ."WHERE name='" . $_REQUEST['module'] . "'");

        $function();
    }

	if($result == true) {
	    $output['message'] = _MODULE_DISABLE;
	} else {
		$output['error'] = _QUERY_ERROR;
	}

	$output['next_view'] = 'admin_menu';
    redirect_on_post();

} elseif($cmd == 'do_enable_module') {

    if($isadmin) {
        $result = db_query("UPDATE ".$table_prefix."modules "
                          ."SET enabled='1' "
                          ."WHERE name='" . $_REQUEST['module'] . "'");
    }

	if($result == true) {
	    $output['message'] = _MODULE_ENABLE;
	} else {
		$output['error'] = _QUERY_ERROR;
	}

	$output['next_view'] = 'admin_menu';
    redirect_on_post();

} elseif($cmd == 'do_load_module') {

    $module = $_REQUEST['module'];
    $what = $_REQUEST['what'];
    $module_file = "modules/" . $module . "/module.inc";
    $module_main = $module . "_module_main";
    include($module_file);

    if($_FILES != NULL) {
        $return = $module_main($what, $_FILES);
    } else {
        $return = $module_main($what);
    }

    $output['message'] = $return['message'];
    $output['error'] = $return['error'];
    $output['next_view'] = $return['next_view'];

    redirect_on_post();

} elseif($cmd == 'do_change_pw') {

    if($isadmin) {
	    $error = change_pw($users, '', $_REQUEST['newpass'], $_REQUEST['verpass']);
    } else {
	    $error = change_pw($users, $_REQUEST['curpass'], $_REQUEST['newpass'], $_REQUEST['verpass']);
    }

	if ($error) {
		show_view('admin_menu');
	} else {
		$output['next_view'] = 'admin_menu';
		redirect_on_post();
	}

} elseif($cmd == 'do_edit_info') {

	if (isset($users) && $users != $login) {
		// trying to edit another user
		verify_admin();
	} else {
		$users = $login;
	}

	if (isset($_REQUEST['newpass'])) {
		$error = change_pw($users, isset($_REQUEST['curpass'])?$_REQUEST['curpass']:null, $_REQUEST['newpass'], $_REQUEST['verpass']);

		if ($error) {
			show_view('admin_menu');
			exit;
		}
	}

	change_user();

	redirect_on_post('index.php?cmd=admin_menu&users='.$users);

} elseif($cmd == 'do_add_user_quick') {

	verify_admin();
    
    if($_REQUEST['add_user'] == "") {
    	$output['error'] = _ENTER_USER;

	    $output['next_view'] = 'admin_menu';
    } else {
        $result = db_query("SELECT * FROM ".$table_prefix."groups WHERE name='$add_user'");

        if(($myrow = mysql_fetch_array($result)) == 0) {
        	$add_user = strip_tags($_REQUEST['add_user']);
	        $result = db_query("INSERT INTO ".$table_prefix."users (login) "
                              ."VALUES ('$add_user')");

        	$output['message'] = _USER_ADDED;

	        $output['next_view'] = 'admin_menu';
        } else {
       	    $output['error'] = _ALREADY_GROUP;

	        $output['next_view'] = 'admin_menu';
        }
    }
    
    redirect_on_post();

} elseif($cmd == 'do_add_group') {
    
	verify_admin();

	if($_REQUEST['group_name'] == "") {
		$output['error'] = _ENTER_GROUP;

		show_view('admin_menu');
	} else {
		$add_group = strip_tags($_REQUEST['group_name']);

        $result = db_query("SELECT * FROM ".$table_prefix."users WHERE login='$add_group'");

        if(($myrow = mysql_fetch_array($result)) == 0) {
    		$result = db_query("INSERT INTO ".$table_prefix."groups (name) "
                              ."VALUES ('$add_group')");
    		$output['message'] = _GROUP_ADDED;
            
            add_groupchat($add_group); // BY ANDREAS

	    	$output['next_view'] = 'admin_menu';
		    redirect_on_post();
        } else {
    		$output['error'] = _ALREADY_USER;

	    	show_view('admin_menu');
        }
	}

} elseif($cmd == 'do_edit_group') {

	verify_admin();

	$group_id = $_REQUEST['group_id'];
	$members = $_REQUEST['members'];

	set_group_members($group_id, $members);
	set_group_name($group_id, $_REQUEST['group_name']);
	$output['message'] = _GROUP_UPDATED;

	redirect_on_post('index.php?cmd=admin_menu&group=' . $group_id);

} elseif($cmd == 'do_remove_from_group') {

	verify_admin();

	$user = $_REQUEST['user'];
	$group_id = $_REQUEST['group_id'];
	remove_user_from_group($user, $group_id);
    remove_user_from_groupchat($user, $group_id);
	$output['message'] = _REMOVED_FROM_GROUP;

	$output['next_view'] = 'admin_menu';
	redirect_on_post();

} elseif($cmd == 'do_add_to_group') {

	verify_admin();

	$user = $_REQUEST['user'];
	$group_id = $_REQUEST['group_id'];
	add_user_to_group($user, $group_id);
    add_user_to_groupchat($user, $group_id);
	$output['message'] = _ADDED_TO_GROUP;

	$output['next_view'] = 'admin_menu';
	redirect_on_post();

} elseif($cmd == 'do_remove_user') {

	verify_admin();

	if($login == $users) {
		$output['error'] = _REMOVE_YOURSELF;
		show_view('admin_menu');
	} else {
		remove_user($users);
		$output['message'] = _USER_REMOVED;

		$output['next_view'] = 'admin_menu';
		redirect_on_post();
	}

} elseif($cmd == 'do_remove_group') {

	verify_admin();

	remove_group($_REQUEST['group_id']);
	$output['message'] = _GROUP_REMOVED;

	$output['next_view'] = 'admin_menu';
	redirect_on_post();

} elseif($cmd == 'do_add_supercat') {
    
	if($_REQUEST['supercat_name'] == "") {
		$output['error'] = _ENTER_SUPERCAT;

		show_view('admin_menu');
	} else {
		$add_supercat = strip_tags($_REQUEST['supercat_name']);

        $result = db_query("SELECT * FROM ".$table_prefix."supercats WHERE name='$add_supercat' AND login='$login'");

        if(($myrow = mysql_fetch_array($result)) == 0) {
    		$result = db_query("INSERT INTO ".$table_prefix."supercats (name, login) "
                              ."VALUES ('$add_supercat', '$login')");
    		$output['message'] = _GROUP_SUPERCAT;

	    	$output['next_view'] = 'admin_menu';
		    redirect_on_post();
        } else {
    		$output['error'] = _ALREADY_SUPERCAT;

	    	show_view('admin_menu');
        }
	}

} elseif($cmd == 'do_remove_from_supercat') {

	$category_id = $_REQUEST['category_id'];
	$supercat_id = $_REQUEST['supercat_id'];
	remove_category_from_supercat($category_id, $supercat_id);
	$output['message'] = _REMOVED_FROM_SUPERCAT;

	$output['next_view'] = 'admin_menu';
	redirect_on_post();

} elseif($cmd == 'do_add_to_supercat') {

	$category_id = $_REQUEST['category_id'];
	$supercat_id = $_REQUEST['supercat_id'];
	add_category_to_supercat($category_id, $supercat_id);
	$output['message'] = _ADDED_TO_SUPERCAT;

	$output['next_view'] = 'admin_menu';
	redirect_on_post();

} elseif($cmd == 'do_remove_supercat') {

	remove_supercat($_REQUEST['supercat_id']);
	$output['message'] = _SUPERCAT_REMOVED;

	$output['next_view'] = 'admin_menu';
	redirect_on_post();

} elseif ($cmd == 'do_add_comment') {

    if($_REQUEST['comment'] == "") {
        $output['error'] = _ENTER_COMMENT;
    } else {
        $comment = strip_tags($_REQUEST['comment'], '<b><i><u><br><li>');
        
        if(isset($_REQUEST['comment_id'])) {
            if(!is_numeric($_REQUEST['comment_id'])) {
                exit;
            }
            update_comment($_REQUEST['comment_id'], $comment);
    	    $output['message'] = _COMMENT_UPDATED;
        } else {
            if(!is_numeric($_REQUEST['task_id'])) {
                exit;
            }
        	add_comment($_REQUEST['task_id'], $comment);
        	$output['message'] = _COMMENT_ADDED;
        }
    }

	$output['next_view'] = 'view_task';
	redirect_on_post();

} elseif ($cmd == 'do_remove_comment') {

	$comment_id = $_REQUEST['comment_id'];

	db_query( "DELETE FROM ".$table_prefix."comments WHERE id = '$comment_id'" );

	$output['message'] = _COMMENT_REMOVED;

	$output['next_view'] = 'view_task';
	redirect_on_post();

} elseif($cmd == 'do_delete_task') {

	$task_id = $_REQUEST['task_id'];

    if (!is_numeric($task_id)) {
        exit;
    }

	$result = db_query("DELETE FROM ".$table_prefix."tasks WHERE id='$task_id'");
	if($result == true) {
		$output['message'] = _TASK_DELETED;
	} else {
		$output['error'] = _QUERY_ERROR;
	}

	show_default_view();

} elseif($cmd == 'do_complete_task') {

	$task_id = $_REQUEST['task_id'];

    if (!is_numeric($task_id)) {
        exit;
    }

	$result = db_query("UPDATE ".$table_prefix."tasks "
		."SET completed='1',compdate=CURDATE()+0,compby='$login' "
		."WHERE id='$task_id'");

	if($result == true) {
		$output['message'] = _TASK_CLOSED;
	} else {
		$output['error'] = _QUERY_ERROR;
	}

	redirect_on_post();

} elseif($cmd == 'do_open_task') {

	$task_id = $_REQUEST['task_id'];

    if (!is_numeric($task_id)) {
        exit;
    }

	$result = db_query("UPDATE ".$table_prefix."tasks "
		."SET completed='0',compdate='00000000',compby=' ' "
		."WHERE id='$task_id'");

	if($result == true) {
		$output['message'] = _TASK_OPENED;
	} else {
		$output['error'] = _QUERY_ERROR;
	}

	redirect_on_post();

} elseif($cmd == 'do_add_category') {

	$name = strip_tags($_REQUEST['category_name']);

	if ($name != "") {
        if ($_REQUEST['global'] == 1) {
    		$result = db_query("INSERT INTO ".$table_prefix."category (name) VALUES ('$name')");
        } else {
    		$result = db_query("INSERT INTO ".$table_prefix."category (name, login) VALUES ('$name', '$login')");
        }

		$output['message'] = _CATEGORY_CREATED;
		$output['next_view'] = 'admin_menu';
		redirect_on_post();
	} else {
		$output['error'] = _ENTER_CATEGORY;
		show_view('admin_menu');
	}

} elseif($cmd == 'do_update_category') {

	$new_name = strip_tags($_REQUEST['new_name']);
	$cat_id = $_REQUEST['category_id'];
	
	if (isset($new_name)) {
		$result = db_query("UPDATE ".$table_prefix."category SET name='$new_name' WHERE id='$cat_id'");

		$output['message'] = _CATEGORY_UPDATED;
		$output['next_view'] = 'admin_menu';
		redirect_on_post();
	} else {
		$output['error'] = _ENTER_CATEGORY;
		show_view('admin_menu');
	}

} elseif($cmd == 'do_remove_category') {
    
    $how = $_REQUEST['how'];
	$category_id = $_REQUEST['category_id'];
    $global = $_REQUEST['global'];

    if ($global == 1) {
        $login = '';
    } else {
        $login = $_SESSION['login'];
    }

	if (isset($category_id)) {
        if ($how == 'rem_all') {
    		$result = db_query("DELETE FROM ".$table_prefix."category "
                              ."WHERE login='$login' "
                              ."AND id='$category_id'");
            
            if($global == 1) {
                $result = db_query("DELETE FROM ".$table_prefix."tasks "
                                  ."WHERE category='$category_id'");
            } else {
                $result = db_query("DELETE FROM ".$table_prefix."tasks "
                                  ."WHERE login='$login' "
                                  ."AND category='$category_id'");
            }
        } elseif ($how == 'reset') {
		    $result = db_query("DELETE FROM ".$table_prefix."category "
                              ."WHERE login='$login' "
                              ."AND id='$category_id'");
            
            if($global == 1) {
                $result = db_query("UPDATE ".$table_prefix."tasks SET category='0' "
                                  ."WHERE category='$category_id'");
            } else {
                $result = db_query("UPDATE ".$table_prefix."tasks SET category='0' "
                                  ."WHERE login='$login' "
                                  ."AND category='$category_id'");
            }
        }

		$output['message'] = _CATEGORY_DELETED;
		$output['next_view'] = 'admin_menu';
		redirect_on_post();
	} else {
		$output['error'] = _NO_ID;
		show_view('admin_menu');
	}

} elseif($cmd == 'do_set_view') {
    
    $default_tasks = $_REQUEST['tasks'];

    if ($_REQUEST['users'] == '') {
        $default_users = "mine";
    } else {
        $default_users = $_REQUEST['users'];
    }
    
    $default_priority = $_REQUEST['priority'];
    $default_category = $_REQUEST['category'];

    $view = & $_SESSION['extra']['view'];

    if (empty($view)) {
        $view = array();
    }
	
    $view['tasks'] = $default_tasks;
    $view['users'] = $default_users;
    $view['priority'] = $default_priority;
    $view['category'] = $default_category;

	$output['message'] = _VIEW_UPDATED;
    
	write_extra();

	show_view('list_tasks');
    
} elseif($cmd == 'do_set_sort') {
	
	$criteria = $_REQUEST['crit'];
	$old_order = $_REQUEST['order'];
	$new_order = $_REQUEST['new_order'];
	
	$order = & $_SESSION['extra']['order'];
	
	if (empty($order)) {
		$order = array();
	}
	
	if ($old_order == 0) {
		$new_crit['criteria'] = $criteria;
		$new_crit['dir'] = 'asc';
		
		$order[$new_order] = $new_crit;
	} elseif ($new_order == 0) {
		$count = count($order);
		if ($old_order < $count) {
			for ($i = $old_order; $i < $count; $i++) {
				$order[$i] = $order[$i + 1];
			}
		}
		unset($order[$count]);
	} else {
		$tmp = $order[$new_order];
		$order[$new_order] = $order[$old_order];
		$order[$old_order] = $tmp;
	}
	
	write_extra();
	
	show_view('list_tasks');
	
} elseif($cmd == 'do_toggle_sort_dir') {
	
	$order = $_REQUEST['order'];
	
	$_SESSION['extra']['order'][$order]['dir'] = $_SESSION['extra']['order'][$order]['dir'] == 'asc' ? 'desc' : 'asc';
	
	write_extra();
	
	show_view('list_tasks');
	
} elseif($cmd == 'do_export_xml') {

    export_xml($_SESSION['query']);
    
} elseif($cmd == 'do_logout') {

	$login = $_SESSION['login'];
    session_destroy();
	remove_onlineuser($login);
	$login_error = _LOGGED_OUT;

	include('html/login.html');

} else {

	if (isset($cmd)) {
		$output['error'] = _COMMAND . "  '$cmd'" . _IS_UNDEFINED;
	}

	show_default_view();
}
?>
