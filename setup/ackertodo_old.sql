# phpMyAdmin SQL Dump
# version 2.6.1
# http://www.phpmyadmin.net
#
# Host: localhost
# Generation Time: Feb 17, 2005 at 04:33 PM
# Server version: 4.0.23
# PHP Version: 4.3.10-2
#

# --------------------------------------------------------

#
# Table structure for table `category`
#

CREATE TABLE `category` (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(20) NOT NULL default '',
  `name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `login` (`login`)
);

# --------------------------------------------------------

#
# Table structure for table `comments`
#

CREATE TABLE `comments` (
  `id` int(5) unsigned NOT NULL auto_increment,
  `task_id` int(5) unsigned NOT NULL default '0',
  `date` timestamp NOT NULL,
  `comment` mediumtext NOT NULL,
  `login` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `task_id` (`task_id`)
);

# --------------------------------------------------------

#
# Table structure for table `group_membership`
#

CREATE TABLE `group_membership` (
  `login` varchar(20) NOT NULL default '',
  `group_id` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`login`,`group_id`)
);

# --------------------------------------------------------

#
# Table structure for table `groups`
#

CREATE TABLE `groups` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
);

# --------------------------------------------------------

#
# Table structure for table `tasks`
#

CREATE TABLE `tasks` (
  `id` int(5) unsigned NOT NULL auto_increment,
  `login` varchar(20) NOT NULL default '',
  `creationdate` timestamp NOT NULL,
  `date` timestamp NOT NULL default '00000000000000',
  `title` varchar(50) NOT NULL default '',
  `task` mediumtext NOT NULL,
  `assignedby` varchar(20) NOT NULL default '',
  `completed` tinyint(1) unsigned NOT NULL default '0',
  `recurrence` tinyint(1) unsigned NOT NULL default '0',
  `priority` tinyint(4) NOT NULL default '0',
  `category` int(11) default '0',
  `compby` varchar(20) NOT NULL default '',
  `compdate` timestamp NOT NULL default '00000000000000',
  PRIMARY KEY  (`id`),
  KEY `priority` (`priority`,`category`),
  KEY `creation_date` (`creationdate`),
  KEY `login` (`login`),
  KEY `category` (`category`)
);

# --------------------------------------------------------

#
# Table structure for table `users`
#

CREATE TABLE `users` (
  `login` varchar(20) NOT NULL default '',
  `password` varchar(50) NOT NULL default '',
  `first` varchar(50) NOT NULL default '',
  `last` varchar(25) NOT NULL default '',
  `email` varchar(50) NOT NULL default '',
  `screenname` varchar(50) NOT NULL default '',
  `admin` tinyint(1) unsigned NOT NULL default '0',
  `allow_others` tinyint(1) NOT NULL default '0',
  `public_list` tinyint(1) NOT NULL default '0',
  `email_notification` tinyint(1) NOT NULL default '0',
  `im_notification` tinyint(1) NOT NULL default '0',
  `theme` varchar(25) NOT NULL default 'default',
  `language` varchar(20) NOT NULL default 'English',
  `date_format` tinyint(1) NOT NULL default '0',
  `extra` blob,
  UNIQUE KEY `login` (`login`)
);

# --------------------------------------------------------

#
# Table structure for table `supercat_membership`
#

CREATE TABLE `supercat_membership` (
  `supercat_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`supercat_id`,`category_id`)
);

# --------------------------------------------------------

#
# Table structure for table `supercats`
#

CREATE TABLE `supercats` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `login` varchar(20) NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
);

# --------------------------------------------------------

# 
# Table structure for table `modules`
# 

CREATE TABLE `modules` (
  `name` varchar(25) NOT NULL default '',
  `enabled` tinyint(1) NOT NULL default '0',
  `version` float default NULL,
  PRIMARY KEY  (`name`)
);

# --------------------------------------------------------

#
# Default values
#

INSERT INTO `users` (login, password, admin) VALUES ('admin', md5('admin'), 1);
INSERT INTO `category` VALUES (0, '', '');
UPDATE `category` SET id=0 WHERE login = '';

