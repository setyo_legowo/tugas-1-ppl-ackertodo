<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: setup.php,v 1.13 2006/09/01 15:49:48 zoidian Exp $
*/
?>
<?php
include('../config/config.inc.php');
global $host, $user, $pass, $database, $table_prefix;
session_start();

if (!function_exists('file_get_contents')) {
	function file_get_contents($url) {
	    $handle = fopen($url, 'r');
	    $string = fread($handle, 2048);
	    fclose($handle);
	    return $string;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title><?php echo $version; ?> Setup</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../themes/default/default.css" type="text/css" />
</head>
<body>
<center>
<?php
if(!isset($_REQUEST['step']) || $_REQUEST['step'] == 1) {
    echo "<form action=\"setup.php?step=2\" method=\"post\">\n"
        ."  <table class=\"login\">\n"
        ."      <th colspan=\"2\">Confirm the Information Below!</th>\n"
        ."      <tr>\n"
        ."          <td align=\"right\">Host:</td><td><input type=\"text\" name=\"host\" value=\"$host\" /></td>\n"
        ."      </tr>\n"
        ."      <tr>\n"
        ."          <td align=\"right\">User:</td><td><input type=\"text\" name=\"user\" value=\"$user\" /></td>\n"
        ."      </tr>\n"
        ."      <tr>\n"
        ."          <td align=\"right\">Password:</td><td><input type=\"password\" name=\"pass\" value=\"$pass\" /></td>\n"
        ."      </tr>\n"
        ."      <tr>\n"
        ."          <td align=\"right\">Database:</td><td><input type=\"text\" name=\"database\" value=\"$database\"/></td>\n"
        ."      </tr>\n"
        ."      <tr>\n"
        ."          <td align=\"right\">Table Prefix:</td><td><input type=\"text\" name=\"table_prefix\" value=\"$table_prefix\"/></td>\n"
        ."      </tr>\n"
        ."      <tr>\n"
        ."          <td></td><td><input type=\"submit\" name=\"create_db\" value=\"Create Database\" /></td>\n"
        ."      </tr>\n"
        ."  </table>\n"
        ."</form>\n";
    session_write_close();
} elseif($_REQUEST['step'] == 2) {
    $_SESSION['host'] = $host = $_REQUEST['host'];
    $_SESSION['user'] = $user = $_REQUEST['user'];
    $_SESSION['pass'] = $pass = $_REQUEST['pass'];
    $_SESSION['database'] = $database = $_REQUEST['database'];

    $db = mysql_connect($host, $user, $pass);
    if(!$db) {
        session_destroy();
        echo "Could not connect to the specified mySQL server!\n"
            ."<br /><br />\n"
            ."<form action=\"" . dirname($_SERVER['PHP_SELF']) . "/setup.php\" method=\"post\">\n"
            ."<input type=\"submit\" name=\"continue\" value=\"Back\" />\n"
            ."</form>\n";
        exit();
    }

    if(mysql_select_db($database)) {
        //session_destroy();
        echo "Database already exists!\n"
            ."<br /><br />\n"
            ."<form action=\"" . dirname($_SERVER['PHP_SELF']) . "/setup.php\" method=\"post\">\n"
            ."<input type=\"submit\" name=\"continue\" value=\"Back\" />\n"
            ."</form>\n"
	        ."<a href=\"setup.php?step=4\">Skip to create tables</a>\n";
        exit();
    }
   
    if($temp = mysql_query("CREATE DATABASE " . $database)) {
        echo "Database created successfully!\n"
            ."<br /><br />\n"
            ."<form action=\"" . dirname($_SERVER['PHP_SELF']) . "/setup.php?step=3\" method=\"post\">\n"
            ."<input type=\"submit\" name=\"continue\" value=\"Continue\" />\n"
            ."</form>\n";
    } else {
        session_destroy();
        echo "Error creating database!\n"
            ."<br /><br />\n"
            ."<form action=\"" . dirname($_SERVER['PHP_SELF']) . "/setup.php\" method=\"post\">\n"
            ."<input type=\"submit\" name=\"continue\" value=\"Back\" />\n"
            ."</form>\n";
        exit();
    }
    session_write_close();
} elseif($_REQUEST['step'] == 3) {
    $host = $_SESSION['host'];
    $user = $_SESSION['user'];
    $pass = $_SESSION['pass'];
    $database = $_SESSION['database'];

    echo "<form action=\"setup.php?step=4\" method=\"post\">\n"
        ."<input type=\"submit\" name=\"create_tables\" value=\"Create Tables\" />\n"
        ."</form>\n";
    session_write_close();
} elseif($_REQUEST['step'] == 4) {
    $host = $_SESSION['host'];
    $user = $_SESSION['user'];
    $pass = $_SESSION['pass'];
    $database = $_SESSION['database'];

    $db = mysql_connect($host, $user, $pass);
    if(!$db) {
        session_destroy();
        echo "Could not connect to specified mySQL server!\n"
            ."<br /><br />\n"
            ."<form action=\"" . dirname($_SERVER['PHP_SELF']) . "/setup.php\" method=\"post\">\n"
            ."<input type=\"submit\" name=\"continue\" value=\"Back\" />\n"
            ."</form>\n";
        exit();
    } else {
    }
   
    mysql_select_db($database);
	$sql = file_get_contents("ackertodo.sql");
   
    if(!$sql) {
        echo "Could not open todo.sql!\n"
            ."<br /><br />\n"
            ."<form action=\"" . dirname($_SERVER['PHP_SELF']) . "/setup.php?step=3\" method=\"post\">\n"
            ."<input type=\"submit\" name=\"continue\" value=\"Back\" />\n"
            ."</form>\n";
        exit();
    } else {
    }
	
    echo "</center>";
    foreach(explode(";", $sql) as $query) {
        $query = trim($query);
        if(empty($query))
			continue;
		// update SQL request with our table name
		$query = preg_replace("/(CREATE TABLE|INSERT INTO|UPDATE) `/is", "\\1 `".$table_prefix, $query);
        $result = mysql_query($query);
        if(!$result) {
            break;
        } else {
            echo $query . " ...[<font color=\"darkgreen\">OK</font>]<br><br><br>";
        }
    }
    echo "<center>";

    if($result) {
        //echo "<meta http-equiv=\"refresh\" content=\"2;url=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/setup.php?step=5 />\n";
        echo "<form action=\"setup.php?step=5\" method=\"post\">\n"
        ."<input type=\"submit\" value=\"Next\" />\n"
        ."</form>\n";
    } else {
        echo "Error creating tables!\n"
            ."<br /><br />\n"
            ."<form action=\"" . dirname($_SERVER['PHP_SELF']) . "/setup.php?step=3\" method=\"post\">\n"
            ."<input type=\"submit\" name=\"continue\" value=\"Back\" />\n"
            ."</form>\n";
        exit();
    }
    session_write_close();
} elseif($_REQUEST['step'] == 5) {
    echo "Setup is complete!";
    session_destroy();
}
?>
</center>
</body>
</html>
