<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: module.inc,v 1.4 2005/06/04 17:03:47 zoidian Exp $
*/
?>
<?php
   /* You can decalre any variables you want to use here, such as:
    *    global $output; 
    *    ...
    */

   /* This function is called when Configure is clicked for a module on
    * the Modules page. */
   function test_disp_module_config($info = NULL) {
   }

   /* This function is called when the admin_edit_user.html file is
    * loaded (when Edit User Information is clicked in the Admin Menu). */
   function testadmin_edit_user($info = NULL) {
   }

   /* This function is called when the view_task.html file is loaded
    * (when a task title is clicked in the Task List). */
   function test_view_task($info = NULL) {
   }

   /* This function is called when the admin_menu.html file is loaded
    * (When Amin Menu is clicked), and it's used to add an entry to the
    * Admin Menu. */
   function test_admin_menu() {
   }

   /* This function is called from index.php and should include the
    * actual code for the module. */
   function test_module_main($what = NULL, $info = NULL) {
   }

   /* This function is called from index.php when the module is disabled
    * from the Module section in the Admin Menu. It's used to maybe add
    * a call to db_query() to update the database to disable your
    * module. See the AIM and Email modules for good examples. */
   function test_disable($info = NULL) {
   }

   /* You can also write your own functions to be called from any of the
    * above functions help make your code easier and cleaner. */
?>
