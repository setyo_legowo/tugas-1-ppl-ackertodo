<?php
/* vim: set ts=4 sw=4 si:
 * ackerTodo - a web-based todo list manager which supports multiple users
 * Copyright (C) 2004-2005 Rob Hensley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * $Id: email.php,v 1.6 2006/09/18 23:47:46 zoidian Exp $
 */
?>
<?php
    $full_path = dirname(__FILE__);
    $temp_path = split("/", $full_path);
    $size = sizeof($temp_path);
    $final_path = '/';

    for ($i = 1; $i < ($size - 2); $i++) {
        $final_path = $final_path . $temp_path[$i] . '/';
    }
    
    include($final_path.'config/config.inc.php');
    include_once($final_path.'includes/languages/english.inc');
    include($final_path.'includes/tasks.inc');

    $replyto = "ackerTodo@localhost"; /* The Reply-To address on the emails. */
    $date = date("Ymd");

    $db = mysql_connect($host, $user, $pass);
    mysql_select_db($database, $db);
    $user_result = mysql_query("SELECT tasks.*, category.name as category_name "
                              ."FROM tasks,category "
                              ."WHERE date='$date' "
                              ."AND tasks.category = category.id");

    while($user_myrow = mysql_fetch_array($user_result)) {
        if($user_myrow['completed'] == 0) {
            $login = $user_myrow['login'];
            $email_result = mysql_query("SELECT * FROM users "
                                       ."WHERE login='$login'");
            $email_myrow = mysql_fetch_array($email_result);
            $email = $email_myrow['email'];
            $date_format = $email_myrow['date_format'];

            $title = $user_myrow['title'];
            $task = $user_myrow['task'];
            $assignedby = $user_myrow['assignedby'];
            list($status, $image) = get_status($user_myrow, $date_format);
            $priority = $_PRIORITY[$user_myrow['priority']];

            if($user_myrow['category'] == 0) {
                $category = _NO_CATEGORY;
            } else {
                $category = $user_myrow['category_name'];
            }

            $body = "You have a task due today!\n";
            $body .= "--------------------------\n\n";
            $body .= "      Title: " . $title . "\n";
            $body .= "       Task: " . $task . "\n";
            $body .= "     Status: " . $status . "\n";
            $body .= "   Priority: " . $priority . "\n";
            $body .= "   Category: " . $category . "\n";
            $body .= "Assigned By: " . $assignedby . "\n\n";
            $body .= "--------------------------\n";
            $body .= "This notification was brought to you by $version!";
            
            $login = $user_myrow['login'];
            mail($email, "You have a task due today!", $body,
                "From: ackerTodo\r\n" .
                "Reply-To: $replyto\r\n" .
                "X-Mailer: PHP/" . phpversion());
        }
    }
?>
