<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: module.inc,v 1.8 2006/09/01 15:49:48 zoidian Exp $
*/
?>
<?php
   global $table_prefix;

   if(!file_exists("languages/" . $_SESSION['language'] . ".inc")) {
      include_once("languages/english.inc");
   } else {
      include_once("languages/" . $_SESSION['language'] . ".inc");
   }

   function aim_admin_edit_user($info = NULL) {
      if($_SESSION['im_notification'] == 1) {
         $status = "checked";
      } else {
         $status = "";
      }
      
      echo "<tr>\n"
          ."   <td align=\"right\"><b>" . _ENABLE_AIM . "</b></td>\n"
          ."   <td><input type=\"checkbox\" name=\"im_notification\""
                  ."class=\"btn\" onmouseover=\"hov(this,'btn btnhov')\""
                  ."onmouseout=\"hov(this,'btn')\" " . $status . ">\n"
          ."</tr>\n";
   }

   function aim_disable($info = NULL) {
      $result = db_query("UPDATE ".$table_prefix."users "
                        ."SET im_notification='0'");
   }
?>
