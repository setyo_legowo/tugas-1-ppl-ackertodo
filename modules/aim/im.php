<?php
/* vim: set ts=4 sw=4 si:
 * ackerTodo - a web-based todo list manager which supports multiple users
 * Copyright (C) 2004-2005 Rob Hensley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * $Id: im.php,v 1.2 2005/05/08 14:59:20 zoidian Exp $
 */
?>
<?php
/* This script is to be used in conjunction with imcomm and sendim
 * written by Claudio Leite. Both of these can be found at
 * http://sourceforge.net/projects/bsflite/ if not as a package, in CVS.
 * All you need to do is copy imcomm into the sendim directory, run
 * make, then copy sendim to /usr/local/bin/ (or wherever else you'd
 * like it). If you put it somewhere other then /usr/local/bin/, then
 * you'll have to change the $sendim variable below to reflect your
 * setup. Once this is done, create a .sendim file in the users home dir
 * who'll be running this script and on the first line put your AIM
 * screenname to send the IM's from, and on the second line put the
 * password for the screenname. Once you've done all this, you should be
 * all set. Run the script manually with: php im.php to make sure it's
 * working correctly, if so you can set up a cronjob similar to this:
 *
 * 0,12   *   *   *   *   /var/www/todo/modules/aim/im.php > /dev/null 2>&1
 * 
 * The above line will run the script at 12:00am and 12:00pm. Read the
 * cron man page for more imformation on setting up cron jobs.
*/
    $full_path = dirname(__FILE__);
    $temp_path = split("/", $full_path);
    $final_path = "/".$temp_path[1]."/".$temp_path[2]."/".$temp_path[3]."/";
    $date = date("Ymd");
    $sendim = "/usr/local/bin/sendim";

    include($final_path.'/config/config.inc.php');
    include($final_path.'/includes/funcs.inc');
    include_once($final_path.'/includes/db.inc');

    $tasks_result = db_query("SELECT * FROM tasks "
                            ."WHERE (date='$date' AND completed='0') "
                            ."OR (recurrence='1' AND completed='0') "
                            ."ORDER BY login");

    while($tasks_myrow = mysql_fetch_array($tasks_result)) {
        $task = $tasks_myrow["task"];
        $title = $tasks_myrow["title"];
        $login = $tasks_myrow["login"];
        $recurrence = $tasks_myrow["recurrence"];
        if($recurrence == 1) {
            $recurrence = " <i>(Daily)</i>";
        }
        
        $im_result = db_query("SELECT * FROM users "
                             ."WHERE login='$login'");
        $im_myrow = mysql_fetch_array($im_result);
        $im = $im_myrow["im_notification"];
        $screenname = $im_myrow["screenname"];

        if($screenname != "") {
            $msg .= $screenname . "\n"
                  . $version . "\n"
                  ."<b>Tasks due today...</b>\n"
                  ."<b>Title:</b> " . $title . $recurrence . "\n"
                  ."<b>Task:</b> " . $task . "\n"
                  .".\n";
        }
        
        $execute = $sendim . " " . $screenname . " \"" . $msg . "\"";
    }

    $fd = popen("/usr/local/bin/sendim", "w");
    fputs($fd, $msg);

    echo $msg;
?>
