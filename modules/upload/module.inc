<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: module.inc,v 1.8 2005/06/14 13:32:04 zoidian Exp $
*/
?>
<?php
   if(!file_exists("languages/" . $_SESSION['language'] . ".inc")) {
      include_once("languages/english.inc");
   } else {
      include_once("languages/" . $_SESSION['language'] . ".inc");
   }

   ini_set('upload_max_filesize', '25000000');
   ini_set('max_execution_time', '100000000');
   global $output;

   function upload_view_task($info = NULL) {
      $files = list_uploaded_files($info);
      $c = 0;

      echo "<table>\n"
          ."<tr>\n"
          ."   <th>" . _NAME . "</th>\n"
          ."   <th>" . _SIZE . "</th>\n"
          ."   <th>" . _ACTIONS . "</th>\n"
          ."</tr>\n";

      for($i = 0; $i < sizeof($files['name']); $i++) {
         $full_file = $files['name'][$i] . "." . $info . "." . $_SESSION['task_id'];
         $class = ((++$c % 2 == 0)?'even':'odd');
         
         echo "<tr class=\"$class\">\n"
             ."   <td><a href=\"tmp/" . $full_file .  "\">" . $files['name'][$i] . "</a></td>\n"
             ."   <td align=\"center\">" . $files['size'][$i] . " bytes</td>\n"
             ."   <td class=\"actions\" align=\"center\"><a href=\"index.php?cmd=do_load_module&amp;module=upload&amp;what=delete&amp;file=" . $full_file . "\">" . _DELETE . "</a></td>\n"
             ."</tr>\n";
      }

      echo "<tr>\n"
          ."   <th>\n"
          ."   <form action=\"index.php?cmd=do_load_module&amp;module=upload&amp;what=attach&amp;task_id=" . $_REQUEST['task_id'] . "\" method=\"post\" enctype=\"multipart/form-data\">\n"
          ."   <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"10000000\" />\n"
          ."   <input type=\"submit\" value=\"" . _ATTACH . "\" />\n"
          ."   <input type=\"hidden\" name=\"who\" value=\"" . $info . "\" />\n"
          ."   </th>\n"
          ."   <th colspan=\"2\"><input type=\"file\" name=\"file\" class=\"btn\" /></th>\n"
          ."   </form>\n"
          ."</tr>\n"
          ."</table>\n"
          ."<br />\n";
   }

   function upload_module_main($what = NULL, $info = NULL) {
      switch ($what) {
         case 'attach':
            $tmp_name = $info['file']['tmp_name'];
            $name = $info['file']['name'] . "." . $_REQUEST['who'] . "." . $_SESSION['task_id'];

            $periods = explode('.', $name);

            if($periods[1] == $_SESSION['login']) {
               $num = 3;
            } elseif($periods[2] == $_SESSION['login']) {
               $num = 4;
            }

            if(!file_exists("tmp/$name") && $periods[$num] == NULL) {   
               $bool = move_uploaded_file($tmp_name, "tmp/$name");
               if($bool) {
                  $return['message'] = _SUCCESS;
               } else {
                  $return['error'] = _PERMISSIONS;
               }
            } else {
               $return['error'] = _EXISTS;
            }
      
            $return['next_view'] = "view_task";
            break;

         case 'delete':
            if(file_exists('tmp/' . $_REQUEST['file'])) {   
               $bool = unlink('tmp/' . $_REQUEST['file']);

               if($bool) {
                  $return['message'] = _DELETE_SUCCESS;
               } else {
                  $return['error'] = _NOT_DELETED;
               }
            } else {
               $return['error'] = _NOT_EXIST;
            }

            $return['next_view'] = "view_task";
            break;
      }
      return $return;
   }

   function list_uploaded_files($login) {
      $i = 0;

      if($handle = opendir('tmp')) {
         while(false !== ($file = readdir($handle))) {
            if($file != '.' && $file != '..' && $file !='CVS') {
               if($file != '') {
                  $size = filesize('tmp/' . $file);
                  $split = explode('.', $file);
                  $n = 0;
                  $file_name = $split[$n];
                  $n++;
                  $file_ext = $split[$n];

                  if($file_ext == $_SESSION['login']) {
                     $file_ext = NULL;
                     $file_full = $file_name;
                  } else {
                     $file_full = $file_name . "." . $file_ext;
                     $n++;
                  }
                  
                  $file_login = $split[$n];
                  $n++;
                  $file_task_id = $split[$n];

                  if($file_task_id == $_SESSION['task_id']) {
                     $uploads['name'][$i] = $file_full;
                     $uploads['size'][$i] = $size;
                     $i++;
                  }
               }
            } else {
               $uploads = NULL;
            }
         }
      }

      return $uploads;
   }

   function upload_disable() {
   }
?>
