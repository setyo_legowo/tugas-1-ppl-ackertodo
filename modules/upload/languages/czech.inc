<?php
/* vim: set ts=4 sw=4 si:
* ackerTodo - a web-based todo list manager which supports multiple users
* Copyright (C) 2004-2005 Rob Hensley
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or (at
* your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* $Id: czech.inc,v 1.1 2005/09/18 15:16:18 zoidian Exp $
*/
?>
<?php
   define("_NAME", "Jméno");
   define("_SIZE", "Velikost");
   define("_ATTACH", "Přiložit");
   define("_SUCCESS", "Soubor byl úspěšně nahrán!");
   define("_PERMISSIONS", "Nepodařilo se nahrát soubor - zkontrolujte oprávnění!");
   define("_EXISTS", "Soubor již existuje!");
   define("_DELETE_SUCCESS", "Soubor byl úspěšně odstraněn!");
   define("_NOT_EXIST", "Soubor neexistuje!");
   define("_NOT_DELETED", "Soubor se nepodařilo odstranit!");
?>
