<?php
/* vim: set ts=4 sw=4 si:
 * ackerTodo - a web-based todo list manager which supports multiple users
 * Copyright (C) 2004-2005 Rob Hensley
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * $Id: cmd.php,v 1.13 2005/03/20 16:03:57 zoidian Exp $
 */
?>
<?php
    $full_path = dirname(__FILE__);
    $temp_path = split("/", $full_path);
    $final_path = "/".$temp_path[1]."/".$temp_path[2]."/".$temp_path[3]."/";
    $i = 1;

    include($final_path.'/config/config.inc.php');
    include($final_path.'/includes/funcs.inc');
    include($final_path.'/includes/cli.inc');
    include_once($final_path.'/includes/db.inc');

    $options = array("list_tasks", "add_task", "del_task",
                     "list_users", "add_user", "del_user",
                     "list_categories", "add_category", "del_category");

    $choice = $argv[1];

    if($choice == NULL || !in_array($choice, $options)) {
        if($choice == NULL) {
            echo "You must enter a command!\n";
        } else {
            echo $choice . " is not a valid command!\n";
        }
        print_help();
        exit;
    }

    if(!defined("STDIN")) { 
        define("STDIN", "fopen('php://stdin','r')");
    }

    $login = login();

    switch($choice) {
        case "list_tasks":
            list_tasks($login);
            break;
        case "add_task":
            add_task($login);
            break;
        case "del_task":
            del_task($login);
            break;
        case "list_users":
            list_users();
            break;
        case "add_user":
            add_user();
            break;
        case "del_user":
            list_users("del");
            del_user($login);
            break;
        case "add_category":
            add_category($login);
            break;
        case "del_category":
            list_categories($login);
            del_category($login);
            break;
        case "list_categories":
            list_categories($login);
            break;
    }
?>
